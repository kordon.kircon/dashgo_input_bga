/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import {StatusBar} from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import {connect} from 'react-redux'
import Login from './Sources/Screens/Login'
import Process from './Sources/Screens/Process'
import Cycle from './Sources/Screens/Cycle'
import Station from './Sources/Screens/Station'
import Scanner from './Sources/Screens/Scanner'
import InputForm from './Sources/Screens/InputForm'
import Home from './Sources/Screens/Home'
import DynamicForm from './Sources/Screens/DynamicForm'
import QuestionForm from './Sources/Screens/QuestionForm'
import SubHistory from './Sources/Screens/SubHistory'
import MemberPicker from './Sources/Screens/MemberPicker'
import Color from './Sources/Utilities/Color'
import {navigationRef} from './Sources/Utilities/RootNavigation'
import NavBar,{navBarRef} from './Sources/Components/NavBar'
import {setBatchLog,setIdStation, identity, doupdate} from './Sources/Redux/Actions/DataAction'
// import GPS, {gpsRef} from './Sources/Components/GPS'

const mapStateToProps = state => {
  return {
      isLogin: state.auth.isLogin,
      statusBarColor: state.data.identity?.navbar_bg
  }
}

const Stack = createStackNavigator()
const options = {headerShown:false}

class App extends React.Component{
  constructor(props){
    super(props)
  }

  componentDidMount(){
    this.props.identity()
  }

  render(){
    let{isLogin,statusBarColor} = this.props
    return(
      <NavigationContainer ref={navigationRef}>
        {/* <GPS ref = {gpsRef}/> */}
        <StatusBar backgroundColor={statusBarColor?statusBarColor:Color.navBarBg}/>
        <NavBar/>
        <Stack.Navigator 
          initialRouteName = {isLogin?"Home":"Login"}
          screenOptions = {{
            cardStyle:{backgroundColor:Color.bg}
          }}
        >
          <Stack.Screen 
            name="Login" 
            component={Login} 
            options = {options}
          />
          <Stack.Screen 
            name="Process" 
            component={Process} 
            options = {options}
            // listeners = {
            //   {
            //     focus: _ => {
            //       this.props.setBatchLog(null)
            //     } 
            //   }
            // }
          />
          <Stack.Screen 
            name="Cycle" 
            component={Cycle} 
            options = {options}
          />
          {/* <Stack.Screen 
            name="Station" 
            component={Station} 
            options = {options}
            // listeners = {
            //   {
            //     focus: _ => {
            //       this.props.setIdStation(null)
            //     } 
            //   }
            // }
          /> */}
          {/* <Stack.Screen 
            name="Scanner" 
            component={Scanner} 
            options = {options}
          /> */}
          <Stack.Screen 
            name="InputForm" 
            component={InputForm} 
            options = {options}
          />
          <Stack.Screen 
            name="Home" 
            component={Home} 
            options = {options}
          />
          <Stack.Screen 
            name="DynamicForm" 
            component={DynamicForm} 
            options = {options}
          />
          <Stack.Screen 
            name="QuestionForm" 
            component={QuestionForm} 
            options = {options}
          />
          <Stack.Screen 
            name="SubHistory" 
            component={SubHistory} 
            options = {options}
          />
          <Stack.Screen 
            name="MemberPicker" 
            component={MemberPicker} 
            options = {options}
          />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

export default connect(mapStateToProps,{setBatchLog,setIdStation,identity,doupdate})(App)