import React from 'react'
import {View,Text,FlatList,TouchableOpacity} from 'react-native'
import NavBar,{NavBarTypes} from '../Components/NavBar'
import {connect} from 'react-redux'
import Color from '../Utilities/Color'
import {dateToPretty,dateToTimePretty, sizeFigma} from '../Utilities/Conversion'
import Button,{ButtonTypes} from '../Components/Button'
import {startProcess,stopProcess,setCurrentStory} from '../Redux/Actions/ProcessAction'
import {startBatch, saveBatch, setBatchLog, checkLog} from '../Redux/Actions/DataAction'
import ItemList from '../Components/ItemList'
import TextNormal from '../Components/TextNormal'
import TextBold from '../Components/TextBold'
import ArrowRight from '../Components/ArrowRight'
import { addListener, navigate } from '../Utilities/RootNavigation'
import { showAlert } from './Alert'
import ic_sync from '../Images/sync.png'
import Image from '../Components/Image'
import styles from '../Utilities/Style'

const mapStateToProps = (state, ownProps) => {
    return {
        batch: state.data.batch,
        startDate: state.process.startDate,
        data: state.data.batchHistory,
        millName: state.data.template ? state.data.template.template.mill_desc : '',
        isRefresh: state.data.refreshProcess,
        isTemplateAvailable: state.data.version != 0
    }
}

class Process extends React.Component{
    componentDidMount() {
        this.props.checkLog()
    }

    render(){
        let{startDate,data,millName,batch,isRefresh, isTemplateAvailable} = this.props
        return(
            <View style={{flex:1}}>
                <NavBar
                    items = {[
                        {
                            type: NavBarTypes.SPACE
                        },
                        {
                            type:NavBarTypes.LOGOUT
                        }
                    ]}
                />
                <Text style={{...styles.productsans_bold_17,paddingTop:sizeFigma(10),alignSelf:'center'}}>{millName}</Text>
                {batch?
                <TouchableOpacity 
                    style={{paddingVertical:10,paddingHorizontal:20,alignItems:'center',flexDirection:'row'}}
                    onPress = {()=>{
                        // this.props.setCurrentStory(startDate)
                        this.props.setBatchLog(null)
                        navigate('Cycle')
                    }}
                >
                    <View style={{flex:1}}>
                        <TextNormal text = {batch.shift}/>
                        <TextNormal text = {batch.tgl_report}/>
                    </View>
                    <ArrowRight/>
                </TouchableOpacity>
                :
                <Clock style={{marginVertical:20}}/>
                }
                <Text style={{backgroundColor:Color.gray_light,fontSize:14,color:Color.gray_dark,paddingVertical:10,paddingHorizontal:20}}>History</Text>
                <FlatList
                    style={{flex:1}}
                    data = {data}
                    keyExtractor = {(index,item) => index+item}
                    extraData = {isRefresh}
                    renderItem = {({item,index}) => 
                        <ItemList 
                            onPress = {()=>{
                                this.props.setBatchLog(item)
                            }}
                            renderItem = { () => {
                                let startTimes = item.start_batch.split(' ')
                                let startTime = startTimes[0]+' ('+startTimes[1]+')'
                                let endTimes = item.end_batch.split(' ')
                                let endTime = endTimes[0]+' ('+endTimes[1]+')'
                                return (
                                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                        {!item.isSent && 
                                        <Image
                                            style = {{width:24,height:24,marginRight:20,tintColor:Color.gray}}
                                            source = {ic_sync}
                                        />
                                        }
                                        <View style = {{flex:1,flexDirection:'row'}}>
                                            <View style = {{flex:2}}>
                                                <TextNormal text = {'Start'}/>
                                                <TextNormal text = {'End'}/>
                                            </View>
                                            <View>
                                                <TextNormal text = {':   '}/>
                                                <TextNormal text = {':   '}/>
                                            </View>
                                            <View style = {{flex:7}}>
                                                <TextNormal text = {startTime}/>
                                                <TextNormal text = {endTime}/>
                                            </View>
                                        </View>
                                        <TextNormal text = {item.shift}/>
                                    </View>
                                )
                            }
                            }
                        />
                    }
                />
                {isTemplateAvailable && 
                
                <Button
                    style = {{margin:10}}
                    type = {batch?ButtonTypes.RED:ButtonTypes.GREEN}
                    text = {batch?'Stop Proses':'Start Proses'}
                    onPress = {()=>{
                        if (batch) {
                            showAlert(
                                'Peringatan',
                                'Apakah anda yakin ingin menghentikan proses? Data akan disimpan dan tidak akan bisa dirubah setelah proses dihentikan',
                                'Stop Process',
                                () => {
                                    this.props.saveBatch()
                                }
                            )
                        } else {
                            this.props.startBatch()
                        }
                    }}
                />
                }
            </View>
        )
    }
}

class Clock extends React.Component{

    constructor(){
        super()
        this.state = {
            currentTime:new Date()
        }
    }

    componentDidMount(){
        this.timer = setInterval(() => {
            this.setState({
                currentTime:new Date()
            })        
        }, 1000);
    }

    componentWillUnmount(){
        clearInterval(this.timer)
    }

    render(){
        let{style} = this.props
        let{currentTime} = this.state
        return(
            <View style={{alignItems:'center',justifyContent: 'center',...style}}>
                <TextBold text = {dateToPretty(currentTime)}/>
                <TextBold 
                    style={{marginTop:5,color:Color.green,backgroundColor:Color.gray_light,paddingVertical:5,paddingHorizontal:20,borderRadius:5}}
                    text = {dateToTimePretty(currentTime)}
                />
            </View>
        )
    }
}

export default connect(mapStateToProps,{startProcess,stopProcess,setCurrentStory,startBatch,saveBatch,setBatchLog,checkLog})(Process)