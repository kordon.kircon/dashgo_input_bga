import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View
} from 'react-native'
import NavBar,{NavBarTypes} from '../Components/NavBar'
// import QRCodeScanner from 'react-native-qrcode-scanner';
// import { RNCamera } from 'react-native-camera';
import {scanQrCode} from '../Redux/Actions/DataAction'
import {connect} from 'react-redux'
import { showLoading, hideLoading } from '../Components/Loading';
import GetLocation from '@react-native-community/geolocation'

class Scanner extends Component {
  onSuccess = e => {
    showLoading()
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
    .then(location => {
      this.props.scanQrCode(e.data, location)
    })
    .catch(error => {
      const { code, message } = error;
      console.warn('GET LOCATION',code, message);
    })
    .finally(() => {
      hideLoading()
    })
  };

  render() {
    return (
      <View style = {{flex:1}}>
        {/* <QRCodeScanner
          onRead={this.onSuccess}
          flashMode={RNCamera.Constants.FlashMode.torch}
        /> */}
        <NavBar
          style = {{position:'absolute',left:0,top:0,right:0}}
          items = {[
            {
              type:NavBarTypes.BACK
            },{
              type:NavBarTypes.TITLE,
              text:'QR Code Scanner'
            }
          ]}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16
  }
});

export default connect(null,{scanQrCode})(Scanner)