import React from 'react'
import {View,Text,FlatList,TouchableOpacity} from 'react-native'
import NavBar,{NavBarTypes} from '../Components/NavBar'
import {connect} from 'react-redux'
import ItemList from '../Components/ItemList'
import Button,{ButtonTypes} from '../Components/Button'
import {startCycle} from '../Redux/Actions/CycleAction'
import {setBatchLog,setCycleNumber,addCycle} from '../Redux/Actions/DataAction'

const mapStateToProps = (state, ownProps) => {
    let isLog = state.data.batchLog != null
    let batch = isLog ? state.data.batchLog : state.data.batch
    let cycles = batch.cycle ? batch.cycle : []
    return {
        data: cycles,
        isLog: isLog,
        // isFinish: !state.process.currentStory.isFinish && cycles.every(item => item.endDate!=undefined && item.endDate!=null),
        // isRefresh:state.cycle.isRefresh,
        isFinish: !isLog && cycles[cycles.length-1].end_date != undefined,
        isRefresh: state.data.refreshCycle,
    }
}

class Cycle extends React.Component{
    render(){
        let{data,isFinish,isRefresh,isLog} = this.props
        return(
            <View style={{flex:1}}>
                <NavBar
                    items = {[
                        {
                            type:NavBarTypes.BACK,
                        },{
                            type:NavBarTypes.TITLE,
                            text:'Batch Cycle'
                        }
                    ]}
                />
                <FlatList
                    style = {{flex:1}}
                    data = {data}
                    keyExtractor = {(item, index) => index+item}
                    extraData = {isRefresh}
                    renderItem = {({item,index}) => 
                        <ItemList
                            onPress = {()=>{
                                this.props.setCycleNumber(item.number)
                                // this.props.startCycle(item)
                            }}
                            leftText = {'Cycle '+item.number}
                            rightText = {isLog?'':item.end_date?'Done':'On Going'}
                        />
                    }
                />
                {isFinish &&
                <Button
                    style = {{margin:10}}
                    type = {ButtonTypes.GREEN}
                    text = {'Tambah Cycle'}
                    onPress = {()=>{
                        this.props.addCycle()
                    }}
                />
                }
            </View>
        )
    }
}

export default connect(mapStateToProps,{startCycle,addCycle,setBatchLog,setCycleNumber})(Cycle)