import React from 'react'
import {View, FlatList, Text, TextInput, ScrollView} from 'react-native'
import {connect} from 'react-redux'
import NavBar, { NavBarTypes } from '../Components/NavBar'
import {copy, sizeFigma} from '../Utilities/Conversion'
import Input, {InputTypes} from '../Components/Input'
import styles from '../Utilities/Style'
import {showAlert, showSimpleAlert} from './Alert'
import Color from '../Utilities/Color'
import Button, { ButtonTypes } from '../Components/Button'
import {goBack, navigate} from '../Utilities/RootNavigation'
import { cancelForm, createForm, saveFormItem, createTemporary, setCurrentHistory, setCurrentSubParent } from '../Redux/Actions/LocalAction'
import { cancelAct, saveFormItemAct, setCurrentAct, startAct } from '../Redux/Actions/ActAction'
// import { setEventListener } from '../Components/GPS'
import GPS from '../Components/GPS'

const mapStateToProps = (state, ownProps) => {
    let {key} = state.local
    var isViewOnly = false
    var isDisable = false
    var display, nav_bar_title, template, attention, image, current
    if(key){
        let currentItem = state.local[key]
        display = currentItem.display
        nav_bar_title = currentItem.nav_bar_title
        template = currentItem.template
        attention = currentItem.attention
        image = currentItem.image
        current = currentItem.current
        if(current && current.start){
            isDisable = true
        }
    }
    var isAct = false
    var isActOnProgress = false
    var isHaveSubs = false
    if(state.local.currentHistory){
        let currentHistory = state.local.currentHistory
        display = currentHistory.display
        nav_bar_title = currentHistory.nav_bar_title
        current = true
        isViewOnly = true
        isDisable = true
    }
    if(state.act.current){
        let currentAct = state.act.current
        display = currentAct.display
        nav_bar_title = currentAct.nav_bar_title
        current = true
        isViewOnly = currentAct.finish
        isAct = true
        isActOnProgress = !currentAct.finish && currentAct.start
        isDisable = true
    }
    if(state.local.currentSubParent){
        let currentSubParent = state.local.currentSubParent
        display = currentSubParent.display
        nav_bar_title = currentSubParent.nav_bar_title
        isViewOnly = true
        isHaveSubs = true
        isDisable = true
    }
    console.log('template',template)
    return {
        datas: display,
        navBarTitle: nav_bar_title,
        template: template,
        attentionDatas: attention,
        image: image,
        current: current,
        isValid: !display?.find(item => {
            if(item.type == 'teampicker' && item.answer) {
                return item.answer.data.findIndex(it => it.isSelected && it.image) == -1
            }
            if(item.required){
                return !item.answer
            }
            return false
        }),
        isViewOnly: isViewOnly,
        isAct: isAct,
        isActOnProgress: isActOnProgress,
        isHaveSubs: isHaveSubs,
        isDisable: isDisable
    }
}

class DynamicForm extends React.Component {
    constructor() {
        super()
        this.state = {
            location: null,
            question: null,
            reload: false,
        }
        this.renderItem = this.renderItem.bind(this)
        this.renderDropdown = this.renderDropdown.bind(this)
        this.getDropdownDatas = this.getDropdownDatas.bind(this)
        this.getTeampickerDatas = this.getTeampickerDatas.bind(this)
        this.renderGPSAutofill = this.renderGPSAutofill.bind(this)
        this.cleanupDependency = this.cleanupDependency.bind(this)
        this.renderTeampicker = this.renderTeampicker.bind(this)
        this.renderGPS = this.renderGPS.bind(this)
        this.renderTextInput = this.renderTextInput.bind(this)
        this.onSuccessGPS = this.onSuccessGPS.bind(this)
    }

    onSuccessGPS(position) {
        if(!isViewOnly){
            if(isAct){
                this.props.saveFormItemAct(item, position.coords.latitude+', '+position.coords.longitude)
            }else{
                this.props.saveFormItem(item, position.coords.latitude+', '+position.coords.longitude)
            }
        }
        this.setState({
            location : {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            }
        })
    }

    // componentDidMount() {
    //     setEventListener(
    //         this.onSuccessGPS,
    //         error => {
    //             if(!isViewOnly){
    //                 if(isAct){
    //                     this.props.saveFormItemAct(item, null)
    //                 }else{
    //                     this.props.saveFormItem(item, null)
    //                 }
    //             }
    //         }
    //     )
    // }

    renderGPS() {
        let{datas, isAct, isViewOnly} = this.props
        let item = datas?.find(item => item.type == 'gpsautofill')
        return(
            <GPS
                isDisabled = {item == undefined}
                onSuccess = {position => {
                    if(!isViewOnly){
                        if(isAct){
                            this.props.saveFormItemAct(item, position.coords.latitude+', '+position.coords.longitude)
                        }else{
                            this.props.saveFormItem(item, position.coords.latitude+', '+position.coords.longitude)
                        }
                    }
                    this.setState({
                        location : {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude
                        }
                    })
                }}
                onError = {error => {
                    if(!isViewOnly){
                        if(isAct){
                            this.props.saveFormItemAct(item, null)
                        }else{
                            this.props.saveFormItem(item, null)
                        }
                    }
                }}
            />
        )
    }

    componentWillUnmount() {
        this.props.setCurrentHistory(null)   
        this.props.setCurrentAct(null)
        this.props.setCurrentSubParent(null)
        // setEventListener(null, null)
    }

    async getDropdownDatas(item, isInit = true) {
        let{template,datas} = this.props
        if(item.dependency_lvl == null){
            // console.log('load from template')
            let datas = template[item.lookup_key].filter(it => it.lvl == item.lvl)
            return datas
        }else{
            let dependencyItem = datas.find(it => it.id_lvl == item.dependency_lvl)
            // console.log('load from answer', dependencyItem.answer, dependencyItem.original_answer)
            if(dependencyItem.answer){
                if(dependencyItem.original_answer) {
                    return dependencyItem.original_answer.children
                }
                return dependencyItem.answer.children
            }else if(!isInit){
                showSimpleAlert('Peringatan',dependencyItem.title+' harus diisi terlebih dahulu')
            }
        } 
        return []
    }

    async getTeampickerDatas(item, isInit = true) {
        let{template, datas} = this.props
        let dependencyItem = datas.find(it => it.id_lvl == item.dependency_lvl)
        if(dependencyItem.answer){
            return template[item.lookup_key].list_team.find(it => it.id_lv4 == dependencyItem.answer.id).teams
        }else if(!isInit){
            showSimpleAlert('Peringatan',dependencyItem.title+' harus diisi terlebih dahulu')
        }
        return []
    }

    renderGPSAutofill(item) {
        return(
            <View style = {{marginBottom: sizeFigma(10)}} key = {JSON.stringify(item)}>
                <Text style = {{...styles.productsans_bold_12, marginBottom: sizeFigma(5)}}>{item.title}</Text>
                <View style = {{...styles.input_border, borderColor: Color.gray_light}}>
                    <Text style = {{borderColor: Color.gray_light}}>{item.answer}</Text>
                </View>
            </View>
        )
    }

    renderTeampicker(item) {
        let{current, isViewOnly} = this.props
        let isValidMember = isViewOnly || item.answer?.data?.findIndex(it => it.isSelected && it.image) != -1
        return(
            <View style = {{marginBottom: sizeFigma(10)}} key = {JSON.stringify(item)}>
                <Text style = {{...styles.productsans_bold_12, marginBottom: sizeFigma(5)}}>{item.title}</Text>
                <View style = {styles.row_center}>
                    <Input
                        style = {{flex: 1}}
                        type = {InputTypes.PICKER}
                        value = {item.answer}
                        placeholder = {item.placeholder}
                        isDisabled = {isViewOnly}
                        onSelect = {it => {
                            this.props.saveFormItem(item, it)
                        }}
                        getDatas = {()=>{
                            return this.getTeampickerDatas(item, false)
                        }}
                    />
                    {item.answer &&
                    <Button
                        style = {{paddingHorizontal: sizeFigma(20), marginLeft: sizeFigma(10)}}
                        type = {isValidMember?ButtonTypes.GREEN:ButtonTypes.RED}
                        text = {isViewOnly?'Lihat Anggota':'Pilih Anggota'}
                        onPress = {()=>{
                            navigate('MemberPicker')
                        }}
                    />
                    }
                </View>
            </View>
        )
    }

    renderTextInput(item) {
        let{datas, current, isViewOnly, isAct, isDisable} = this.props
        let{reload} = this.state
        let key = copy(item)
        delete key.answer
        delete key.original_answer
        return(
            <View style = {{marginBottom: sizeFigma(10)}} key = {JSON.stringify(key)}>
                <Text style = {{...styles.productsans_bold_12, marginBottom: sizeFigma(5)}}>{item.title}</Text>
                <Input
                    // type = {InputTypes.PICKER}
                    value = {item.answer}
                    placeholder = {item.placeholder}
                    // isDisabled = {isViewOnly || isAct}
                    isDisabled = {isDisable}
                    onChangeText = {it => {
                        this.props.saveFormItem(item, it)
                    }}
                />
            </View>
        )
    }

    renderDropdown(item) {
        let{datas, current, isViewOnly, isAct, isDisable} = this.props
        let{reload} = this.state
        return(
            <View style = {{marginBottom: sizeFigma(10)}} key = {JSON.stringify(item)}>
                <Text style = {{...styles.productsans_bold_12, marginBottom: sizeFigma(5)}}>{item.title}</Text>
                <Input
                    type = {InputTypes.PICKER}
                    value = {item.answer}
                    placeholder = {item.placeholder}
                    // isDisabled = {isViewOnly || isAct}
                    isDisabled = {isDisable}
                    onSelect = {it => {
                        this.props.saveFormItem(item, it)
                    }}
                    getDatas = {()=>{
                        return this.getDropdownDatas(item, false)
                    }}
                />
            </View>
        )
    }

    renderItem({item, index}) {
        switch(item.type){
            case 'teampicker':
                return this.renderTeampicker(item)
            case 'dropdown':
                return this.renderDropdown(item)
            case 'gpsautofill':
                return this.renderGPSAutofill(item)
            case 'text':
                return this.renderTextInput(item)
            default:
                return null
        }
    }

    cleanupDependency(id_lvl) {
        let{reload} = this.state
        let{datas} = this.props
        var newIdLvl = null
        datas = datas.map(it => {
            if(it.dependency_lvl == id_lvl){
                it.answer = null
                newIdLvl = it.id_lvl
            }
            return it
        })
        if(newIdLvl != null){
            this.cleanupDependency(newIdLvl)
        }
        this.setState({
            reload: !reload
        })
    }

    render(){
        let{navBarTitle, datas, attentionDatas, image, current, isValid, isViewOnly, isActOnProgress, isAct, isHaveSubs} = this.props
        let{question, reload, location} = this.state
        var showCancelButton = !isViewOnly
        if(isAct){
            showCancelButton = !isViewOnly && isActOnProgress
        }
        return(
            <View style = {{flex: 1}}>
                {this.renderGPS()}
                <NavBar
                    items = {
                        [
                            {
                                type: NavBarTypes.BACK,
                                onBack: (isAct && isActOnProgress) || (!isAct && current && !isViewOnly) ? ()=>{
                                    showAlert(null,'Apakah anda yakin ingin membatalkan proses?', 'Ya', () => {
                                        if(isActOnProgress){
                                            this.props.cancelAct()
                                        }else{
                                            this.props.cancelForm()
                                        }
                                        goBack()
                                    })
                                }:null
                            }, {
                                type: NavBarTypes.TITLE,
                                text: navBarTitle
                            }

                        ]
                    }
                />
                <ScrollView style = {{flex: 1}}>
                    <View style = {{padding: sizeFigma(20)}}>
                    {datas.map((item,index) => this.renderItem({item,index}))}
                    {
                        (current && current.start) || isActOnProgress ? 
                        <View style = {{flexDirection: 'row',marginTop: sizeFigma(20)}}>
                            {showCancelButton &&
                            <Button
                                style = {{flex: 1, marginRight: sizeFigma(20)}}
                                type = {ButtonTypes.RED}
                                text = {'Batalkan'}
                                onPress = {()=>{
                                    if(isActOnProgress){
                                        this.props.cancelAct()
                                    }else{
                                        this.props.cancelForm()
                                    }
                                }}
                            />
                            }
                            <Button
                                style = {{flex: 1}}
                                type = {ButtonTypes.GREEN}
                                text = {isAct?isActOnProgress?'Lanjutkan':isViewOnly?'Lanjutkan':'Mulai':'Lanjutkan'}
                                onPress = {()=>{
                                    if(!isActOnProgress && isAct && !isViewOnly && location){
                                        this.props.startAct(location.latitude + ', ' + location.longitude)
                                    }
                                    navigate('QuestionForm')
                                }}
                            />
                        </View>
                        :
                        <Button
                            style = {{marginTop: sizeFigma(20)}}
                            type = {isValid?ButtonTypes.GREEN:ButtonTypes.DISABLE}
                            text = {isHaveSubs?'Lihat Subs': isViewOnly?'Lihat':'Submit'}
                            onPress = {()=>{
                                if(isHaveSubs){
                                    navigate('SubHistory')
                                }else{
                                    if(isValid){
                                        if(!isViewOnly){
                                            if(!isActOnProgress && isAct && location){
                                                this.props.startAct(location.latitude + ', ' + location.longitude)
                                            }else{
                                                this.props.createForm()
                                            }
                                        }
                                        navigate('QuestionForm')
                                    }else{
                                        showSimpleAlert(null, 'Semua harus terisi terlebih dahulu')
                                    }
                                }
                            }}
                        />
                    }
                    </View>
                </ScrollView>
                {/* <FlatList
                    style = {{flex: 1}}
                    data = {datas}
                    keyExtractor = {(item, index) => item+index+item.answer}
                    contentContainerStyle = {{padding: sizeFigma(20)}}
                    renderItem={this.renderItem}
                    ListFooterComponent={
                        current || isActOnProgress ? 
                        <View style = {{flexDirection: 'row',marginTop: sizeFigma(20)}}>
                            {showCancelButton &&
                            <Button
                                style = {{flex: 1, marginRight: sizeFigma(20)}}
                                type = {ButtonTypes.RED}
                                text = {'Batalkan'}
                                onPress = {()=>{
                                    if(isActOnProgress){
                                        this.props.cancelAct()
                                    }else{
                                        this.props.cancelForm()
                                    }
                                }}
                            />
                            }
                            <Button
                                style = {{flex: 1}}
                                type = {ButtonTypes.GREEN}
                                text = {isAct?isActOnProgress?'Lanjutkan':isViewOnly?'Lanjutkan':'Mulai':'Lanjutkan'}
                                onPress = {()=>{
                                    if(!isActOnProgress && isAct && !isViewOnly && location){
                                        this.props.startAct(location.latitude + ', ' + location.longitude)
                                    }
                                    navigate('QuestionForm')
                                }}
                            />
                        </View>
                        :
                        <Button
                            style = {{marginTop: sizeFigma(20)}}
                            type = {isValid?ButtonTypes.GREEN:ButtonTypes.DISABLE}
                            text = {isHaveSubs?'Lihat Subs': 'Submit'}
                            onPress = {()=>{
                                if(isHaveSubs){
                                    navigate('SubHistory')
                                }else{
                                    if(isValid){
                                        this.props.createForm()
                                        navigate('QuestionForm')
                                    }else{
                                        showSimpleAlert(null, 'Semua harus terisi terlebih dahulu')
                                    }
                                }
                            }}
                        />
                    }
                /> */}
            </View>
        )
    }
}

export default connect(mapStateToProps, 
    {
        saveFormItem,
        createForm,
        cancelForm,
        createTemporary,
        setCurrentHistory,
        setCurrentAct,
        startAct,
        cancelAct,
        setCurrentSubParent,
        saveFormItemAct
    })(DynamicForm)