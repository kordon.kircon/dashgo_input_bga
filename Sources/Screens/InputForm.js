import React from 'react'
import {View,FlatList,TextInput,SectionList,Text,Switch} from 'react-native'
import NavBar,{NavBarTypes} from '../Components/NavBar'
import {connect} from 'react-redux'
import TextNormal from '../Components/TextNormal'
import TextBold from '../Components/TextBold'
import Color from '../Utilities/Color'
import Line from '../Components/Line'
import {setInputForm} from '../Redux/Actions/InputFormAction'
import {getStation, getUnits, setMeasureValue, setStationEndDate, setUnitOperate, setUnitReason, setUnitNotes} from '../Redux/Actions/DataAction'
import Button,{ButtonTypes} from '../Components/Button'
import { showAlert } from './Alert'
import { showPicker } from '../Components/Picker'
import { sizeFigma } from '../Utilities/Conversion'
import styles from '../Utilities/Style'

const mapStateToProps = (state, ownProps) => {
    let isLog = state.data.batchLog ? true : false
    var batch
    if (isLog) {
        batch = state.data.batchLog
    } else {
        batch = state.data.batch
    }
    let cycleNumber = state.data.cycleNumber
    let idStation = state.data.idStation
    let station = getStation(batch,cycleNumber,idStation)
    let stationName = station.station_desc
    let data = getUnits(batch,cycleNumber,idStation).map(item => {
        return {
            id: item.id_unit,
            title: item.unit_desc,
            data: item.measures,
            isOperate: item.is_operate,
            reason: item.reason
        }
    })
    return {
        isLog:isLog,
        stationName: stationName,
        data: data,
        isFinish:isLog || station.end_date,
        isComplete: data.filter(item => item.isOperate).every(item => item.data.every(item => item.value != undefined && item.value != null && item.value != '')),
        reasons: state.data.template.reasons
    }
}

class InputForm extends React.Component{
    render(){
        let{data,stationName,isFinish,isComplete,isLog,reasons} = this.props
        return(
            <View style={{flex:1}}>
                <NavBar
                    items = {[
                        {
                            type: NavBarTypes.BACK
                        },{
                            type: NavBarTypes.TITLE,
                            text: stationName+' Input Form'
                        }
                    ]}
                />
                <SectionList
                    style={{flex:1}}
                    sections = {data}
                    keyExtractor = {(item,index) => index+item}
                    renderItem = {({ item,section}) => 
                        section.isOperate == 1 ?
                        <Input
                            item = {item}
                            onChangeText = {text => {
                                this.props.setMeasureValue(section.id,item.id_measure,text)
                            }}
                            isDisable = {isFinish}
                        />
                        :
                        null
                    }
                    renderSectionHeader={({ section }) => {
                        let{id,title,data,isOperate,reason,notes} = section
                        return(
                            <View>
                                <View style = {{...styles.row_center,padding:sizeFigma(20),backgroundColor:Color.gray_light}}>
                                    <TextBold text = {title} style = {{flex:1}}/>
                                    {isFinish ? 
                                    <TextNormal text = {isOperate == 0 ? 'Not Operational' : null}/>
                                    :
                                    <Switch
                                        value = {isOperate == 1}
                                        onValueChange = { value => {
                                            if (!value) {
                                                showPicker('Pilih alasan unit tidak beroprasi',reasons,item => {
                                                    this.props.setUnitOperate(id,value)
                                                    this.props.setUnitReason(id, item.text)    
                                                })
                                            } else {
                                                this.props.setUnitOperate(id,value)
                                            }
                                        }}
                                        disabled = {isFinish}
                                    />
                                    }
                                </View>
                                {isOperate == 0 && 
                                <View style = {{padding:10}}>
                                    <TextBold text = {reason}/>
                                    <Line/>
                                    <TextInput
                                        style = {{paddingVertical:5,paddingHorizontal:10,flex:1,borderWidth:1,borderColor:Color.gray_light,marginTop:10,...styles.productsans_regular_14}}
                                        multiline = {true}
                                        // keyboardType = {'numeric'}
                                        onChangeText = {text => {
                                            this.props.setUnitNotes(id, text)
                                        }}
                                        placeholder = {'Tuliskan alasan'}
                                        value = {notes}
                                        editable = {!isFinish}
                                    />
                                </View>
                                }
                            </View>
                        )
                    }
                    }
                />
                {isComplete && !isFinish &&
                <Button
                    style = {{margin:10}}
                    type = {ButtonTypes.RED}
                    text = {'Stop Station'}
                    onPress = {()=>{
                        showAlert(
                            'Peringatan',
                            'Apakah anda yakin ingin menghentikan proses input? Input tidak bisa dirubah setelah proses dihentikan',
                            'Stop Station',
                            () => {
                                this.props.setStationEndDate()
                            }
                        )
                    }}
                />
                }
            </View>
        )
    }
}

function Input(props){
    let{item,onChangeText,isDisable} = props
    if(item.form_type == 'text') {
        return(
            <View>
                <TextNormal text = {item.measure_desc} style={{flex:2, marginVertical: sizeFigma(10), paddingHorizontal: sizeFigma(20)}}/>
                <TextInput
                    style = {{borderWidth:1,borderColor:isDisable?'white':Color.gray,paddingVertical:sizeFigma(5),paddingHorizontal:sizeFigma(20),borderRadius:sizeFigma(5),flex:1, marginHorizontal: sizeFigma(20), marginBottom: sizeFigma(10)}}
                    onChangeText = {onChangeText}
                    placeholder = {item.value?item.value+'':null}
                    value = {isDisable ? null : item.value}
                    editable = {!isDisable}
                />
                <Line/>
            </View>
        )
    }
    return(
        <View>
            <View style={{flexDirection:'row',paddingVertical:sizeFigma(10),paddingHorizontal:sizeFigma(20),alignItems:'center'}}>
                <TextNormal text = {item.measure_desc} style={{flex:2}}/>
                {/* <TextNormal text = {item.std} style={{flex:1}}/> */}
                <TextInput
                    style = {{borderWidth:1,borderColor:isDisable?'white':Color.gray,paddingVertical:5,paddingHorizontal:20,textAlign:'center',borderRadius:5,flex:1}}
                    keyboardType = {'numeric'}
                    onChangeText = {onChangeText}
                    placeholder = {item.value?item.value+'':null}
                    value = {isDisable ? null : item.value}
                    editable = {!isDisable}
                />
            </View>
            <Line/>
        </View>
    )
}

export default connect(mapStateToProps,{setInputForm,setMeasureValue,setStationEndDate,setUnitOperate,setUnitReason,setUnitNotes})(InputForm)