import React from 'react'
import {View,Text,FlatList,TouchableOpacity} from 'react-native'
import NavBar,{NavBarTypes} from '../Components/NavBar'
import {connect} from 'react-redux'
import ItemList from '../Components/ItemList'
import Button,{ButtonTypes} from '../Components/Button'
// import {startCycle} from '../Redux/Actions/CycleAction'
import ic_qr from '../Images/qr-code-scan.png'
import { navigate } from '../Utilities/RootNavigation'
import {startStation} from '../Redux/Actions/StationAction'
import {stopCycle} from '../Redux/Actions/CycleAction'
import {setIdStation,setCycleEndDate} from '../Redux/Actions/DataAction'
import GetLocation from '@react-native-community/geolocation'
import RNAndroidLocationEnabler from 'react-native-android-location-enabler'
import { hideLoading, showLoading } from '../Components/Loading'

const mapStateToProps = (state, ownProps) => {
    let cycleNumber = state.data.cycleNumber
    let isLog = state.data.batchLog ? true : false
    var batch
    if (isLog) {
        batch = state.data.batchLog
    } else {
        batch = state.data.batch
    }
    let stations = batch.cycle.find(item => item.number == cycleNumber).stations
    let refresh = state.data.refreshStation
    return {
        data: stations,
        isFinish: !isLog && stations.every(item => item.end_date!=undefined),
        isRefresh: refresh,
        isLog: isLog
    }
}

class Station extends React.Component{
    render(){
        let{data,isFinish,isRefresh,isLog} = this.props
        let navBarItems = [
            {
                type:NavBarTypes.BACK
            },{
                type:NavBarTypes.TITLE,
                text:'Station'
            }   
        ]
        if (!isLog) {
            navBarItems.push({
                type:NavBarTypes.RIGHT_BUTTON,
                icon:ic_qr,
                onPress:()=>{
                    navigate('Scanner')
                }
            })
        }
        return(
            <View style={{flex:1}}>
                <NavBar
                    items = {navBarItems}
                />
                <FlatList
                    style = {{flex:1}}
                    data = {data}
                    keyExtractor = {(item, index) => item+index+''}
                    extraData = {isRefresh}
                    renderItem = {({item,index}) => 
                        <ItemList
                            onPress = {()=>{
                                if (item.latitude && item.longitude) {
                                    this.props.setIdStation(item.id_station)
                                } else {
                                    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
                                        interval: 10000,
                                        fastInterval: 5000,
                                    })
                                    .then((data) => {
                                        // The user has accepted to enable the location services
                                        // data can be :
                                        //  - "already-enabled" if the location services has been already enabled
                                        //  - "enabled" if user has clicked on OK button in the popup
                                        showLoading()
                                        GetLocation.getCurrentPosition({
                                            enableHighAccuracy: true,
                                            timeout: 15000,
                                        })
                                        .then(location => {
                                            this.props.setIdStation(item.id_station,location)
                                        })
                                        .catch(error => {
                                            const { code, message } = error;
                                            console.warn('GET LOCATION',code, message);
                                        })
                                        .finally(() => {
                                            hideLoading()
                                        })
                                    })
                                    .catch((err) => {
                                        // The user has not accepted to enable the location services or something went wrong during the process
                                        // "err" : { "code" : "ERR00|ERR01|ERR02|ERR03", "message" : "message"}
                                        // codes :
                                        //  - ERR00 : The user has clicked on Cancel button in the popup
                                        //  - ERR01 : If the Settings change are unavailable
                                        //  - ERR02 : If the popup has failed to open
                                        //  - ERR03 : Internal error
                                    })
                                }
                            }}
                            leftText = {item.station_desc}
                            rightText = {isLog?'':item.end_date?'Done':item.start_date?'On Going':''}
                        />
                    }
                />
                {isFinish &&
                <Button
                    style = {{margin:10}}
                    type = {ButtonTypes.RED}
                    text = {'Stop Cycle'}
                    onPress = {()=>{
                        this.props.setCycleEndDate()
                        // this.props.stopCycle()
                    }}
                />
                }
            </View>
        )
    }
}

export default connect(mapStateToProps,{startStation,stopCycle,setIdStation,setCycleEndDate})(Station)