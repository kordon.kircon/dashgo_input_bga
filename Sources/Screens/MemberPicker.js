import React from 'react'
import {View, TouchableOpacity, Text, FlatList} from 'react-native'
import {connect} from 'react-redux'
import NavBar, { NavBarTypes } from '../Components/NavBar'
import ic_delete from '../Images/trash-2.png'
import Image from '../Components/Image'
import styles from '../Utilities/Style'
import { sizeFigma } from '../Utilities/Conversion'
import Input, { InputTypes } from '../Components/Input'
import Color from '../Utilities/Color'
import { deleteMember, setMember, setMemberImage } from '../Redux/Actions/LocalAction'
import imagePicker, {launchCam} from '../Components/ImagePicker'
import ic_camera from '../Images/camera.png'
import { createNativeWrapper } from 'react-native-gesture-handler'

const mapStateToProps = (state, ownProps) => {
    let {key} = state.local
    var currentItem = state.local[key]
    if(state.local.currentSubParent){
        currentItem = state.local.currentSubParent
    }
    let teamPickerItem = currentItem.display.find(it => it.type == 'teampicker')
    return {
        nav_bar_title: teamPickerItem?.nav_bar_title,
        pickerData: teamPickerItem?.answer?.data?.filter(it => !it.isSelected),
        data: teamPickerItem?.answer?.data?.filter(it => it.isSelected),
        imageMaxSize: currentItem?.template?.image.max_size,
        isViewOnly: state.local.currentSubParent
    }
}

class MemberPicker extends React.Component {
    constructor(){
        super()
        this.renderItem = this.renderItem.bind(this)
        this.renderFooter = this.renderFooter.bind(this)
    }

    renderItem({item, index}){
        let{pickerData, imageMaxSize, isViewOnly} = this.props
        return(
            <View style = {{...styles.card, ...styles.row_center, marginBottom: sizeFigma(10)}}>
                <View style = {{flex: 1}}>
                    <Text style = {{...styles.productsans_bold_14}}>Petugas {index+1}</Text>
                    <Text style = {{...styles.productsans_regular_14, marginTop: sizeFigma(10)}}>{item.nama}</Text>
                    {/* <Input
                        style = {{marginTop: sizeFigma(10)}}
                        type = {InputTypes.PICKER}
                        placeholder = {'Pilih Petugas'}
                        datas = {pickerData}
                        value = {item}
                    /> */}
                </View>
                <TouchableOpacity 
                    onPress={()=>{
                        launchCam(result => {
                            this.props.setMemberImage(item, result)
                        },imageMaxSize)
                    }}
                >
                    <View style = {{...styles.size_round_50, backgroundColor: Color.gray, marginLeft: sizeFigma(20), ...styles.center}}>
                        <Image
                            style = {styles.absolute}
                            uri = {item.image}
                        />
                        {!item.image &&
                        <Image
                            style = {styles.size_24}
                            source = {ic_camera}
                        />
                        }
                    </View>

                </TouchableOpacity>
                {!isViewOnly && 
                <TouchableOpacity
                    style = {{paddingLeft: sizeFigma(20)}}
                    onPress={()=>{
                        this.props.deleteMember(item)
                    }}
                >
                    <Image
                        style = {styles.size_24}
                        source = {ic_delete}
                    />
                </TouchableOpacity>
                }
            </View>
        )
    }

    renderFooter() {
        let{data, pickerData, isViewOnly} = this.props
        if(isViewOnly || pickerData.length == 0) return null
        return(
            <View style = {{...styles.card, ...styles.row_center, backgroundColor: Color.gray_light}}>
                <View style = {{flex: 1}}>
                    <Text style = {{...styles.productsans_bold_14}}>Petugas {data.length + 1}</Text>
                    <Input
                        style = {{marginTop: sizeFigma(10)}}
                        type = {InputTypes.PICKER}
                        placeholder = {'Pilih Petugas'}
                        datas = {pickerData}
                        onSelect = {item => {
                            this.props.setMember(item)
                        }}
                        value = {null}
                        isResetValue = {true}
                    />
                </View>
                {/* <Image
                    style = {{...styles.size_round_50}}
                    uri = {}
                /> */}
            </View>
        )
    }

    render(){
        let{nav_bar_title, data} = this.props
        return(
            <View style = {{flex: 1}}>
                <NavBar
                    items = {[
                        {
                            type: NavBarTypes.BACK
                        },{
                            type: NavBarTypes.TITLE,
                            text: nav_bar_title
                        }
                    ]}
                />
                <FlatList
                    style = {{flex: 1}}
                    data = {data}
                    keyExtractor={(item, index) => item + index}
                    contentContainerStyle = {{margin: sizeFigma(20)}}
                    renderItem={this.renderItem}
                    ListFooterComponent={this.renderFooter}
                />
            </View>
        )
    }
}

export default connect(mapStateToProps,{setMember, deleteMember, setMemberImage})(MemberPicker)