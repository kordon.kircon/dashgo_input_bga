import React from 'react'
import {View,Text,TextInput,TouchableOpacity,PermissionsAndroid,Linking} from 'react-native'
import Button,{ButtonTypes} from '../Components/Button'
import {connect} from 'react-redux'
import {login} from '../Redux/Actions/AuthAction'
import {menu} from '../Redux/Actions/DataAction'
import {showAlert, showSimpleAlert} from './Alert'
import ic_logo from '../Images/kisspng-bumitama-gunajaya-agro-bumitama-agri-oil-palms-pt-5b138dc3ba31c4.9235557615280081317627.png'
import Image from '../Components/Image'
import ic_email from '../Images/email.png'
import ic_password from '../Images/password.png'
import Color from '../Utilities/Color'
import RadioButton from '../Components/RadioButton'
import ic_pass_visible from '../Images/invisible.png'
import ic_pass_invisible from '../Images/visibility.png'
import { ScrollView } from 'react-native-gesture-handler'
import ReactNativeAN from 'react-native-alarm-notification'
import { sizeFigma } from '../Utilities/Conversion'
import styles from '../Utilities/Style'
// import RNFetchBlob from 'rn-fetch-blob'

const mapStateToProps = (state, ownProps) => {
    return {
        identity: state.data.identity
    }
}

class Login extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            username:'',
            password:'',
            isMorning:true,
        }
        this.login = this.login.bind(this)        
        this.initFileAccess = this.initFileAccess.bind(this)
    }

    componentDidMount(){
        this.initFileAccess()
    }

    async initFileAccess() {
        try {
            let granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
            ])
            
            console.log('permission value',granted[PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE] === PermissionsAndroid.RESULTS.GRANTED && 
            granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] === PermissionsAndroid.RESULTS.GRANTED)

            if (Object.values(granted).every(result => result == PermissionsAndroid.RESULTS.GRANTED)) {
                // let firedate = ReactNativeAN.parseDate(new Date(Date.now() + (2*60*1000)))
                // console.log(RNFetchBlob.fs.dirs.DocumentDir+'/persistStore.txt')
                this.props.menu()
            } else {
                console.log(granted)
                showAlert('Peringatan', 
                'Aplikasi tidak dapat digunakan bila izin akses file ditolak, silahkan nyalakan izin akses file di pengaturan untuk melanjutkan menggunakan aplikasi', 
                'Ke Pengaturan', 
                async ()=>{
                    this.setState({
                        goToSetting: true  
                    })
                    await Linking.openSettings()
                },()=>{
                    this.initFileAccess()
                },false)
            }
        } catch (err) {
            console.log('Login','initFileAccess','error',err)
        }
    }

    login(){
        let{username,password,isMorning} = this.state
        if(!username){
            showSimpleAlert('Warning','Email must be filled')
            return
        }
        if(!password){
            showSimpleAlert('Warning','Password must be filled')
            return
        }
        this.props.login(username,password)
    }

    render(){
        let{username,password} = this.state
        let{identity} = this.props
        var source = ic_logo
        var uri = null
        if(identity){
            uri = identity.logo
            source = null
        }
        return(
            <ScrollView style = {{flex:1}}>
                <View style={{alignItems:'center', padding:sizeFigma(20),paddingTop:sizeFigma(20)}}>
                    <Image
                        style={{width:sizeFigma(250),height:sizeFigma(250),marginBottom:sizeFigma(30)}}
                        source = {source}
                        uri = {uri}
                        resizeMode = {'contain'}
                    />
                    <Text style = {{...styles.productsans_bold_15, marginBottom:sizeFigma(20)}}>{identity?.nama_aplikasi}</Text>
                    <Input
                        placeholder = {'Email address'}
                        icon = {ic_email}
                        onChangeText = {text => {
                            this.setState({
                                username:text
                            })
                        }}
                        value = {username}
                        keyboardType = {'email-address'}
                    />
                    <Input
                        style = {{marginTop:sizeFigma(10)}}
                        placeholder = {'Password'}
                        icon = {ic_password}
                        secureTextEntry = {true}
                        onChangeText = {text => {
                            this.setState({
                                password:text
                            })
                        }}
                        value = {password}
                    />
                    <Button
                        style = {{width:'80%',marginTop:sizeFigma(40)}}
                        type = {ButtonTypes.GREEN}
                        text = {'Login'}
                        onPress = {this.login}
                    />
                    {/* <Text style={{fontSize:12,color:Color.gray,marginTop:20}}>Sign in with :</Text>
                    <Button
                        style = {{width:'100%',marginTop:10}}
                        type = {ButtonTypes.ORANGE}
                        text = {'Email ELBAB'}
                        onPress = {this.login}
                    /> */}
                </View>
            </ScrollView>
        )
    }
}

class Input extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            isFocus:false,
            isPassword:props.secureTextEntry,
            isSecure:props.secureTextEntry
        }
    }

    render(){
        let{placeholder,icon,style,onChangeText,secureTextEntry,value,keyboardType} = this.props
        let{isFocus,isPassword,isSecure} = this.state
        return(
            <View style={{width:'80%',...style}}>
                <Text style={{...styles.productsans_regular_14,color:Color.gray_dark}}>{isFocus?placeholder:null}</Text>
                <View style={{flexDirection:'row'}}>
                    <Image
                        style = {{...styles.size_24,marginTop:sizeFigma(5),marginBottom:sizeFigma(5),marginRight:sizeFigma(10), tintColor:isFocus?Color.gray_dark:Color.gray}}
                        source = {icon}
                    />
                    <TextInput
                        style = {{...styles.productsans_regular_14, flex:1,paddingTop:0,paddingBottom:0}}
                        placeholder = {isFocus?null:placeholder}
                        value = {value}
                        onFocus = {() => {
                            this.setState({
                                isFocus:true
                            })
                        }}
                        onBlur = {() => {
                            this.setState({
                                isFocus:false
                            })
                        }}
                        onChangeText = {onChangeText}
                        secureTextEntry = {isSecure}
                        keyboardType = {keyboardType}
                    />
                    {isPassword && 
                        <TouchableOpacity
                            onPress = { () => {
                                this.setState({
                                    isSecure : !isSecure
                                })
                            }}
                        >
                            <Image
                                style = {{width:24,height:24,marginTop:5,marginBottom:5,marginRight:10, tintColor:isFocus?Color.gray_dark:Color.gray}}
                                source = {isSecure?ic_pass_invisible:ic_pass_visible}
                            />
                        </TouchableOpacity>
                    }
                </View>
                <View style={{height:1,backgroundColor:isFocus?Color.blue:Color.gray}}/>
            </View>
        )
    }
}

export default connect(mapStateToProps,{login, menu})(Login)