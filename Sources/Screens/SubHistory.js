import React from 'react'
import {FlatList, View, Text, TouchableOpacity} from 'react-native'
import {connect} from 'react-redux'
import NavBar, { NavBarTypes } from '../Components/NavBar'
import { addZero, sizeFigma } from '../Utilities/Conversion'
import styles from '../Utilities/Style'
import Color from '../Utilities/Color'
import { goBack, navigate } from '../Utilities/RootNavigation'
import { setCurrentSub } from '../Redux/Actions/LocalAction'

const mapStateToProps = (state, ownProps) => {
    let current = state.local.currentSubParent
    return {
        nav_bar_title: current?.nav_bar_title + ' ' + current?.data.subs.length + ' Subs',
        data: current?.data.subs
    }
}

class SubHistory extends React.Component {
    constructor(){
        super()
        this.renderItem = this.renderItem.bind(this)
    }

    renderItem({item, index}) {
        return(
            <TouchableOpacity 
                style = {{...styles.card, marginBottom: sizeFigma(10)}}
                onPress = {()=>{
                    this.props.setCurrentSub(item)
                    navigate('QuestionForm')
                    // if(subText){
                    //     this.props.setCurrentSubParent(item)
                    //     navigate('SubHistory')
                    // }else{
                    //     if(tabIndex == 1){
                    //         this.props.setCurrentHistory(item)
                    //     }else{
                    //         this.props.setCurrentAct(item)
                    //     }
                    //     navigate('DynamicForm')
                    // }
                }}
            >
                <Text style = {{...styles.productsans_bold_14, flex: 1}}>{item.title} {addZero(index+1)}</Text>
                <Text style = {{...styles.productsans_regular_14, marginTop: sizeFigma(5)}}>{item.subtitle}</Text>
                <Text style = {{...styles.productsans_regular_12, marginTop: sizeFigma(5)}}>{item.duration}</Text>
            </TouchableOpacity>
        )
    }

    render(){
        let{nav_bar_title, data} = this.props
        let callback = this.props.route?.params?.callback
        return(
            <View style = {{flex: 1}}>
                <NavBar
                    items = {[
                        {
                            type: NavBarTypes.BACK,
                            onBack: ()=>{
                                if(callback){
                                    callback()
                                }
                                goBack()
                            }
                        },{
                            type: NavBarTypes.TITLE,
                            text: nav_bar_title
                        }
                    ]}
                />
                <FlatList
                    style = {{flex: 1}}
                    data = {data}
                    keyExtractor={(item, index) => item+index}
                    contentContainerStyle = {{padding: sizeFigma(20)}}
                    renderItem={this.renderItem}
                />
            </View>
        )
    }
}

export default connect(mapStateToProps, {setCurrentSub})(SubHistory)