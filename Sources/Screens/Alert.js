import React from 'react'
import Modal from 'react-native-modal'
import { View,Text,TextInput } from 'react-native'
import Color from '../Utilities/Color'
import Button,{ButtonTypes} from '../Components/Button'
import Image from '../Components/Image'
import { sizeFigma } from '../Utilities/Conversion'
import styles from '../Utilities/Style'
// import {launchCamera,launchLibrary} from '../Components/ImagePicker'

export const alertRef = React.createRef();

// export function showImagePicker(){

// }

export function hideAlert(onHide = null){
    alertRef.current?.hide(onHide)
}

export function showCustomAlert(title,customView,actionText,okAction,showCancel = true){
    alertRef.current?.showCustom(title,customView,actionText,okAction,showCancel)
}

export function showInputAlert(title,okAction){
    alertRef.current?.showInput(title,okAction)
}

export function showAlert(title,subtitle,actionText,okAction,cancelAction = null, showCancel = true){
    alertRef.current?.show(title,subtitle,actionText,okAction,cancelAction, showCancel)
}

export function showSimpleAlert(title,subtitle){
    alertRef.current?.showSimple(title,subtitle)
}

export default class Alert extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            isVisible:false,
            title:null,
            subtitle:null,
            actionText:null,
            okAction:null,
            isSimple:false,
            cancelAction:null,
            isInput:false,
            input:null,
            customView:null,
            showCancel:true,
            onHide:null,
        }
        this.hide = this.hide.bind(this)
        this.showSimple = this.showSimple.bind(this)
        this.showCustom = this.showCustom.bind(this)
        this.timeout = 2000
    }

    // showImagePicker(){

    // }

    showCustom(title,customView,actionText = 'Ok',okAction = null,showCancel = true){
        this.setState({
            title:title,
            customView:customView,
            actionText:actionText,
            okAction:okAction,
            isVisible:true,
            isSimple:false,
            showCancel:showCancel
        })
    }

    showInput(title,okAction){
        this.setState({
            customView:null,
            isVisible:true,
            isSimple:false,
            isInput:true,
            title:'',
            subtitle:title,
            actionText:'Submit',
            okAction:okAction,
            input:null,
            showCancel:true,
        })
    }

    show(title,subtitle,actionText,okAction,cancelAction,showCancel = true){
        this.setState({
            customView:null,
            isVisible:true,
            isSimple:false,
            isInput:false,
            title:title,
            subtitle:subtitle,
            actionText:actionText,
            okAction,okAction,
            cancelAction:cancelAction,
            showCancel:showCancel,
        })
    }

    showSimple(text,subtitle){
        this.setState({
            customView:null,
            isVisible:true,
            isSimple:true,
            isInput:false,
            title:text,
            subtitle:subtitle,
            showCancel:true
        })
    }

    hide(onHide = null){
        this.setState({
            isVisible:false,
            customView:null,
            onHide:onHide,
        })
    }

    // close(){
        
    //     // this.props.showAlert(false)
    // }

    render(){
        // let{isVisible,title,subtitle,actionText,okAction,isSimple} = this.props.alert
        let{isVisible,title,subtitle,actionText,okAction,isSimple,cancelAction,isInput,input,customView,showCancel,onHide} = this.state
        return(
            <Modal
                style = {{margin:sizeFigma(20)}}
                isVisible = {isVisible}
                onBackButtonPress = {()=>{this.hide(cancelAction)}}
                onBackdropPress = {()=>{this.hide(cancelAction)}}
                onModalHide = {()=>{
                    if(onHide && typeof onHide == 'function')onHide()
                    this.setState({
                        onHide:null,
                    })
                }}
            >
                <View style={{backgroundColor:Color.white,borderRadius:sizeFigma(10),paddingVertical:sizeFigma(27),paddingHorizontal: sizeFigma(20), ...styles.center}}>
                    {customView?
                    <View style = {{width:'100%'}}>
                        {typeof title == 'string' &&
                        <View style={{flexDirection:'row',alignItems:'center',marginBottom:14}}>
                            {/* <Image
                                style={{width:24,height:24,marginRight:5}}
                                source = {ic_info}
                            /> */}
                            <Text style={{fontFamily:'Roboto-Bold',fontSize:14,color:'white'}}>{title?title:null}</Text>
                        </View>
                        }
                        {customView()}
                    </View>
                    :
                    <View style = {{width:'100%'}}>
                        {typeof subtitle == 'function' ? 
                        subtitle()
                        :
                        typeof subtitle == 'string' ?
                        <Text style={{...styles.productsans_regular_14,color:Color.primary_blue}}>{subtitle?subtitle:null}</Text>
                        :
                        null
                        }
                        {isInput?
                        <TextInput
                            style={{borderWidth:1,borderRadius:3,borderColor:Color.gray,width:'100%'}}
                            onChangeText = {text => {
                                this.setState({
                                    input:text
                                })
                            }}
                            value = {input}
                        />
                        :null}
                    </View>
                    }
                    {isSimple?
                    null
                    :
                    <View style={{flexDirection:'row',marginTop:20,justifyContent:'flex-end'}}>
                        {isInput || !showCancel ?null:
                        <Button
                            style={{flex:1,marginRight:20}}
                            type = {ButtonTypes.BORDER_BOLD_40_RED}
                            text= {'Batal'}
                            onPress = {()=>{
                                // if(cancelAction)cancelAction()
                                this.hide(cancelAction)
                            }}
                        />
                        }
                        <Button
                            style={{flex:1}}
                            // type = {!showCancel?ButtonTypes.GRAY:ButtonTypes.RED}
                            type = {ButtonTypes.BORDER_BOLD_40_BLUE}
                            text= {actionText}
                            onPress = {()=>{
                                // if(okAction) this.action = okAction
                                this.hide(okAction)
                                // if(okAction){
                                //     if(isInput){
                                //         okAction(input)
                                //     }else{
                                //         okAction()
                                //     }
                                // }
                            }}
                        />
                    </View>
                    }
                </View>
            </Modal>
        )
    }
}

// export default connect(mapStateToProps,{showAlert})(Alert)