import React from 'react'
import {View, FlatList, Text, TouchableOpacity, PermissionsAndroid, AppState, Linking} from 'react-native'
import {connect} from 'react-redux'
import NavBar, {NavBarTypes} from '../Components/NavBar'
import { sizeFigma } from '../Utilities/Conversion'
import styles from '../Utilities/Style'
import RadioButton, {RadioButtonTypes} from '../Components/RadioButton'
import Input, { InputTypes } from '../Components/Input'
import Color from '../Utilities/Color'
import ic_camera from '../Images/camera.png'
import Image from '../Components/Image'
import Button, {ButtonTypes} from '../Components/Button'
import imagePicker, {launchCam} from '../Components/ImagePicker'
import { saveQuestionItem, setImage, setNeedAct, finishForm, saveForm, setCurrentSub, setImageData } from '../Redux/Actions/LocalAction'
import { showAlert, showSimpleAlert } from './Alert'
import { finishAct, saveQuestionAct, setActNeedAct, setImageAct, setImageDataAct } from '../Redux/Actions/ActAction'
import GPS from '../Components/GPS'

const mapStateToProps = (state, ownProps) => {
    let key = state.local.key
    var nav_bar_title, template, attention, image, current, question, imageDatas, multisave
    if(key && state.local[key]){
        let localItem = state.local[key]
        nav_bar_title = localItem.nav_bar_title
        template = localItem.template
        attention = localItem.attention
        image = localItem.image
        current = localItem.current
        question = localItem.question
        imageDatas = localItem.imageDatas   
        multisave = localItem.multisave
        // console.log('QuestionForm','mapStateToProps','CURRENT',current)
        // console.log('QuestionForm','mapStateToProps','QUESTION',question)
        // console.log('QuestionForm','mapStateToProps','MULTISAVE',multisave)
    }
    var isViewOnly = false
    var needAct = current?.need_act
    var currentItem
    var isAct = false
    if(state.local.currentHistory){
        currentItem = state.local.currentHistory
        isViewOnly = true
        needAct = currentItem.data.need_act
    }
    if(state.act.current){
        currentItem = state.act.current
        needAct = currentItem.need_act
        isViewOnly = currentItem.finish
        isAct = true
        multisave = false
    }
    if(state.local.currentSub){
        currentItem = state.local.currentSub
        isViewOnly = true
        needAct = currentItem.data.need_act
    }
    if(currentItem){
        nav_bar_title = currentItem.nav_bar_title
        question = currentItem.question
        attention = currentItem.attention
        imageDatas = currentItem.image_list
        image = currentItem.image
        current = true
        multisave = false
    }

    return {
        attentionDatas: attention,
        datas: question,
        navBarTitle: 'Detail ' + nav_bar_title,
        image: image,
        imageDatas: imageDatas,
        needAct: needAct,
        isViewOnly: isViewOnly,
        isAct: isAct,
        multisave: multisave
    }
}

class QuestionForm extends React.Component {

    constructor() {
        super()
        this.state = {
            reload : false,
            location: null
        }
        this.renderItem = this.renderItem.bind(this)
        this.renderRadio = this.renderRadio.bind(this)
        this.renderMultiple = this.renderMultiple.bind(this)
        this.renderNumber = this.renderNumber.bind(this)
        this.renderEssay = this.renderEssay.bind(this)
        this.renderFooter = this.renderFooter.bind(this)
        this.renderGPS = this.renderGPS.bind(this)
        this.showCompleteMessage = this.showCompleteMessage.bind(this)
    }

    componentWillUnmount() {
        this.props.setCurrentSub(null)
    }

    renderGPS() {
        return(
            <GPS
                onSuccess = {position => {
                    this.setState({
                        location: position.coords.latitude+', '+position.coords.longitude
                    })
                }}
                onError = {error => {
                    this.setState({
                        location: null
                    })
                }}
            />
        )
    }

    renderEssay(item, index) {
        var value
        if(item.answer && item.answer.length > 0){
            value = item.answer[0].opsi
        }
        let{isViewOnly} = this.props
        return(
            <View style = {{marginBottom: sizeFigma(20)}}>
                <Text style = {{...styles.productsans_bold_14, marginBottom: sizeFigma(5)}}>{index+1}.&ensp;{item.question}</Text>
                <Input
                    value = {value}
                    onChangeText = {text => {
                        if(!item.answer){
                            item.answer = [{opsi:''}]
                        }
                        item.answer[0].opsi = text
                    }}
                    isDisabled = {isViewOnly}
                />
            </View>
        )
    }

    renderNumber(item, index) {
        let{isViewOnly} = this.props
        var value
        if(item.answer && item.answer.length > 0){
            value = item.answer[0].opsi
        }
        return(
            <View style = {{marginBottom: sizeFigma(20)}}>
                <Text style = {{...styles.productsans_bold_14, marginBottom: sizeFigma(5)}}>{index+1}.&ensp;{item.question}</Text>
                <Input
                    keyboardType = {'numeric'}
                    value = {value}
                    onChangeText = {text => {
                        if(!item.answer){
                            item.answer = [{opsi:''}]
                        }
                        item.answer[0].opsi = text
                    }}
                    isDisabled = {isViewOnly}
                />
            </View>
        )
    }

    renderMultiple(item, index) {
        let{reload} = this.state
        let{isViewOnly, isAct} = this.props
        return(
            <View style = {{marginBottom: sizeFigma(20)}}>
                <Text style = {{...styles.productsans_bold_14, marginBottom: sizeFigma(5)}}>{index+1}.&ensp;{item.question}</Text>
                {item.answer_list.map(it => {
                    var isOnItem
                    if(item.answer){
                        isOnItem = item.answer.find(ct => ct.opsi == it.opsi)
                    }
                    return(
                        <View key = {Math.random()*1000}>
                            <RadioButton
                                type = {RadioButtonTypes.SQUARE}
                                text = {it.opsi}
                                onToggle = {isOn => {
                                    if(isAct){
                                        this.props.saveQuestionAct(item.id, it, isOn)
                                    }else{
                                        this.props.saveQuestionItem(item, it, isOn)
                                    }
                                }}
                                isOn = {isOnItem}
                                isDisabled = {isViewOnly}
                            />
                            {isOnItem && it.nanya == 1 && 
                                <View style = {{marginTop: sizeFigma(5)}}>
                                    <Text style = {{...styles.productsans_regular_14, marginBottom: sizeFigma(5)}}>Jelaskan jawaban kamu</Text>
                                    <Input
                                        value = {isOnItem.desc}
                                        onChangeText = {text => {
                                            isOnItem.desc = text
                                        }}
                                        isDisabled = {isViewOnly}
                                    />
                                </View>
                            }
                        </View>
                    )
                }
                )}
            </View>
        )
    }

    renderRadio(item, index) {
        let{reload} = this.state
        let{datas, isViewOnly, isAct} = this.props
        // let isNanya = item.answer_list.find(it => it.nanya == 1 && it.isOn)
        return(
            <View style = {{marginBottom: sizeFigma(20)}}>
                <Text style = {{...styles.productsans_bold_14, marginBottom: sizeFigma(5)}}>{index+1}.&ensp;{item.question}</Text>
                {item.answer_list.map(it => {
                    let isOn = item.answer && item.answer.length > 0 && item.answer[0].opsi == it.opsi
                    return(
                        <View key = {Math.random()*1000}>
                            <RadioButton
                                text = {it.opsi}
                                onToggle = {isOn => {
                                    if(it.nanya == 0 && item.answer && item.answer.length > 0){
                                        item.answer[0].desc = ''
                                    }
                                    if(isAct){
                                        this.props.saveQuestionAct(item.id, it)
                                    }else{
                                        this.props.saveQuestionItem(item, it)
                                    }
                                }}
                                isOn = {isOn}
                                isDisabled = {isViewOnly}
                            />
                            {isOn && it.nanya == 1 && 
                                <View style = {{marginTop: sizeFigma(5)}}>
                                    <Text style = {{...styles.productsans_regular_14, marginBottom: sizeFigma(5)}}>Jelaskan jawaban kamu</Text>
                                    <Input 
                                        value = {item.answer[0].desc}
                                        onChangeText = {text => {
                                            item.answer[0].desc = text
                                            // this.props.saveQuestionItem(item, it, text)
                                        }}
                                        isDisabled = {isViewOnly}
                                    />
                                </View>
                            }
                        </View>
                    )
                }
                )}
            </View>
        )
    }

    renderItem({item, index}) {
        switch(item.type){
            case 'radio':
                return this.renderRadio(item, index)
            case 'multiple':
                return this.renderMultiple(item, index)
            case 'number':
                return this.renderNumber(item, index)
            case 'essay':
                return this.renderEssay(item, index)
            default :
                return null
        }
    }

    showCompleteMessage() {
        let{datas, imageDatas} = this.props
        let isQuestionValid = datas.findIndex(item => item.required == 1 && !item.answer) == -1
        let isImageValid = imageDatas.findIndex(item => item.answer && !item.data) == -1
        if(!isQuestionValid){
            showSimpleAlert(null,'Lengkapi jawaban terlebih dahulu')    
        }else if(!isImageValid){
            showSimpleAlert(null,'Lengkapi data gambar terlebih dahulu')    
        }else{
            showSimpleAlert(null,'Pilih tindak lanjut terlebih dahulu')
        }
    }

    renderFooter(){
        let{attentionDatas, image, imageDatas, needAct, isViewOnly, isAct, multisave, datas} = this.props
        let{reload, location} = this.state
        let isImageValid = imageDatas.findIndex(item => item.answer && !item.data) == -1
        let isQuestionValid = datas.findIndex(item => item.required == 1 && !item.answer) == -1
        let isValid = needAct != undefined && isImageValid && isQuestionValid
        if(!attentionDatas?.answer || !imageDatas){
            return null
        }
        return(
            <View style = {{paddingTop: sizeFigma(20)}}>
                <View
                    style = {{height: sizeFigma(1), backgroundColor: Color.gray_dark, marginBottom: sizeFigma(20)}}
                />
                <View style = {{marginBottom: sizeFigma(10)}}>
                    <Text style = {{...styles.productsans_bold_12, marginBottom: sizeFigma(5)}}>GPS</Text>
                    <View style = {{...styles.input_border, borderColor: Color.gray_light}}>
                        <Text style = {{borderColor: Color.gray_light}}>{location}</Text>
                    </View>
                </View>

                <Text style = {{...styles.productsans_bold_14, marginBottom: sizeFigma(5)}}>{attentionDatas.question}</Text>
                {attentionDatas.answer.map(it => 
                    <View key = {Math.random() * 1000}>
                        <RadioButton
                            text = {it.opsi}
                            onToggle = {isOn => {
                                if(isAct){
                                    this.props.setActNeedAct(it.value)
                                }else{
                                    this.props.setNeedAct(it.value)
                                }
                            }}
                            isOn = {needAct == it.value}
                            isDisabled = {isViewOnly}
                        />
                    </View>
                )}
                {imageDatas.map((it,index) => 
                    <View key = {'image'+index}>
                        <View 
                            style = {{backgroundColor: Color.gray_light, borderRadius: sizeFigma(10), height: sizeFigma(150), marginTop: sizeFigma(10), overflow: 'hidden', flexDirection: 'row-reverse'}}
                        >
                            <Image
                                style = {styles.absolute}
                                uri = {it.answer ? it.answer : it.image_url ? it.image_url : null}
                                isZoom = {true}
                            />
                            {!isViewOnly &&
                            <TouchableOpacity 
                                style = {{...styles.size_round_50, ...styles.center, backgroundColor: Color.orange, borderRadius: sizeFigma(10)}}
                                onPress={ ()=>{
                                    if(!isViewOnly){
                                        launchCam(result => {
                                            if(isAct){
                                                this.props.setImageAct(it.index, result)
                                            }else{
                                                this.props.setImage(it, result)
                                            }
                                        },image.max_size)
                                    }
                                } }
                            >
                                <Image
                                    style = {{...styles.size_24, tintColor: 'white'}}
                                    source = {ic_camera}
                                />
                            </TouchableOpacity>
                            }
                        </View>
                        {(it.answer || it.image_url) &&
                        <Input
                            type = {InputTypes.PICKER}
                            datas = {datas}
                            value = {it.data}
                            textKey = {'question'}
                            onSelect = {dt => {
                                if(isAct){
                                    this.props.setImageDataAct(it.index, dt)
                                }else{
                                    this.props.setImageData(it, dt)
                                }
                            }}
                            isDisabled = {isViewOnly}
                        />
                        }
                    </View>
                )}
                {!isViewOnly && 
                <View style = {{marginTop: sizeFigma(20), flexDirection: 'row'}}>
                    {multisave &&
                    <Button
                        style = {{flex: 1, marginRight: sizeFigma(10)}}
                        type = {isValid ? ButtonTypes.ORANGE : ButtonTypes.DISABLE}
                        text = {'Simpan & Input Lagi'}
                        onPress = {()=>{
                            if(isValid){
                                this.props.saveForm(location)
                            }else{
                                this.showCompleteMessage()
                            }
                        }}
                    />
                    }
                    <Button
                        style = {{flex: 1}}
                        type = {isValid ? ButtonTypes.GREEN : ButtonTypes.DISABLE}
                        text = {'Simpan & Akhiri'}
                        onPress = {()=>{
                            if(isValid){
                                if(isAct){
                                    this.props.finishAct(location)
                                }else{
                                    this.props.finishForm(location, multisave)
                                }
                            }else{
                                this.showCompleteMessage()
                            }
                            // if(multisave){
                            //     this.props.finishForm(location)
                            // }else{
                                
                            // }
                        }}
                    />
                </View>
                }
            </View>
        )
    }

    render(){
        let{navBarTitle, datas, attentionDatas} = this.props
        let{reload} = this.state
        return(
            <View style = {{flex: 1}}>
                {this.renderGPS()}
                <NavBar
                    items = {[
                        {
                            type: NavBarTypes.BACK
                        }, {
                            type: NavBarTypes.TITLE,
                            text: navBarTitle
                        }
                    ]}
                />
                <FlatList
                    style = {{flex: 1}}
                    data={datas}
                    // extraData={attentionDatas.answer}
                    keyExtractor={(item, index) => item+index}
                    contentContainerStyle = {{padding: sizeFigma(20)}}
                    renderItem={this.renderItem}
                    ListFooterComponent={this.renderFooter}
                />
            </View>
        )
    }
}

export default connect(mapStateToProps , 
    {
        saveQuestionItem, 
        setNeedAct, 
        setImage, 
        finishForm, 
        setActNeedAct,
        setImageAct,
        saveQuestionAct,
        finishAct,
        saveForm,
        setCurrentSub,
        setImageData,
        setImageDataAct
    })(QuestionForm)