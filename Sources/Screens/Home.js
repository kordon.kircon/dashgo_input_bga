import React from 'react'
import {View, Text, TouchableOpacity, FlatList, RefreshControl} from 'react-native'
import {connect} from 'react-redux'
import Image from '../Components/Image'
import { sizeFigma } from '../Utilities/Conversion'
import Color from '../Utilities/Color'
import styles from '../Utilities/Style'
import ic_logout from '../Images/log-out.png'
import {logout} from '../Redux/Actions/AuthAction'
import {menu, getTemplate} from '../Redux/Actions/DataAction'
import { navigate } from '../Utilities/RootNavigation'
import { deleteForm, initMenu, setCurrentHistory, setCurrentSubParent, uploadForm } from '../Redux/Actions/LocalAction'
import Button, { ButtonTypes } from '../Components/Button'
import { deleteAct, initAct, setCurrentAct, uploadAct } from '../Redux/Actions/ActAction'
import TabBar from '../Components/TabBar'

const mapStateToProps = (state, ownProps) => {
    // console.log('Home','mapStateToProps',state.local.history[state.auth.idRole])
    return {
        identity: state.data.identity,
        menuData: state.data.template,
        name: state.auth.loginData?.name,
        history: state.local.history[state.auth.idRole],
        actData: state.act.data
    }
}

class Home extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            tabIndex: 0,
            refreshing: false,
        }
        this.renderHeader = this.renderHeader.bind(this)
        this.renderItem = this.renderItem.bind(this)
        this.renderHistoryItem = this.renderHistoryItem.bind(this)
    }

    renderHeader() {
        let{name, identity} = this.props
        let{menuData, history} = this.props
        var datas = menuData
        if(datas && datas.length % 2 != 0) {
            datas.push({})
        }
        return(
            <View>
                <Image
                    style = {{width: sizeFigma(57), height: sizeFigma(57), alignSelf: 'center'}}
                    uri =  {identity.logo}
                />
                <View style = {{backgroundColor: Color.gray_alpha, borderRadius: sizeFigma(8), padding: sizeFigma(20), ...styles.row_center, marginTop: sizeFigma(10)}}>
                    <View style = {{flex: 1}}>
                        <Text style = {{...styles.productsans_regular_14}}>Selamat Datang</Text>
                        <Text style = {{...styles.productsans_bold_18, marginTop: sizeFigma(10)}}>{name}</Text>
                    </View>
                    <TouchableOpacity
                        onPress={()=>{
                            this.props.logout()  
                        }}
                    >

                        <Image
                            style = {{width: sizeFigma(24), height: sizeFigma(24)}}
                            source = {ic_logout}
                        />
                    </TouchableOpacity>
                </View>
                <FlatList
                    style = {{flex: 1, marginTop: sizeFigma(20)}}
                    data = {datas}
                    keyExtractor={ (item, index) => item+index }
                    // ListHeaderComponent={this.renderHeader}
                    contentContainerStyle = {{padding: sizeFigma(20)}}
                    renderItem={this.renderItem}
                    numColumns={2}
                />
                <TabBar
                    style = {{marginBottom: sizeFigma(20)}}
                    items = {[
                        {
                            text: 'Need Action'
                        }, {
                            text: 'Data Input & Sync'
                        }
                    ]}
                    onTabChange = {idx => {
                        this.setState({
                            tabIndex: idx
                        })
                    }}
                />
                {/* <Text style = {{...styles.productsans_bold_14, marginTop: sizeFigma(10), marginBottom: sizeFigma(10)}}>Sinkronisasi & History</Text> */}
            </View>
        )
    }

    renderItem({item, index}) {
        var marginRight = 0
        var marginLeft = 0
        if(index%2 == 0){
            marginRight = sizeFigma(5)
        }else{
            marginLeft = sizeFigma(5)
        }
        if(!item){ return <View style = {{flex: 1}}/> }
        return(
            <TouchableOpacity 
                style = {{flex: 1, ...styles.card, backgroundColor: item.bgcolor, marginLeft: marginLeft, marginRight: marginRight, paddingBottom: sizeFigma(30), marginBottom: sizeFigma(20)}}
                onPress = {()=>{
                    this.props.initMenu(item.menu_tag, item.data_template)
                    navigate('DynamicForm')
                }}
            >
                <Text style = {{...styles.productsans_bold_18, color: item.txtcolor}}>{item.title}</Text>
                <Text style = {{...styles.productsans_regular_14, color: item.txtcolor}}>{item.sub_title}</Text>
            </TouchableOpacity>
        )
    }

    renderHistoryItem({item, index}) {
        let{tabIndex} = this.state
        let isUploaded = item.isDataUploaded && item.isImageUploaded
        var extraStyle = {}
        if(isUploaded){
            extraStyle = {
                backgroundColor: Color.gray_light
            }
        }
        var isShowAction = false
        if(tabIndex == 1){
            isShowAction = !isUploaded
        }else{
            isShowAction = !isUploaded && item.finish
        }
        var isDeleteButtonEnabled = true
        if(item.isDataUploaded && !item.isImageUploaded){
            isDeleteButtonEnabled = false
        }
        var subText
        if(item.data?.subs){
            subText = item.data.subs.length + 'Subs'
        }
        return(
            <TouchableOpacity 
                style = {{...styles.card, marginBottom: sizeFigma(10), ...extraStyle}}
                onPress = {()=>{
                    if(subText){
                        this.props.setCurrentSubParent(item)
                    }else if(tabIndex == 1){
                        this.props.setCurrentHistory(item)
                    }else{
                        this.props.setCurrentAct(item)
                    }
                    navigate('DynamicForm')
                }}
            >
                <View style = {{...styles.row_center}}>
                    <Text style = {{...styles.productsans_bold_14, flex: 1}}>{item.title}</Text>
                    {subText && 
                    <Button
                        style = {{paddingHorizontal: sizeFigma(10)}}
                        type = {ButtonTypes.ORANGE}
                        text = {subText}
                        onPress = {()=>{
                            this.props.setCurrentSubParent(item)
                            navigate('SubHistory',{callback: ()=>{
                                this.props.setCurrentSubParent(null)
                            }})
                        }}
                    />
                    }
                </View>
                <Text style = {{...styles.productsans_regular_14, marginTop: sizeFigma(5)}}>{item.subtitle}</Text>
                <Text style = {{...styles.productsans_regular_12, marginTop: sizeFigma(5)}}>{item.duration}</Text>
                {isShowAction &&
                <View style = {{marginTop: sizeFigma(10), flexDirection: 'row'}}>
                    {isDeleteButtonEnabled && 
                    <Button
                        style = {{flex: 1}}
                        type = {ButtonTypes.RED}
                        text = {'Hapus'}
                        onPress = {()=>{
                            if(tabIndex == 1){
                                this.props.deleteForm(index)
                            }else{
                                this.props.deleteAct(index)
                                this.props.initAct()
                            }
                        }}
                    />
                    }
                    <Button
                        style = {{flex: 1, marginLeft: sizeFigma(10)}}
                        type = {ButtonTypes.GREEN}
                        text = {'Unggah'}
                        onPress = {()=>{
                            if(tabIndex == 1){
                                this.props.uploadForm(index)
                            }else{
                                this.props.uploadAct(index)
                            }
                        }}
                    />
                </View>
                }
            </TouchableOpacity>
        )
    }

    render(){
        let{menuData, history, actData} = this.props
        let{tabIndex, refreshing} = this.state
        var datas = actData
        if(tabIndex == 1){
            datas = history
        }
        return(
            <FlatList
                style = {{flex: 1}}
                data = {datas}
                keyExtractor={ (item, index) => item+index+item.isDataUploaded+item.isImageUploaded }
                ListHeaderComponent={this.renderHeader}
                contentContainerStyle = {{padding: sizeFigma(20)}}
                renderItem={this.renderHistoryItem}
                refreshControl={
                    <RefreshControl
                        refreshing = {refreshing}
                        onRefresh={()=>{
                            this.setState({
                                refreshing: true
                            })
                            this.props.initAct(()=>{
                                this.setState({
                                    refreshing: false
                                })
                            })
                        }}
                    />
                }
            />
        )
    }
}

export default connect(mapStateToProps, 
    {
        logout, 
        menu, 
        getTemplate, 
        initMenu, 
        deleteForm, 
        uploadForm, 
        setCurrentHistory, 
        initAct,
        setCurrentAct,
        uploadAct,
        deleteAct,
        setCurrentSubParent
    })(Home)