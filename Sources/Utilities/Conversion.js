import {Dimensions} from 'react-native'
let{width} = Dimensions.get('window')

export function copy(value) {
    if(!value) return value
    return JSON.parse(JSON.stringify(value))
}

export function valueToColor(value){
    if(value<70)return '#ff2900'
    if(value<81)return '#e3e30e'
    if(value<91)return '#0ee312'
    return '#0e98e3'
}

export function stringToNumber(value){
    if(!value)return 0
    if(typeof value == 'number') return Number(value.toFixed(2))
    var result = Number(value)
    if(isNaN(result)) return 0
    return Number(result.toFixed(2)) 
}

export function dateToJSONDate(date){
    let dateString = date.toISOString()
    return {
        date:dateStringToApiFormat(dateString),
        pretty:dateStringToPretty(dateString),
        raw:dateString
    }
}

export function onlyDateApiFormat(date){
    if (!date) {
        date = new Date()
    }
    let month = addZero(date.getMonth()+1) 
    let mDate = addZero(date.getDate()) 
    return date.getFullYear()+'-'+month+'-'+mDate
}

export function dateStringToApiFormat(dateInput){
    var date
    if (!dateInput) {
        date = new Date()
    }else{
        date = new Date(dateInput)
    }
    let month = addZero(date.getMonth()+1) 
    let mDate = addZero(date.getDate()) 
    return mDate+'/'+month+'/'+date.getFullYear()+' '+dateToTimePretty(date, false)
}

export function dateToPretty(date){
    let dateString = date.toISOString()
    return dateStringToPretty(dateString)
}

export function dateToTimePretty(date, withSpace = true){
    var hours
    if(date.getHours()<10){
        hours = '0'+date.getHours()
    }else{
        hours = date.getHours()
    }
    var minutes
    if(date.getMinutes()<10){
        minutes = '0'+date.getMinutes()
    }else{
        minutes = date.getMinutes()
    }
    var seconds
    if(date.getSeconds()<10){
        seconds = '0'+date.getSeconds()
    }else{
        seconds = date.getSeconds()
    }
    let bridge = withSpace ? ' : ' : ':'
    return hours+bridge+minutes+bridge+seconds
}

export function dateStringToPretty(dateString){
    let date = new Date(dateString)
    var month
    switch (date.getMonth()) {
        case 0:
            month = 'Januari'
            break;
        case 1:
            month = 'Februari'
            break;
        case 2:
            month = 'Maret'
            break;
        case 3:
            month = 'April'
            break;
        case 4:
            month = 'Mei'
            break;
        case 5:
            month = 'Juni'
            break;
        case 6:
            month = 'Juli'
            break;
        case 7:
            month = 'Agustus'
            break;
        case 8:
            month = 'September'
            break;
        case 9:
            month = 'Oktober'
            break;
        case 10:
            month = 'Nopember'
            break;
        default:
            month = 'Desember'
            break;
    }
    return date.getDate()+' '+month+' '+date.getFullYear()
}

export function addZero(input){
    if(input<10){
        return '0'+input
    }else{
        return ''+input
    }
}

export function sizeFigma(size) {
    return (width/360) * size
}

export function isValidImage(image) {
    if(!image) return false
    return image.includes('http:') || image.includes('file:') || image.includes('https:')
}