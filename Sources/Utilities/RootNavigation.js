import * as React from 'react';

export const navigationRef = React.createRef();

export function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}

export function goBack(){
    navigationRef.current?.goBack()
}

export function reset(index, routes){
  navigationRef.current?.reset({
    index:index,
    routes:routes
  })
}

export function addListener(onFocus){
  return navigationRef.current?.addListener('focus',onFocus)
}