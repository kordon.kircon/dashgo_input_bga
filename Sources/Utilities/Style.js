import {StyleSheet} from 'react-native'
import Color from './Color'
import { sizeFigma } from './Conversion'

export default styles = StyleSheet.create({
    absolute: {
        position: 'absolute',
        left: 0, right: 0, bottom: 0, top: 0
    },
    card: {
        backgroundColor: Color.green_light,
        borderRadius: sizeFigma(10),
        padding: sizeFigma(10),
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    image_16: {
        width: sizeFigma(16),
        height: sizeFigma(16)
    },
    image_24: {
        width: sizeFigma(24),
        height: sizeFigma(24)
    },
    image_48: {
        width: sizeFigma(48),
        height: sizeFigma(48)
    },
    input_border: {
        borderRadius: sizeFigma(5), 
        borderWidth: sizeFigma(1), 
        borderColor: Color.inputBorder, 
        padding: sizeFigma(5), 
        flexDirection: 'row',
        alignItems: 'center'
    },
    productsans_bold_12: {
        fontFamily: 'ProductSans-Bold',
        fontSize: sizeFigma(12),
        color: Color.textBlack
    },
    productsans_bold_14: {
        fontFamily: 'ProductSans-Bold',
        fontSize: sizeFigma(14),
        color: Color.textBlack
    },
    productsans_bold_15: {
        fontFamily: 'ProductSans-Bold',
        fontSize: sizeFigma(15),
        color: Color.textBlack
    },
    productsans_bold_17: {
        fontFamily: 'ProductSans-Bold',
        fontSize: sizeFigma(17),
        color: Color.textBlack
    },
    productsans_bold_18: {
        fontFamily: 'ProductSans-Bold',
        fontSize: sizeFigma(18),
        color: Color.textBlack
    },
    productsans_regular_12: {
        fontFamily: 'ProductSans-Regular',
        fontSize: sizeFigma(12),
        color: Color.textBlack
    },
    productsans_regular_14: {
        fontFamily: 'ProductSans-Regular',
        fontSize: sizeFigma(14),
        color: Color.textBlack
    },
    row_center: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    size_24: {
        width: sizeFigma(24),
        height: sizeFigma(24)
    },
    size_round_8: {
        width: sizeFigma(8),
        height: sizeFigma(8),
        borderRadius: sizeFigma(4)
    },
    size_round_18: {
        width: sizeFigma(18),
        height: sizeFigma(18),
        borderRadius: sizeFigma(9)
    },
    size_round_50: {
        width: sizeFigma(50),
        height: sizeFigma(50),
        borderRadius: sizeFigma(25),
        overflow: 'hidden'
    }
})