const Color = {
    navBarBg:'#1e1e1e',
    bg:'white',
    textBlack:'#585858',
    textButtonVeryDark:'#757575',
    bgContent:'#202020',
    red:'#cc2a2a',
    green:'#23a335',
    green_glow:'#2acc40',
    green_light: '#E6F8E6',
    gray_dark:'#505050',
    gray:'#808080',
    gray_light:'#e0dede',
    gray_alpha: '#F1F1F180',
    textPlaceholder: '#bcbcbc',
    yellow:'#ffc233',
    blue:'#338AFF',
    orange:'#FF9C33',
    inputBorder: '#666666',
    white: '#FFFFFF'
}
export default Color