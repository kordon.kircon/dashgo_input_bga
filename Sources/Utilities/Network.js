import {Alert} from 'react-native'

import Snackbar from 'react-native-snackbar'
import NetInfo from "@react-native-community/netinfo"
import {store} from '../Redux/index'
import {showSimpleAlert, showAlert} from '../Screens/Alert'
import {showLoading,hideLoading} from '../Components/Loading'
import RNFetchBlob from 'rn-fetch-blob'

// const BASE_URL = 'https://millcontrol.bumitama.com/v2/api/'
const BASE_URL = 'http://dashgo.mgt.technology/api/'
// const BASE_URL = 'http://bpsmobiletest.bumitama.com:8081/expgeninspec/api/'

export function post(url,body,isLoading,isMultipart,isRaw,isDisableError){
    return NetInfo.fetch()
    .then(state => {
      if(state.isConnected) return doPost('POST',BASE_URL,url,body,isLoading,isMultipart,isRaw,isDisableError)
      else showSimpleAlert('Error Aplikasi','Terjadi kesalahan koneksi internet')
    })
}

async function doPost(method,baseUrl,url,body,isLoading,isMultipart,isRaw,isDisableError){
    if(isLoading) showLoading()
    let idRole = store.getState().auth.idRole
    let userId = store.getState().auth.userId
    var formBody
    if(!isRaw) {
      if(isMultipart){
        // formBody = []
        // formBody.push({name:'timestamp', data:new Date().getTime().toString()})
        // formBody.push({name:'id_role', data:idRole})
        // formBody.push({name:'id_user', data:userId})
        // if(body){
        //   for(let [key,value] of Object.entries(body)){
        //     if(Array.isArray(value)){
        //       for(var i=0; i< value.length; i++){
        //         formBody.push({name:key+'['+i+']', data:value[i]})
        //       }
        //     }else{
        //       if(value.uri){
        //         formBody.push({name: key, filename:value.name, data: RNFetchBlob.wrap(value.uri.replace("file:///",""))})
        //       }else{
        //         formBody.push({name: key, data: value})
        //       }
        //     }
        //   }
        // }


        formBody = new FormData()
        formBody.append('timestamp',new Date().getTime().toString())
        formBody.append('id_role',idRole)
        formBody.append('id_user',userId)
        if(body){
          for(let [key,value] of Object.entries(body)){
            if(Array.isArray(value)){
              for(var i=0; i< value.length; i++){
                formBody.append(key+'['+i+']',value[i])
              }
            }else{
              formBody.append(key,value)
            }
          }
        }
      }else{
        formBody = []
        formBody.push('timestamp='+new Date().getTime())
        formBody.push('id_role='+idRole)
        formBody.push('id_user='+userId)
        if(body){
            for(var property in body){
              if(Array.isArray(body[property])){
                let list = body[property]
                for(var i=0; i< list.length; i++){
                  var encodedKey = encodeURIComponent(property)+'['+i+']'
                  var encodedValue = encodeURIComponent(list[i])
                  formBody.push(encodedKey+'='+encodedValue)
                }
              }else{
                var encodedKey = encodeURIComponent(property)
                var encodedValue = encodeURIComponent(body[property])
                formBody.push(encodedKey+'='+encodedValue)
              }
            }
        }
        formBody = formBody.join('&')
      }
    } else {
      formBody = JSON.stringify(body)
    }


    // let contentType = isMultipart?'multipart/form-data':'application/json'
    


    // console.log('FORM BODY',baseUrl+url,formBody)
    let contentType = isRaw?'application/json':isMultipart?'multipart/form-data':'application/x-www-form-urlencoded'
    if(isMultipart){
      return fetch(baseUrl+url, {
          method: method,
          headers: {
              'Accept':'application/json',
              'Content-Type':contentType,
              'Authorization':'Bearer ' + store.getState().auth.accessToken,
              'X-Timezone':'Asia/Jakarta',
              'X-localization':'id',
          },
          body: isMultipart?formBody:JSON.stringify(formBody),
      })
      .then(async response => {
        if(url == 'loger') {
          return
        }
        let text = await response.text()
        // console.log('POST RESPONSE',text)
        if(response.status >= 500 || response.status == 408){
          post('loger',{code: response.status, url: baseUrl+url, error_message: text, body: body?JSON.stringify(body):null})
          if (!isDisableError) showSimpleAlert('Error Aplikasi','Terjadi kesalahan pada aplikasi, coba ulangi kembali')
          return
        }
        if(text.includes("<div style") || text.includes('<html')){
          showAlert(null, 'Terjadi kesalahan pada server','Kirim Error',()=>{
            post('loger',{code: response.status, url: baseUrl+url, error_message: text, body: body?JSON.stringify(body):null})
          })
          return
        }
        return text
      })
      .then(responseText => {
        if(!responseText)return
        if(responseText.includes("<div style")){
          return
        }else if(responseText.includes("<")){
          return
        }
        let json = JSON.parse(responseText)
        return json
      })
      .then(responseJson => {
        if(!responseJson) return 
        if (responseJson.result) {
          if (responseJson.result == 'success' || responseJson.result == 'succes') {
            return responseJson.data
          }
          // if (responseJson.result.message) {
          //   return {}
          // }
          return responseJson.result
        }
        if(responseJson.status) return responseJson.data
        if (!isDisableError) showSimpleAlert(responseJson.result,responseJson.message)      
        return
      })
      .catch(error => {
        // console.log('Error POST', error)
        // if (!isDisableError) showSimpleAlert('Error',error)      
      })
      .finally(()=>{
        if(isLoading) hideLoading()
      })
    }
    
    return RNFetchBlob.config({
      trusty:true
    }).fetch(method,baseUrl+url,{
      'Accept':'application/json',
      'Content-Type':contentType
    },formBody)
    .then(async response => {
      if(url == 'loger') {
        return
      }
      let text = await response.text()
      // console.log('POST RESPONSE',text)
      if(response.status >= 500 || response.status == 408){
        post('loger',{code: response.status, url: baseUrl+url, error_message: text, body: body?JSON.stringify(body):null})
        if (!isDisableError) showSimpleAlert('Error Aplikasi','Terjadi kesalahan pada aplikasi, coba ulangi kembali')
        return
      }
      if(text.includes("<div style") || text.includes('<html')){
        showAlert(null, 'Terjadi kesalahan pada server','Kirim Error',()=>{
          post('loger',{code: response.status, url: baseUrl+url, error_message: text, body: body?JSON.stringify(body):null})
        })
        return
      }
      return text
    })
    .then(responseText => {
      if(!responseText)return
      if(responseText.includes("<div style")){
        return
      }else if(responseText.includes("<")){
        return
      }
      let json = JSON.parse(responseText)
      return json
    })
    .then(responseJson => {
      if(!responseJson) return 
      if (responseJson.result) {
        if (responseJson.result == 'success' || responseJson.result == 'succes') {
          return responseJson.data
        }
        if (responseJson.result.message) {
          return {}
        }
        return responseJson.result
      }
      if(responseJson.status) return responseJson.data
      if (!isDisableError) showSimpleAlert(responseJson.result,responseJson.message)      
      return
    })
    .catch(error => {
      // console.log('Error POST', error)
      // if (!isDisableError) showSimpleAlert('Error',error)      
    })
    .finally(()=>{
      if(isLoading) hideLoading()
    })
}

export function get(url,params,isLoading,isReturnRoot){
    return NetInfo.fetch()
    .then(state => {
      if(state.isConnected) return doGet(BASE_URL,url,params,isLoading,isReturnRoot)
      else showSimpleAlert('Error Aplikasi','Terjadi kesalahan koneksi internet')
    })
}

export function generateGetUrl(url,params){
  return getUrlWithQueryParams(BASE_URL+url,params)
}

export function futch(url, opts={}, onProgress) {
  return new Promise( (res,rej) =>{
    var xhr = new XMLHttpRequest()
    xhr.open(opts.method || 'get', url,true)
    for (var k in opts.headers||{})
        xhr.setRequestHeader(k, opts.headers[k])
    xhr.onload = e => res(e.target.responseText)
    xhr.onerror = e => rej(e)
    if (xhr.upload && onProgress)
        xhr.upload.onprogress = onProgress // event.loaded / event.total * 100 ; //event.lengthComputable
    xhr.send(opts.body)
});
}

async function doGet(baseUrl,url,params,isLoading,isReturnRoot){
  if(isLoading) showLoading()
  if(!params) params = {}
  let idRole = store.getState().auth.idRole
  let userId = store.getState().auth.userId
  params.id_user = userId
  params.id_role = idRole
  url = baseUrl+url
  url = getUrlWithQueryParams(url,params)
  // console.log('GET URL',url)
  let header = {
    'Accept':'application/json',
    'Content-Type':'application/x-www-form-urlencoded',
  }
  return RNFetchBlob.config({
    trusty:true
  }).fetch('GET',url,header)
  .then(async response => {
    let header = await response.headers
    let text = await response.text()
    // console.log('RAW GET',text)
    if(response.status >= 500 || response.status == 408){
      post('loger',{code: response.status, url: url, error_message: text})
      showSimpleAlert('Error Aplikasi','Terjadi kesalahan pada aplikasi, coba ulangi kembali')
      return
    }
    if(text.includes("<div style") || text.includes('<html')){
      showAlert(null, 'Terjadi kesalahan pada server','Kirim Error',()=>{
        post('loger',{code: response.status, url: url, error_message: text})
      })
      return
    }
    return text
  })
  .then(responseText => {
    if(!responseText)return
    let json = JSON.parse(responseText)
    return json
  })
  .then(responseJson => {
    // console.log('RESPONSE JSON',url,responseJson)
    if(params && params.page) return responseJson
    if(!responseJson)return
    if(isReturnRoot) return responseJson
    if(responseJson.result) return responseJson.result
    if(responseJson.status) return responseJson.data
    else{
      showSimpleAlert('Error',responseJson.message)
      return
    }
  })
  .catch(error => {
    if(Object.keys(error).length != 0){
      showSimpleAlert('Error',JSON.stringify(error))      
    }else{

      showSimpleAlert('Error',error)      
    }
    
  })
  .finally(()=>{
    if(isLoading) hideLoading()
  })
}

function getUrlWithQueryParams(url,params){
    url = url.replace(' ','%20')
    if(params){
      if(!url.endsWith('?')) url = url+'?'
      var formBody = []
      for(var property in params){
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(params[property]);
        formBody.push(encodedKey+'='+encodedValue)
      }
      formBody = formBody.join('&')
      url = url+formBody
    }
    return url
}