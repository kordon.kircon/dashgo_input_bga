import { CLEAR, LOGIN, LOGOUT, SHIFT } from "../Constant"
import {reset} from '../../Utilities/RootNavigation'
import { post } from "../../Utilities/Network"
import { checkVersion, getTemplate } from "./DataAction"
import { initAct } from "./ActAction"

export const logout = () => dispatch => {
    dispatch({
        type:LOGOUT
    })
    return reset(0,[{name:'Login'}])
    dispatch({
        type:LOGOUT
    })
    return dispatch({
        type:CLEAR
    })
}

export const login = (username,password) => dispatch => {
    return post('users/authenticate',{username:username,password:password},true)
    .then(response => {
        if(!response) return
        // console.log('AuthAction','login',response)
        // return
        dispatch({
            type:LOGIN,
            payload:response
        })
        dispatch(initAct())
        dispatch(getTemplate(()=>{
            return reset(0,[{name:'Home'}])
        }))
        // dispatch(checkVersion())
    })
}