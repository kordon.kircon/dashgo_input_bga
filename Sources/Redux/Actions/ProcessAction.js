import {CURRENT_STORY, INIT_CYCLE, LOGOUT, SHIFT, START_PROCESS, STOP_PROCESS} from "../Constant"
import { get } from "../../Utilities/Network"
import { dateStringToPretty,dateStringToApiFormat,dateToJSONDate } from "../../Utilities/Conversion"
import {reset,navigate} from '../../Utilities/RootNavigation'
import { initStation } from "./StationAction"
import { logout } from "./AuthAction"
import { setCycleKey } from "./CycleAction"

export const startProcess = () => dispatch => {
    let date = dateToJSONDate(new Date()) 
    dispatch({
        type:START_PROCESS,
        payload:date
    })
    dispatch({
        type:INIT_CYCLE,
        payload:date.raw
    })
    dispatch(initStation())
    return navigate('Cycle')
}

export const stopProcess = () => dispatch => {
    let date = dateToJSONDate(new Date()) 
    dispatch({
        type:STOP_PROCESS,
        payload:date
    })
    dispatch(logout())
    return reset(0,[{name:'Login'}])
}

export const setShift = (isMorning) => dispatch => {
    return dispatch({
        type:SHIFT,
        payload:isMorning
    })
}

export const setCurrentStory = (date) => dispatch => {
    dispatch({
        type:CURRENT_STORY,
        payload:date
    })
    dispatch(setCycleKey(date.raw))
    return navigate('Cycle')

}