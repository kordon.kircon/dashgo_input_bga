import { showSimpleAlert } from "../../Screens/Alert"
import { post } from "../../Utilities/Network"
import { goBack, reset } from "../../Utilities/RootNavigation"
import { 
    INIT_MENU, 
    SAVE_FORM_ITEM, 
    CREATE_FORM, 
    CANCEL_FORM, 
    SAVE_QUESTION_ITEM, 
    SET_NEED_ACT, 
    CREATE_TEMPORARY, 
    SET_IMAGE, 
    FINISH_FORM, 
    DELETE_FORM, 
    SET_FORM_ID, 
    SET_SUCCESS_UPLOAD_IMAGE, 
    SET_CURRENT_HISTORY,
    SAVE_FORM,
    SET_CURRENT_SUB_PARENT,
    SET_CURRENT_SUB,
    SET_MEMBER,
    DELETE_MEMBER,
    SET_MEMBER_IMAGE,
    UPLOAD_MEMBER,
    SET_IMAGE_DATA
} from "../Constant"

export const setMemberImage = (item, image) => dispatch => {
    dispatch({
        type: SET_MEMBER_IMAGE,
        item: item,
        payload: image
    })
}

export const deleteMember = (item) => dispatch => {
    dispatch({
        type: DELETE_MEMBER,
        payload: item
    })
}

export const setMember = (item) => dispatch => {
    dispatch({
        type: SET_MEMBER,
        payload: item
    })
}

export const setCurrentSub = (item) => dispatch => {
    dispatch({
        type: SET_CURRENT_SUB,
        payload: item
    })
}

export const setCurrentSubParent = (item) => dispatch => {
    dispatch({
        type: SET_CURRENT_SUB_PARENT,
        payload: item
    })
}

export const setCurrentHistory = (item) => dispatch => {
    dispatch({
        type: SET_CURRENT_HISTORY,
        payload: item
    })
}

export const deleteForm = (index) => (dispatch, getState) => {
    dispatch({
        type: DELETE_FORM,
        payload: index,
        idRole: getState().auth.idRole
    })
}

export const uploadImage = (index) => (dispatch, getState) => {
    let idRole = getState().auth.idRole
    let history = getState().local.history[idRole]
    let historyItem = history[index]
    if(historyItem.isImageUploaded){ return }
    var image, id
    if(historyItem.data.subs){
        let subItem = historyItem.data.subs.find(it => !it.isImageUploaded)
        id = subItem.id
        image = subItem.image_list.find(it => !it.isUploaded)
    }else{
        id = historyItem.id
        image = historyItem.image_list.find(it => !it.isUploaded)
    }
    let api = historyItem.image.image_post
    let param = {menu_tag: historyItem.key, id: id, images: image.answer, id_question: image.data.id, question: image.data.question}
    post(api,param,true,true)
    .then(response => {
        if(!response) return
        if(response.status){
            dispatch({
                type: SET_SUCCESS_UPLOAD_IMAGE,
                idRole: idRole,
                index: index,
                payload: image.index,
                id: id
            })
            if(getState().local.history[idRole][index].isImageUploaded){
                showSimpleAlert(null,response.message)
            }else{
                dispatch(uploadImage(index))
            }
        }else{
            showSimpleAlert(null, response.message)
        }
    })

}

export const uploadMember = (index) => (dispatch, getState) => {
    let idRole = getState().auth.idRole
    let history = getState().local.history[idRole]
    let historyItem = history[index]
    let teamPickerItem = historyItem.display.find(it => it.type == 'teampicker')
    let memberItem = teamPickerItem.answer.data.find(it => it.isSelected && it.image && !it.isUploaded)
    if(!memberItem){
        dispatch(uploadImage(index))
        return
    }
    let param = {
        id_patroli: historyItem.id,
        nik: memberItem.nik,
        images: memberItem.image,
        employee_name: memberItem.nama,
        id_team: teamPickerItem.answer.id,
        team_name: teamPickerItem.answer.nama
    }
    post(teamPickerItem.image_post, param, true, true)
    .then(response => {
        if(!response) return
        dispatch({
            type: UPLOAD_MEMBER,
            index: index,
            idRole: idRole,
            payload: memberItem.id
        })
        dispatch(uploadMember(index))
    })
}

export const uploadForm = (index) => (dispatch, getState) => {
    let idRole = getState().auth.idRole
    let history = getState().local.history[idRole]
    let historyItem = history[index]

    if(historyItem.isDataUploaded){
        if(historyItem.isMemberUploaded){
            dispatch(uploadImage(index))
        }else{
            dispatch(uploadMember(index))
        }
        return
    }
    let param = historyItem.data
    param.display = historyItem.display
    post(historyItem.api, historyItem.data, true, false, true)
    .then(response => {
        if(!response) return
        dispatch({
            type: SET_FORM_ID,
            idRole: idRole,
            index: index,
            payload: response
        })
        if(getState().local.history[idRole][index].isImageUploaded && getState().local.history[idRole][index].isMemberUploaded){
            showSimpleAlert(null, "Data berhasil diupload")
        }else if(getState().local.history[idRole][index].isMemberUploaded){
            dispatch(uploadImage(index))
        }else{
            dispatch(uploadMember(index))
        }
    })
}

export const saveForm = (location) => (dispatch, getState) => {
    let idRole = getState().auth.idRole
    dispatch({
        type: SAVE_FORM,
        payload: location,
        idRole: idRole
    })
}

export const finishForm = (location, multisave) => (dispatch, getState) => {
    if(multisave){
        dispatch(saveForm(location))
    }
    let idRole = getState().auth.idRole
    dispatch({
        type: FINISH_FORM,
        payload: location,
        idRole: idRole
    })
    goBack()
}

export const setImageData = (item, data) => dispatch => {
    dispatch({
        type: SET_IMAGE_DATA,
        item: item,
        payload: data
    })
}

export const setImage = (item, answer) => dispatch => {
    dispatch({
        type: SET_IMAGE,
        item: item,
        payload: answer
    })
}

export const createTemporary = () => dispatch => {
    dispatch({
        type: CREATE_TEMPORARY
    })
}

export const setNeedAct = value => dispatch => {
    dispatch({
        type: SET_NEED_ACT,
        payload: value
    })
}

export const saveQuestionItem = (item, answer, value) => dispatch => {
    dispatch({
        type: SAVE_QUESTION_ITEM,
        item: item,
        value: value,
        payload: answer
    })
}

export const cancelForm = () => dispatch => {
    dispatch({
        type: CANCEL_FORM
    })
}

export const createForm = () => (dispatch, getState) => {
    let idRole = getState().auth.idRole
    let userId = getState().auth.userId
    dispatch({
        type: CREATE_FORM,
        idRole: idRole,
        userId: userId
    })
}

export const saveFormItem = (item, answer) => (dispatch, getState) => {
    let {key} = getState().local
    dispatch({
        type: SAVE_FORM_ITEM,
        key: key,
        item: item,
        payload: answer
    })
}

export const initMenu = (key, payload) => (dispatch) => {
    dispatch({
        type: INIT_MENU,
        key: key,
        payload: payload
    })
}