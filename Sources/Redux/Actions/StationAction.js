import { CURRENT_STATION, INIT_STATION, START_STATION, STOP_STATION, REFRESH_STATION, SET_STATION_KEY } from "../Constant"
import { initInputForm } from "./InputFormAction"
import { dateToJSONDate } from "../../Utilities/Conversion"
import { navigate } from "../../Utilities/RootNavigation"
import {showSimpleAlert} from '../../Screens/Alert'

export const setStationKey = (key) => dispatch => {
    return dispatch({
        type:SET_STATION_KEY,
        payload:key
    })
}

export const initStation = () => (dispatch,getState) => {
    let cycle = getState().cycle
    let key = cycle.key+cycle.currentId
    return dispatch({
        type:INIT_STATION,
        payload:key
    })
}

export const setCurrentStation = (id) => dispatch => {
    return dispatch({
        type:CURRENT_STATION,
        payload:id
    })
}

export const startStation = (item) => (dispatch,getState) => {
    let isFinish = getState().process.currentStory.isFinish
    if(!isFinish && (!item.startDate && !item.endDate)){
        if(!(getState().station[getState().station.key].every(_item => !(_item.startDate && !_item.endDate)))){
            showSimpleAlert('Peringatan','On Going Station harus dibereskan terlebih dahulu sebelum lanjut input ke station yang lain')
            return
        }
    }
    dispatch(setCurrentStation(item.id))
    dispatch(refreshStation())
    if(!item.startDate){
        if(!isFinish){
            let date = dateToJSONDate(new Date())
            dispatch({
                type:START_STATION,
                payload:date
            })
        }
        dispatch(initInputForm())
    }
    return navigate('InputForm')
}

export const stopStation = () => (dispatch,getState) => {
    let date = dateToJSONDate(new Date())
    dispatch(refreshStation())
    return dispatch({
        type:STOP_STATION,
        payload:date
    })
}

export const refreshStation = () => dispatch => {
    return dispatch({
        type:REFRESH_STATION
    })
}