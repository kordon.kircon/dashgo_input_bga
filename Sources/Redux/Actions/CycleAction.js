import { dateToJSONDate } from "../../Utilities/Conversion"
import { navigate, goBack } from "../../Utilities/RootNavigation"
import { CURRENT_CYCLE, REFRESH_CYCLE, START_CYCLE, STOP_CYCLE, ADD_CYCLE, SET_CYCLE_KEY } from "../Constant"
import { initStation, setStationKey } from "./StationAction"

export const setCycleKey = (key) => dispatch => {
    return dispatch({
        type:SET_CYCLE_KEY,
        payload:key
    })
}

export const refreshCycle = () => dispatch => {
    return dispatch({
        type:REFRESH_CYCLE
    })
}

export const stopCycle = () => (dispatch,getState) => {
    let date = dateToJSONDate(new Date())
    dispatch(refreshCycle())
    dispatch({
        type:STOP_CYCLE,
        payload:date
    })
    return goBack()
}

export const setCurrentCycle = (id) => dispatch => {
    return dispatch({
        type:CURRENT_CYCLE,
        payload:id
    })
}

export const startCycle = (item) => (dispatch,getState) => {
    dispatch(setCurrentCycle(item.id))
    if(!item.startDate){
        let date = dateToJSONDate(new Date())
        dispatch({
            type:START_CYCLE,
            payload:date
        })
    }
    dispatch(setStationKey(getState().cycle.key+item.id))
    return navigate('Station')
}

export const addCycle = () => dispatch => {
    dispatch({
        type:ADD_CYCLE
    })
    dispatch(initStation())
    return navigate('Station')
}