import {Image} from 'react-native'
import { showSimpleAlert } from "../../Screens/Alert"
import { get, post } from "../../Utilities/Network"
import { reset } from "../../Utilities/RootNavigation"
import {doupdate} from '../Actions/DataAction'
import { CANCEL_ACT, INIT_ACT, SET_ACT_NEED_ACT, SET_CURRENT_ACT, START_ACT, SET_IMAGE_ACT, SAVE_QUESTION_ACT, FINISH_ACT, SET_ACT_ID, SET_SUCCESS_UPLOAD_IMAGE_ACT, DELETE_ACT, SET_IMAGE_DATA_ACT, SAVE_FORM_ITEM_ACT } from "../Constant"

export const doActLog = () => (dispatch) => {
    // dispatch(doupdate())
}

export const saveFormItemAct = (item, answer) => (dispatch, getState) => {
    dispatch({
        type: SAVE_FORM_ITEM_ACT,
        item: item,
        payload: answer
    })
}

// export const saveFormAct = (location) => (dispatch, getState) => {
//     let idRole = getState().auth.idRole
//     dispatch({
//         type: SAVE_FORM,
//         payload: location,
//         idRole: idRole
//     })
// }

export const deleteAct = (index) => (dispatch) => {
    dispatch({
        type: DELETE_ACT,
        payload: index
    })
}

export const uploadActImage = (index) => (dispatch, getState) => {
    let item = getState().act.data[index]
    if(item.isImageUploaded) { return }
    let imageItem = item.image_list.find(it => it.answer && !it.isUploaded)
    // console.log('ActAction','imageItem',imageItem)
    post(item.image.image_post, {menu_tag: item.key, id: item.id, images: imageItem.answer, ticket_no: item.ticket_no, act_no: item.act_no, id_question: imageItem.data.id, question: imageItem.data.question},true,true)
    .then(response => {
        if(!response)return
        if(response.status){
            dispatch({
                type: SET_SUCCESS_UPLOAD_IMAGE_ACT,
                index: index,
                payload: imageItem.index
            })
            if(getState().act.data[index].isImageUploaded){
                showSimpleAlert(null,response.message)
            }else{
                dispatch(uploadActImage(index))
            }
        }else{
            showSimpleAlert(null, response.message)
        }
    })
}

export const uploadAct = (index) => (dispatch, getState) => {
    let item = getState().act.data[index]
    if(item.isDataUploaded){
        dispatch(uploadActImage(index))
        return
    }
    post(item.api, item, true, false, true)
    .then(response => {
        if(!response) return
        dispatch({
            type: SET_ACT_ID,
            index: index,
            payload: response.id
        })
        if(getState().act.data[index].isImageUploaded){
            showSimpleAlert(null,'Data berhasil diunggah')
        }else{
            dispatch(uploadActImage(index))
        }
    })
}

export const finishAct = (location) => (dispatch, getState) => {
    let idRole = getState().auth.idRole
    let userId = getState().auth.userId
    dispatch({
        type: FINISH_ACT,
        idRole: idRole,
        userId: userId,
        payload: location
    })
    reset(0,[{name: 'Home'}])
}

export const saveQuestionAct = (id, answer, value) => dispatch => {
    dispatch({
        type: SAVE_QUESTION_ACT,
        id: id,
        value: value,
        payload: answer
    })
}

export const setImageDataAct = (index, data) => dispatch => {
    dispatch({
        type: SET_IMAGE_DATA_ACT,
        index: index,
        payload: data
    })
}

export const setImageAct = (index, answer) => dispatch => {
    dispatch({
        type: SET_IMAGE_ACT,
        index: index,
        payload: answer
    })
}

export const setActNeedAct = value => dispatch => {
    dispatch({
        type: SET_ACT_NEED_ACT,
        payload: value
    })
}

export const cancelAct = () => dispatch => {
    dispatch({
        type: CANCEL_ACT
    })
}

export const startAct = (location) => dispatch => {
    dispatch({
        type: START_ACT,
        payload: location
    })
}

export const setCurrentAct = item => dispatch => {
    dispatch({
        type: SET_CURRENT_ACT,
        payload: item
    })
}

export const initAct = (callback) => dispatch => {
    get('act')
    .then(response => {
        if(!response) return
        dispatch({
            type: INIT_ACT,
            payload: response.data
        })
        if(callback){
            callback()
        }
        let preFetchTasks = []
        response.images.forEach((url)=>{
            preFetchTasks.push(Image.prefetch(url))
        })

        Promise.all(preFetchTasks).then((results)=>{
            try {
                let downloadedAll = true;
                results.forEach((result)=>{
                    if(!result){
                        //error occurred downloading a pic
                        downloadedAll = false
                    }
                })
                if(!downloadedAll){
                    post('loger',{code: '', url: 'imagedownloadact', error_message: 'image not all downloaded', body: ''})
                }
            }catch(e){
                post('loger',{code: '', url: 'imagedownloadact', error_message: JSON.stringify(e), body: ''})
            }
        })
    })
}