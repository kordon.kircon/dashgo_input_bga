import { INIT_INPUT_FORM, SET_INPUT_FORM } from "../Constant"
import { stopStation } from "./StationAction"

export const initInputForm = () => (dispatch,getState) => {
    let station = getState().station
    let key = station.key+station.currentId
    return dispatch({
        type:INIT_INPUT_FORM,
        payload:key
    })
}

export const setInputForm = (id,value) => (dispatch,getState) => {
    let station = getState().station
    if(!station[station.key].find(item => item.id == station.currentId).endDate){
        let inputForm = getState().inputform
        if(inputForm[inputForm.key].every(item => item.id == id || (item.value != undefined && item.value != null))){
            dispatch(stopStation())
        }
    }
    return dispatch({
        type:SET_INPUT_FORM,
        id:id,
        payload:value
    })
}