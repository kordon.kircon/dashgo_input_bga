import Snackbar from "react-native-snackbar"
import { batch } from "react-redux"
import { showAlert, showSimpleAlert } from "../../Screens/Alert"
import { dateStringToApiFormat, onlyDateApiFormat } from "../../Utilities/Conversion"
import { get, post } from "../../Utilities/Network"
import { goBack, navigate } from "../../Utilities/RootNavigation"
import { SET_TEMPLATE, SET_VERSION, SET_BATCH, SET_CYCLE_NUMBER, SAVE_BATCH, SET_BATCH_LOG, SET_ID_STATION, REFRESH_STATION, REFRESH_CYCLE, SET_ALARM, IDENTITY, MENU } from "../Constant"
import { logout } from "./AuthAction"
import ReactNativeAN from 'react-native-alarm-notification'
import {doActLog} from '../Actions/ActAction'

export const doupdate = () => (dispatch,getState) => {
    let maxTry = 10
    // for(var i=0; i<maxTry; i++){
    //     if(getState().data?.identity.is_update == 1){
    //         dispatch(updateMime())
    //     }
    // }
}

export const menu = () => dispatch => {
    get('identity/menu')
    .then(response => {
        if(!response) return
        dispatch({
            type: MENU,
            payload: response
        })
    })
}

export const identity = () => (dispatch, getState) => {
    get('identity',null,true)
    .then(response => {
        if(!response)return
        dispatch({
            type: IDENTITY,
            payload: response
        })
        if(response.is_update == 1){
            // dispatch(doActLog())
        }
    })
}

export const updateMime = () => (dispatch, getState) => {
    if(getState().data.identity.is_update == 1){
        // dispatch(doActLog())
    }
}

export const scanQrCode = (data,location) => (dispatch,getState) => {
    let datas = data.split("_")
    var idStation
    if (datas.length > 1) {
        idStation = datas[datas.length-1]
    } else {
        goBack()
        return showSimpleAlert('Peringatan', 'Format QRCode tidak dikenali')
    }
    let batch = getState().data.batch
    let cycleNumber = getState().data.cycleNumber
    let station = getStation(batch,cycleNumber,idStation)
    goBack()
    if (station) {
        return dispatch(setIdStation(idStation,location))
    } else {
        return showSimpleAlert('Peringatan', 'Station tidak ditemukan')
    }
}

export const stopAlarm = () => (dispatch,getState) => {
    ReactNativeAN.stopAlarmSound()
    ReactNativeAN.removeAllFiredNotifications()
    ReactNativeAN.getScheduledAlarms()
    .then(response => {
        response.forEach(alarm => {
            ReactNativeAN.deleteAlarm(alarm.id)
        })
    })
    return
}

export const setAlarm = () => (dispatch,getState) => {
    let intervalData = getState().data.template.interval[0]
    const alarmNotifData = {
        title: "Peringatan",
        message: "Segera selesaikan input station",
        channel: "dashgoinput_channel_id",
        small_icon: "ic_launcher",
        has_button: true,
        sound_name:'alarm.mp3',
        schedule_type: 'repeat',
        repeat_interval: 'minutely',
        interval_value: parseInt(intervalData.reminder),
        volume:1,
        vibrate: true,
        use_big_text: true
    }
    let alarm = getState().data.alarm
    var interval = parseInt(intervalData.interval_cycle_1)*60*1000
    if (alarm) {
        interval = parseInt(intervalData.interval_cycle_n)*60*1000
    }
    ReactNativeAN.getScheduledAlarms()
    .then(response => {
        response.forEach(alarm => {
            ReactNativeAN.deleteAlarm(alarm.id)
            ReactNativeAN.removeFiredNotification(alarm.id)
            ReactNativeAN.stopAlarmSound()
        })
    })
    ReactNativeAN.stopAlarmSound()
    ReactNativeAN.removeAllFiredNotifications()
    let fireDate = ReactNativeAN.parseDate(new Date(Date.now() + interval))
    return ReactNativeAN.scheduleAlarm({ ...alarmNotifData, fire_date: fireDate })
    .then(response=> {
        if (!response) return   
        dispatch({
            type:SET_ALARM,
            payload:response
        })
    })    
}

export const checkVersion = () => (dispatch, getState) => {
    return get('templatev2/version',null,true)
    .then(response => {
        if(!response) return
        dispatch({
            type: SET_VERSION,
            payload: response.current_version
        })
        if (response.current_version != 0) {
            dispatch(getTemplate())
        } else {
            showSimpleAlert('Peringatan', response.message)
        }
    })
}

export const getTemplate = (onFinish) => dispatch => {
    return get('templatev2',null,true)
    .then(response => {
        if(!response) return
        dispatch({
            type:SET_TEMPLATE,
            payload:response
        })
        if(onFinish) {
            onFinish()
        }
    })
}

export const startBatch = () => (dispatch,getState) => {
    let template = getState().data.template
    let userId = getState().auth.userId
    let shift = getState().process.isMorning ? 'pagi' : 'malam'
    if (!template) {
        return showAlert('Peringatan','Data template tidak ditemukan, silahkan coba download ulang template', 'Download Template',()=>{
            dispatch(getTemplate())
        })
    }
    let result = {
        id_user : userId,
        id_template : template.id_template,
        id_mill : template.template.id_mill,
        shift : shift,
        tgl_report : onlyDateApiFormat(),
        start_batch : dateStringToApiFormat(),
        cycle : []
    }

    dispatch(setBatch(result))
    return dispatch(addCycle())

    // return navigate('Cycle')
}

export const addCycle = () => (dispatch,getState) => {
    let template = getState().data.template
    let batch = getState().data.batch
    let number = 1
    let cycle = batch.cycle
    let cycleLength = cycle.length
    if (cycleLength > 0) {
        number = cycle[cycleLength-1].number + 1
    }
    let stations = template.template.stations.map(station => {
        let units = station.input_form.map(inputForm => {
            let measures = inputForm.measure_input.filter(input => {
                return input.available == 1
            }).map(input => {
                return {
                    id_measure : input.id_measure,
                    measure_desc : input.measure_desc,
                    std : input.std,
                    form_type: input.form_type
                }
            })
            return {
                id_unit : inputForm.id_unit,
                unit_desc : inputForm.unit_desc,
                measures : measures,
                is_operate : 1,
            }
        })
        return {
            id_station : station.id_station,
            station_desc : station.station_desc,
            units : units
        }
    })
    cycle.push({
        number : number,
        start_date : dateStringToApiFormat(),
        stations : stations
    })
    dispatch(setAlarm())
    return dispatch(setBatch(batch))
}

export const setCycleNumber = (number) => dispatch => {
    dispatch({
        type: SET_CYCLE_NUMBER,
        payload: number
    })
    return navigate('Station')
}

export const checkLog = () => (dispatch,getState) => {
    let batchHistory = getState().data.batchHistory
    batchHistory.forEach(item => {
        if (!item.isSent) {
            post('batch/upload',item,false,false,true,true)
            .then(response => {
                if (response) {
                    item.isSent = true
                    dispatch({
                        type: SAVE_BATCH,
                        payload: batchHistory
                    })
                }
            })
        }
    })
}

export const saveBatch = () => (dispatch,getState) => {
    let batch = getState().data.batch
    let date = dateStringToApiFormat()
    batch.end_batch = date
    batch.cycle.forEach(item => {
        if (item.start_date == undefined) {
            item.start_date = date
        }
        if (item.end_date == undefined) {
            item.end_date = date
        }
        item.stations.forEach(item => {
            if (item.start_date == undefined) {
                item.start_date = date
            }
            if (item.end_date == undefined) {
                item.end_date = date
            }   
            item.units.forEach(item => {
                if (item.is_operate == undefined) {
                    item.is_operate = 1
                }
                item.measures.forEach(item => {
                    if (item.value == undefined) {
                        item.value = 0
                    }
                })
            })
        })
    })
    return post('batch/upload',batch,true,false,true)
    .then(response => {
        if (response) {
            batch.isSent = true
        }
        let batchHistory = getState().data.batchHistory
        let length = batchHistory.length
        if (length > 0) {
            length = batchHistory.unshift(batch)   
        } else {
            length = batchHistory.push(batch)
        }
        if (length > 10) {
            batchHistory.pop()
        }
        dispatch({
            type: SAVE_BATCH,
            payload: batchHistory
        })
        dispatch(stopAlarm())
        dispatch(logout())
    })
}

export const setBatchLog = batch => dispatch => {
    dispatch({
        type: SET_BATCH_LOG,
        payload: batch
    })
    if (!batch) return
    return navigate('Cycle')
}

export const setIdStation = (idStation,location = null) => (dispatch,getState) => {
    if (!idStation) {
        return dispatch({
            type: SET_ID_STATION,
            payload: null
        })
    }

    let isLog = getState().data.batchLog ? true : false
    if (!isLog) {
        let batch = getState().data.batch
        let cycleNumber = getState().data.cycleNumber
        let station = getStation(batch,cycleNumber,idStation)
        if (station.start_date == undefined) {
            let validSet = getStations(batch,cycleNumber)
            .filter(item => item.id_station != idStation)
            .every(item => (item.start_date && item.end_date) || (!item.start_date && !item.end_date))
            
            if (!validSet) {
                return showSimpleAlert('Peringatan','On Going Station harus dibereskan terlebih dahulu sebelum lanjut input ke station yang lain')
            }
            station.start_date = dateStringToApiFormat()
            if (location) {
                station.latitude = location.latitude
                station.longitude = location.longitude
            }
        }
        dispatch(setBatch(batch))
        dispatch(refreshStation())
    }

    dispatch({
        type: SET_ID_STATION,
        payload: idStation
    })
    return navigate('InputForm')
}

export const setBatch = batch => dispatch => {
    return dispatch({
        type : SET_BATCH,
        payload : batch
    })
}

export const setUnitOperate = (idUnit,isOperate) => (dispatch,getState) => {
    let data = getState().data
    let batch = data.batch
    let cycleNumber = data.cycleNumber
    let idStation = data.idStation
    let unit = getUnit(batch,cycleNumber,idStation,idUnit)
    unit.is_operate = isOperate ? 1 : 0
    return dispatch(setBatch(batch))
}

export const setUnitReason = (idUnit, reason) => (dispatch,getState) => {
    let data = getState().data
    let batch = data.batch
    let cycleNumber = data.cycleNumber
    let idStation = data.idStation
    let unit = getUnit(batch,cycleNumber,idStation,idUnit)
    unit.reason = reason
    return dispatch(setBatch(batch))
}

export const setUnitNotes = (idUnit, notes) => (dispatch,getState) => {
    let data = getState().data
    let batch = data.batch
    let cycleNumber = data.cycleNumber
    let idStation = data.idStation
    let unit = getUnit(batch,cycleNumber,idStation,idUnit)
    unit.notes = notes
    return dispatch(setBatch(batch))
}

export const setMeasureValue = (idUnit,idMeasure,value) => (dispatch,getState) => {
    let data = getState().data
    let batch = data.batch
    let cycleNumber = data.cycleNumber
    let idStation = data.idStation
    let measure = getMeasure(batch,cycleNumber,idStation,idUnit,idMeasure)
    measure.value = value
    return dispatch(setBatch(batch))
}

export const setStationEndDate = () => (dispatch,getState) => {
    let data = getState().data
    let batch = data.batch
    let cycleNumber = data.cycleNumber
    let idStation = data.idStation
    let station = getStation(batch, cycleNumber, idStation)
    station.end_date = dateStringToApiFormat()
    dispatch(setBatch(batch)) 
    dispatch(refreshStation())
    return goBack()
}

export const setCycleEndDate = () => (dispatch,getState) => {
    let data = getState().data
    let batch = data.batch
    let cycleNumber = data.cycleNumber
    let cycle = getCycle(batch,cycleNumber)
    cycle.end_date = dateStringToApiFormat()
    dispatch(stopAlarm())
    dispatch(setBatch(batch)) 
    dispatch(refreshCycle())
    return goBack()
}

export const getCycle = (batch,cycleNumber) => {
    return batch.cycle
    .find(item => item.number == cycleNumber)
}

export const getStations = (batch,cycleNumber) => {
    return batch.cycle
    .find(item => item.number == cycleNumber).stations
}

export const getStation = (batch,cycleNumber,idStation) => {
    return batch.cycle
    .find(item => item.number == cycleNumber).stations
    .find(item => item.id_station == idStation)
}

export const getUnits = (batch,cycleNumber,idStation) => {
    return batch.cycle
    .find(item => item.number == cycleNumber).stations
    .find(item => item.id_station == idStation).units
}

export const getUnit = (batch,cycleNumber,idStation,idUnit) => {
    return batch.cycle
    .find(item => item.number == cycleNumber).stations
    .find(item => item.id_station == idStation).units
    .find(item => item.id_unit == idUnit)
}

export const getMeasure = (batch,cycleNumber,idStation,idUnit,idMeasure) => {
    return batch.cycle
    .find(item => item.number == cycleNumber).stations
    .find(item => item.id_station == idStation).units
    .find(item => item.id_unit == idUnit).measures
    .find(item => item.id_measure == idMeasure)
}

const refreshStation = () => dispatch => {
    return dispatch({
        type:REFRESH_STATION
    })
}

const refreshCycle = () => dispatch => {
    return dispatch({
        type:REFRESH_CYCLE
    })
}