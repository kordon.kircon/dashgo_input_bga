import {combineReducers,createStore,applyMiddleware,compose} from 'redux'
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'
import authReducer from './Reducers/AuthReducer'
import processReducer from './Reducers/ProcessReducer'
import cycleReducer from './Reducers/CycleReducer'
import stationReducer from './Reducers/StationReducer'
import inputFormReducer from './Reducers/InputFormReducer'
import dataReducer from './Reducers/DataReducer'
import localReducer from './Reducers/LocalReducer'
import actReducer from './Reducers/ActReducer'
import FilesystemStorage from 'redux-persist-filesystem-storage'
import RNFetchBlob from 'rn-fetch-blob'

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

FilesystemStorage.config({
    storagePath: RNFetchBlob.fs.dirs.DocumentDir+'/persistStore',
    encoding: "utf8",
    toFileName: (name) => name.split(":").join("-"),
    fromFileName: (name) => name.split("-").join(":"),
});

const persistConfig = {
    // Root
    key: 'rootdashgoinput',
    // Storage Method (React Native)
    storage: FilesystemStorage,
    toFileName: (name) => name.split(":").join("-"),
    fromFileName: (name) => name.split("-").join(":"),
    // // Whitelist (Save Specific Reducers)
    timeout: null,
    whitelist: [
        'auth',
        'process',
        'cycle',
        'station',
        'inputform',
        'data',
        'local',
        'act'
    ],
    // Blacklist (Don't Save Specific Reducers)
    blacklist: [
        
    ],
}

const rootReducer = combineReducers({
    auth:authReducer,
    process:processReducer,
    cycle:cycleReducer,
    station:stationReducer,
    inputform:inputFormReducer,
    data:dataReducer,
    local:localReducer,
    act: actReducer,
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

let store = createStore(
    persistedReducer,
    storeEnhancers(applyMiddleware(thunk))
    
)

let persistor = persistStore(store)

export {store,persistor}