// auth
export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'

// process
export const START_PROCESS = 'START_PROCESS'
export const STOP_PROCESS = 'STOP_PROCESS'
export const SHIFT = 'SHIFT'
export const CURRENT_STORY = 'CURRENT_STORY'

// cycle
export const INIT_CYCLE = 'INIT_CYCLE'
export const START_CYCLE = 'START_CYCLE'
export const CURRENT_CYCLE = 'CURRENT_CYCLE'
export const STOP_CYCLE = 'STOP_CYCLE'
export const ADD_CYCLE = 'ADD_CYCLE'
export const SET_CYCLE_KEY = 'SET_CYCLE_KEY'

// station
export const INIT_STATION = 'INIT_STATION'
export const CURRENT_STATION = 'CURRENT_STATION'
export const START_STATION = 'START_STATION'
export const STOP_STATION = 'STOP_STATION'
export const SET_STATION_KEY = 'SET_STATION_KEY'

// input form
export const INIT_INPUT_FORM = 'INIT_INPUT_FORM'
export const SET_INPUT_FORM = 'SET_INPUT_FORM'

// dev
export const CLEAR = 'CLEAR'

// data
export const SET_VERSION = 'SET_VERSION'
export const SET_TEMPLATE = 'SET_TEMPLATE'
export const SET_BATCH = 'START_BATCH'
export const SET_CYCLE_NUMBER = 'SET_CYCLE_NUMBER'
export const SAVE_BATCH = 'SAVE_BATCH'
export const SET_BATCH_LOG = 'SET_BATCH_LOG'
export const SET_ID_STATION = 'SET_ID_STATION'
export const REFRESH_STATION = 'REFRESH_STATION'
export const REFRESH_CYCLE = 'REFRESH_CYCLE'
export const REFRESH_PROCESS = 'REFRESH_PROCESS'
export const SET_ALARM = 'SET_ALARM'
export const IDENTITY = 'IDENTITY'
export const MENU = 'MENU'

// local
export const INIT_MENU = 'INIT_MENU'
export const SAVE_FORM_ITEM = 'SAVE_FORM_ITEM'
export const CREATE_FORM = 'CREATE_FORM'
export const CANCEL_FORM = 'CANCEL_FORM'
export const SAVE_QUESTION_ITEM = 'SAVE_QUESTION_ITEM'
export const SET_NEED_ACT = 'SET_NEED_ACT'
export const SET_IMAGE = 'SET_IMAGE'
export const CREATE_TEMPORARY = 'CREATE_TEMPORARY'
export const FINISH_FORM = 'FINISH_FORM'
export const UPLOAD_FORM = 'UPLOAD_FORM'
export const DELETE_FORM = 'DELETE_FORM'
export const SET_FORM_ID = 'SET_FORM_ID'
export const SET_SUCCESS_UPLOAD_IMAGE = 'SET_SUCCESS_UPLOAD_IMAGE'
export const SET_CURRENT_HISTORY = 'SET_CURRENT_HISTORY'
export const SAVE_FORM = 'SAVE_FORM'
export const SET_CURRENT_SUB_PARENT = 'SET_CURRENT_SUB_PARENT'
export const SET_CURRENT_SUB = 'SET_CURRENT_SUB'
export const SET_MEMBER = 'SET_MEMBER'
export const DELETE_MEMBER = 'DELETE_MEMBER'
export const SET_MEMBER_IMAGE = 'SET_MEMBER_IMAGE'
export const UPLOAD_MEMBER = 'UPLOAD_MEMBER'
export const SET_IMAGE_DATA = 'SET_IMAGE_DATA'

// act
export const INIT_ACT = 'INIT_ACT'
export const SET_CURRENT_ACT = 'SET_CURRENT_ACT'
export const START_ACT = 'START_ACT'
export const CANCEL_ACT = 'CANCEL_ACT'
export const SET_ACT_NEED_ACT = 'SET_ACT_NEED_ACT'
export const SET_IMAGE_ACT = 'SET_IMAGE_ACT'
export const SET_IMAGE_DATA_ACT = 'SET_IMAGE_DATA_ACT'
export const SAVE_QUESTION_ACT = 'SAVE_QUESTION_ACT'
export const FINISH_ACT = 'FINISH_ACT'
export const UPLOAD_ACT = 'UPLOAD_ACT'
export const SET_ACT_ID = 'SET_ACT_ID'
export const SET_SUCCESS_UPLOAD_IMAGE_ACT = 'SET_SUCCESS_UPLOAD_IMAGE_ACT'
export const DELETE_ACT = 'DELETE_ACT'
export const SAVE_FORM_ITEM_ACT = 'SAVE_FORM_ACT'