import {START_PROCESS, STOP_PROCESS, CURRENT_STORY, LOGOUT, SHIFT, CLEAR} from "../Constant"

const initialState = {
    startDate:null,
    isMorning:false,
    history:[],
    currentStory:null
}

const processReducer = (state = initialState, action) => {
    var result
    switch(action.type){
        case SHIFT:
            return {...state,isMorning:action.payload}
        case CURRENT_STORY:
            return {...state,currentStory:action.payload}
        case START_PROCESS:
            result = {...state}
            action.payload.isMorning = result.isMorning
            result.startDate = action.payload
            result.currentStory = action.payload
            return result
        case STOP_PROCESS:
            result = {...state}
            if(!result.history){
                result.history = []
            }
            result.startDate.isFinish = true
            result.history.push({
                startDate:result.startDate,
                endDate:action.payload
            })
            result.startDate = null
            result.currentStory = null
            return result
        case CLEAR:
            return initialState
        default:
            return state
    }
}

export default processReducer