import {CLEAR, CURRENT_STATION, INIT_STATION, LOGOUT, REFRESH_STATION, SET_STATION_KEY, START_STATION, STOP_STATION} from "../Constant"

const initialState = {
    currentId:null,
    key:null,
    isRefresh:false,
}

const stationReducer = (state = initialState,action) => {
    var result
    switch(action.type){
        case SET_STATION_KEY:
            return {...state,key:action.payload}
        // case REFRESH_STATION:
        //     return {...state,isRefresh:!state.isRefresh}
        case STOP_STATION:
            result = {...state}
            result[result.key].find(item => item.id == result.currentId).endDate = action.payload
            return result
        case CURRENT_STATION:
            return {...state,currentId:action.payload}
        case START_STATION:
            result = {...state}
            result[result.key].find(item => item.id == result.currentId).startDate = action.payload
            return result
        case INIT_STATION:
            result = {...state,key:action.payload}
            result[action.payload] = [
                {
                    id:1,
                    name:'STERILIZER'
                },
                {
                    id:2,
                    name:'PRESS'
                },
                {
                    id:3,
                    name:'KLARIFIKASI'
                },
                {
                    id:4,
                    name:'NUT & KERNEL'
                },
                {
                    id:5,
                    name:'BOILEER'
                },
                {
                    id:6,
                    name:'KAMAR MESIN'
                }
            ]
            return result
        case CLEAR:
            return initialState
        default:
            return state
    }
}

export default stationReducer