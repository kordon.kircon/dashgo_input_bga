import { version } from "react"
import { SET_TEMPLATE, SET_VERSION, SET_BATCH, SET_CYCLE_NUMBER, SAVE_BATCH, LOGOUT, SET_BATCH_LOG, SET_ID_STATION, REFRESH_STATION, REFRESH_CYCLE, REFRESH_PROCESS, SET_ALARM, IDENTITY, MENU } from "../Constant"

const initialState = {
    version:null,
    template:null,
    batch:null,
    cycleNumber:null,
    batchHistory:[],
    batchLog:null,
    idStation:null,
    refreshStation:false,
    refreshCycle:false,
    refreshProcess:false,
    alarm:null,
    identity: null,
    menu: null,
    maskMimeLog:null,
}

const dataReducer = (state = initialState,action) => {
    switch(action.type){
        case SET_ALARM:
            return {...state, alarm: action.payload}
        case REFRESH_PROCESS:
            return {...state,refreshProcess:!state.refreshProcess}
        case REFRESH_CYCLE:
            return {...state,refreshCycle:!state.refreshCycle}
        case REFRESH_STATION:
            return {...state,refreshStation:!state.refreshStation}
        case SET_ID_STATION:
            return {...state,idStation:action.payload}
        case SET_BATCH_LOG:
            return {...state,batchLog:action.payload}
        case LOGOUT:
            return {...state,batch:null, alarm:null}
        case SAVE_BATCH:
            return {...state,batchHistory:action.payload,refreshProcess:!state.refreshProcess}
        case SET_BATCH:
            return {...state,batch:action.payload}
        case SET_VERSION:
            return {...state,version:action.payload}
        case SET_TEMPLATE:
            return {...state,template:action.payload}
        case SET_CYCLE_NUMBER:
            return {...state,cycleNumber:action.payload}
        case IDENTITY:
            return {...state, identity: action.payload, maskMimeLog: action.payload.is_update == 1}
        case MENU:
            return {...state, menu: action.payload}
        default:
            return state
    }
}

export default dataReducer