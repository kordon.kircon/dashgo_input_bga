import {CURRENT_CYCLE, INIT_CYCLE, LOGOUT, START_CYCLE, STOP_CYCLE, REFRESH_CYCLE, ADD_CYCLE, CLEAR, SET_CYCLE_KEY} from "../Constant"

const initialState = {
    currentId:1,
    key:null,
    isRefresh:false,
}

const cycleReducer = (state = initialState,action) => {
    var result
    switch(action.type){
        case SET_CYCLE_KEY:
            return {...state,key:action.payload}
        case ADD_CYCLE:
            // let currentId = state.currentId+1
            result = {...state}
            let datas = result[result.key]
            let id  = datas[datas.length-1].id+1
            datas.push({
                id:id,
                name:'Cycle '+id
            })
            result.currentId = id
            return result
        // case REFRESH_CYCLE:
        //     return {...state,isRefresh:!state.isRefresh}
        case STOP_CYCLE:
            result = {...state}
            result[result.key].find(item => item.id == result.currentId).endDate = action.payload
            return result
        case CURRENT_CYCLE:
            return {...state,currentId:action.payload}
        case START_CYCLE:
            result = {...state}
            result[result.key].find(item => item.id == result.currentId).startDate = action.payload
            return result
        case INIT_CYCLE:
            result = {...state,key:action.payload}
            result[action.payload] = [
                {
                    id:1,
                    name:'Cycle 1'
                }
            ]
            return result
        case CLEAR:
            return initialState
        default:
            return state
    }
}

export default cycleReducer