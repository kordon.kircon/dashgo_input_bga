import { copy, dateStringToApiFormat } from "../../Utilities/Conversion"
import { CANCEL_ACT, DELETE_ACT, FINISH_ACT, INIT_ACT, LOGOUT, SAVE_QUESTION_ACT, SET_ACT_ID, SET_ACT_NEED_ACT, SET_CURRENT_ACT, SET_IMAGE_ACT, SET_SUCCESS_UPLOAD_IMAGE_ACT, START_ACT, SET_IMAGE_DATA_ACT, SAVE_FORM_ITEM_ACT} from "../Constant"

const initialState = {
    data: [],
    current: null
}

const actReducer = (state = initialState, action) => {
    var result, current, question, display
    switch(action.type) {
        case SAVE_FORM_ITEM_ACT:
            result = {...state}
            current = result.current
            current.display = current.display?.map(item => {
                if(item.id_lvl == action.item.id_lvl){
                    item.answer = copy(action.payload)
                    if(item.type == 'teampicker'){
                        item.answer.data = copy(current.template.teampicker.list_employee.find(it => it.id_team == action.payload.id)?.employees)
                    }
                }
                return item
            })
            var index = 0
            var dependency_lvl = action.item.id_lvl
            display = current.display
            while(dependency_lvl && index < display.length){
                if(display[index].dependency_lvl == dependency_lvl) {
                    display[index].answer = null
                    dependency_lvl = display[index].id_lvl
                }
                index ++
            }
            return copy(result)
        case DELETE_ACT:
            result = {...state}
            let current = result.data[action.payload]
            current.isImageUploaded = false
            current.isDataUploaded = false
            current.finish = null
            current.start = null
            current.image_list = current.image_list.map(it => {
                it.isUploaded = false
                return it
            })
            return copy(result)
        case SET_SUCCESS_UPLOAD_IMAGE_ACT:
            result = {...state}
            current = result.data[action.index]
            current.image_list.find(it => it.index == action.payload).isUploaded = true
            current.isImageUploaded = !current.image_list.find(it => !it.isUploaded && it.answer)
            return copy(result)
        case SET_ACT_ID:
            result = {...state}
            result.data[action.index].id = action.payload
            result.data[action.index].isDataUploaded = true
            return copy(result)
        case FINISH_ACT:
            result = {...state}
            current = result.current
            current.id_user = action.userId
            current.id_role = action.idRole
            current.finish = dateStringToApiFormat()
            current.gps_finish = action.payload
            current.isDataUploaded = false
            current.isImageUploaded = !current.image_list.find(it => it.answer != null)
            current.image_list = current.image_list.filter(it => it.answer || it.image_url)
            result.data = result.data.map(it => {
                if(it.ticket_no == current.ticket_no && it.act_no == current.act_no){
                    return copy(current)
                }
                return it
            })
            current = null
            return copy(result)
        case SAVE_QUESTION_ACT:
            result = {...state}
            question = result.current.question
            question = question.map(it => {
                if(it.id == action.id){
                    let answerItem = action.payload
                    if(it.type == 'multiple'){
                        it.answer = it.answer.filter(fl => fl.opsi != answerItem.opsi)
                        if(action.value){
                            it.answer.push(answerItem)
                        }
                    }else{
                        it.answer = [answerItem]
                    }
                }
                return it
            })
            return copy(result)
        case SET_IMAGE_DATA_ACT:
            result = {...state}
            current = result.current
            current.image_list = current.image_list.map(it => {
                if(it.index == action.index){
                    it.data = action.payload
                }
                return it
            })
            return copy(result)
        case SET_IMAGE_ACT:
            result = {...state}
            current = result.current
            current.image_list = current.image_list.map(it => {
                if(it.index == action.index){
                    it.answer = action.payload
                }
                return it
            })
            return result
        case SET_ACT_NEED_ACT:
            result = {...state}
            current = result.current
            current.need_act = action.payload
            return result
        case CANCEL_ACT:
            result = {...state}
            current = result.current
            current.start = null
            current.gps_start = null
            current.need_act = null
            current.image_list = current.image_list.map(it => {
                if(!it.image_url){
                    it.data = null
                }
                it.answer = null
                return it
            })
            return result
        case START_ACT:
            result = {...state}
            current = result.current
            current.start = dateStringToApiFormat()
            current.gps_start = action.payload
            return result
        case INIT_ACT:
            result = {...state}
            let data = result.data
            let filteredData = action.payload.map(it => {
                let dataItem = data.find(ite => {
                    return ite.finish && ite.act_no == it.act_no && ite.ticket_no == it.ticket_no && (!ite.isDataUploaded || !ite.isImageUploaded) && ite.title == it.title
                })
                if(dataItem){
                    return dataItem
                }
                return it
            })
            return {...state, data: filteredData}
        case SET_CURRENT_ACT:
            return {...state, current: action.payload}
        // case LOGOUT:
        //     return initialState
        default:
            return state
    }
}

export default actReducer