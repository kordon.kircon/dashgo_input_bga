import {INIT_INPUT_FORM, LOGOUT, SET_INPUT_FORM,CLEAR} from "../Constant"

const initialState = {
    key:null,
}

const inputFormReducer = (state = initialState,action) => {
    var result
    switch(action.type){
        case SET_INPUT_FORM:
            result = {...state}
            let item = result[result.key].find(_item => _item.id == action.id)
            item.value = action.payload
            return result
        case INIT_INPUT_FORM:
            result = {...state,key:action.payload}
            result[action.payload] = [
                {
                    id:1,
                    name:'input 1'
                },
                {
                    id:2,
                    name:'input 2'
                },
                {
                    id:3,
                    name:'input 3'
                },
                {
                    id:4,
                    name:'input 4'
                },
                {
                    id:5,
                    name:'input 5'
                },
                {
                    id:6,
                    name:'input 6'
                }
            ]
            return result
        case CLEAR:
            return initialState
        default:
            return state
    }
}

export default inputFormReducer