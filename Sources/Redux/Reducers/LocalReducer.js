import {
    INIT_MENU, 
    SAVE_FORM_ITEM, 
    CREATE_FORM, 
    CANCEL_FORM, 
    SAVE_QUESTION_ITEM, 
    SET_NEED_ACT, 
    CREATE_TEMPORARY, 
    SET_IMAGE, 
    FINISH_FORM, 
    LOGOUT, 
    DELETE_FORM,
    SET_FORM_ID,
    SET_SUCCESS_UPLOAD_IMAGE,
    SET_CURRENT_HISTORY,
    SAVE_FORM,
    SET_CURRENT_SUB_PARENT,
    SET_CURRENT_SUB,
    SET_MEMBER,
    DELETE_MEMBER,
    SET_MEMBER_IMAGE,
    UPLOAD_MEMBER,
    SET_IMAGE_DATA
} from '../Constant'
import { copy, dateStringToApiFormat } from '../../Utilities/Conversion'

const initialState = {
    key: null,
    history: {},
    currentHistory: null,
    currentSubParent: null,
    currentSub: null,
}

const localReducer = (state = initialState, action) => {
    var result, display, key, image, imageDatas, historyItem, subsItem, current, questionItem, temp, isImageUploaded, teampicker, teamPickerItem
    switch(action.type) {
        case SET_CURRENT_SUB:
            return {...state, currentSub: action.payload}
        case SET_CURRENT_SUB_PARENT:
            return {...state, currentSubParent: action.payload}
        case INIT_MENU:
            if(state[action.key] && state[action.key].current){
                return {...state}
            }
            result = {...state}
            result.key = action.key
            result[action.key] = {
                data_template: copy(action.payload),
                display: copy(action.payload.template.display),
                nav_bar_title: copy(action.payload.nav_bar_title),
                template: copy(action.payload.template),
                attention: copy(action.payload.template.attention),
                image: copy(action.payload.template.image),
                multisave: copy(action.payload.multisave)
            }
            return result
        case SAVE_QUESTION_ITEM:
            result = {...state}
            key = result.key
            result[key].question = result[key].question.map(item => {
                if(item.id == action.item.id){
                    if(!item.answer){
                        item.answer = []
                    }
                    let answerItem = action.payload
                    if(item.type == 'multiple'){
                        item.answer = item.answer.filter(fl => fl.opsi != answerItem.opsi)
                        if(action.value){
                            item.answer.push(answerItem)
                        }
                    }else{
                        item.answer = [answerItem]
                    }
                }
                return item
            })
            return result
        case SET_MEMBER_IMAGE:
            result = {...state}
            key = result.key
            result[key].display.find(it => it.type == 'teampicker').answer.data.map(it => {
                if(it.id == action.item.id){
                    it.image = action.payload
                }
                return it
            })
            return copy(result)
        case DELETE_MEMBER:
            result = {...state}
            key = result.key
            result[key].display.find(it => it.type == 'teampicker').answer.data.map(it => {
                if(it.id == action.payload.id){
                    it.isSelected = false
                    it.image = null
                }
                return it
            })
            return copy(result)
        case SET_MEMBER:
            result = {...state}
            key = result.key
            result[key].display.find(it => it.type == 'teampicker').answer.data.map(it => {
                if(it.id == action.payload.id){
                    it.isSelected = true
                }
                return it
            })
            return copy(result)
        case SAVE_FORM_ITEM:
            result = {...state}
            result[action.key].display = result[action.key].display.map(item => {
                if(item.id_lvl == action.item.id_lvl){
                    item.answer = copy(action.payload)
                    item.original_answer = copy(action.payload)
                    if(item.type == 'teampicker'){
                        item.answer.data = copy(result[action.key].template.teampicker.list_employee.find(it => it.id_team == action.payload.id)?.employees)
                    }
                }
                return item
            })
            var index = 0
            var dependency_lvl = action.item.id_lvl
            display = result[action.key].display
            while(dependency_lvl && index < display.length){
                if(display[index].dependency_lvl == dependency_lvl) {
                    display[index].answer = null
                    display[index].original_answer = null
                    dependency_lvl = display[index].id_lvl
                }
                index ++
            }
            return copy(result)
        case CREATE_FORM:
            result = {...state}
            let idRole = action.idRole
            let userId = action.userId
            key = result.key
            display = result[key].display
            let itemWithQuestion = display.find(it => it.answer.question != undefined).answer
            var lastLevelingItem
            for(var i = display.length - 1; i>=0; i--){
                if(display[i].lookup_key == 'leveling'){
                    lastLevelingItem = display[i].answer
                    break
                }
            }
            var questionResult
            var levelingResult
            for(var i=display.length - 1; i>=1; i--){
                if(display[i].lookup_key){
                    if(display[i].lookup_key == 'question'){
                        if(display[i-1].lookup_key == 'question'){
                            display[i-1].answer.children = display[i].answer
                            questionResult = display[i-1].answer
                        }
                    }else if(display[i].lookup_key == 'leveling'){
                        if(display[i-1].lookup_key == 'leveling'){
                            display[i-1].answer.children = display[i].answer
                            levelingResult = display[i-1].answer
                        }
                    }
                }
            }
            result[result.key].current = {
                id_user: userId,
                id_role: idRole,
                start: dateStringToApiFormat(),
                gps_start: copy(display.find(it => it.type == 'gpsautofill').answer),
                last_id_question: itemWithQuestion.id,
                last_lvl_question: itemWithQuestion.lvl,
                last_id_leveling: lastLevelingItem?.id,
                last_lvl_leveling: lastLevelingItem?.lvl,
                question: copy(questionResult),
                leveling: copy(levelingResult),
                image: copy(result[result.key].image)
            }

            result[result.key].question = itemWithQuestion.question
            image = result[result.key].image
            imageDatas = []
            for(var i=0; i<image.max_image; i++){
                imageDatas.push({
                    index: i,
                    answer: null
                })
            }
            result[result.key].imageDatas = imageDatas
            return result
        case CANCEL_FORM:
            result = {...state}
            key = result.key
            result[key].current = null
            result[key].display = result[key].display.map(it => {
                it.answer = null
                return it
            })
            return result
        case SET_NEED_ACT:
            result = {...state}
            key = result.key
            result[key].current.need_act = action.payload
            return result
        case CREATE_TEMPORARY:
            result = {...state}
            key = result.key
            image = result[key].image
            imageDatas = []
            for(var i=0; i<image.max_image; i++){
                imageDatas.push({
                    index: i,
                    answer: null
                })
            }
            result[key].imageDatas = imageDatas
            return result
        case SET_IMAGE:
            result = {...state}
            key = result.key
            result[key].imageDatas = result[key].imageDatas.map(it => {
                if(it.index == action.item.index){
                    it.answer = action.payload
                }
                return it
            })
            return result
        case SET_IMAGE_DATA:
            result = {...state}
            key = result.key
            result[key].imageDatas = result[key].imageDatas.map(it => {
                if(it.index == action.item.index){
                    it.data = action.payload
                }
                return it
            })
            return copy(result)
        case SAVE_FORM:
            result = {...state}
            result = {...state}
            key = result.key
            current = result[key].current
            current.finish = dateStringToApiFormat()
            current.gps_finish = action.payload
            questionItem = current.question
            var subtitle = questionItem.nama
            while(!questionItem.question){
                subtitle += ' - '
                questionItem = questionItem.children
                subtitle +=  questionItem.nama
            }
            questionItem.question = result[key].question
            if(!current.subs){
                current.subs = []
            }
            
            subsItem = {
                key: key,
                title: result[key].nav_bar_title.toUpperCase(),
                subtitle: subtitle,
                duration: result[key].current.start + ' ~ ' + result[key].current.finish,
                isDataUploaded: false,
                isImageUploaded: !result[key].imageDatas.find(it => it.answer != null),
                api: result[key].data_template.data_subpost,
                data: copy(result[key].current),
                image_list: copy(result[key].imageDatas.filter(it => it.answer != null)),
                display: copy(result[key].display),
                nav_bar_title: result[key].nav_bar_title,
                attention: copy(result[key].attention),
                question: copy(result[key].question),
                image: copy(result[key].image)
            }
            current.subs.push(subsItem)
            current.need_act = null
            result[key].question = result[key].question.map(it => {
                it.answer = null
                return it
            })
            result[key].imageDatas = result[key].imageDatas.map(it => {
                it.answer = null
                it.data = null
                return it
            })
            // history[action.idRole].push(historyItem)
            return copy(result)
        case FINISH_FORM:
            result = {...state}
            key = result.key
            current = result[key].current
            current.finish = dateStringToApiFormat()
            current.gps_finish = action.payload
            questionItem = current.question
            var subtitle = questionItem.nama
            while(!questionItem.question){
                subtitle += ' - '
                questionItem = questionItem.children
                subtitle +=  questionItem.nama
            }
            questionItem.question = result[key].question
            if(!result.history){
                result.history = {}
            }
            let history = result.history
            if(!history[action.idRole]){
                history[action.idRole] = []
            }
            teamPickerItem = result[key].display.find(it => it.type == 'teampicker')
            var isMemberUploaded = true
            if(teamPickerItem){
                isMemberUploaded = false
                teamPickerItem.image_post = result[key].template.teampicker.image_post
            }
            historyItem = {
                key: key,
                title: result[key].nav_bar_title.toUpperCase(),
                subtitle: subtitle,
                duration: result[key].current.start + ' ~ ' + result[key].current.finish,
                isDataUploaded: false,
                isImageUploaded: !result[key].imageDatas.find(it => it.answer != null),
                isMemberUploaded: isMemberUploaded,
                api: result[key].data_template.data_post,
                data: copy(result[key].current),
                image_list: result[key].imageDatas.filter(it => it.answer != null),
                display: copy(result[key].display),
                nav_bar_title: result[key].nav_bar_title,
                attention: copy(result[key].attention),
                question: copy(result[key].question),
                image: copy(result[key].image)
            }
            // console.log('LocalReducer', 'FINISH_FORM', historyItem)
            history[action.idRole].push(historyItem)
            
            result[key].current.start = null
            result[key].current.finish = null
            result[key].current.gps_finish = null
            result[key].current.gps_start = null
            result[key].current.need_act = null
            result[key].question = copy(result[key]).question.map(item => {
                item.answer = null
                return item
            })
            result[key].imageDatas = copy(result[key]).imageDatas.map(item => {
                item.answer = null
                return item
            })
            return copy(result)
        case DELETE_FORM:
            result = {...state}
            result.history[action.idRole].splice(action.payload, 1)
            result.currentSub = null
            result.currentSubParent = null
            return copy(result)
        case SET_FORM_ID:
            result = {...state}
            historyItem = result.history[action.idRole][action.index]
            historyItem.id = action.payload.id
            historyItem.isDataUploaded = true
            if(historyItem.data.subs){
                historyItem.data.id_patroli = action.payload
                historyItem.data.subs = historyItem.data.subs.map((it, index) => {
                    it.id = action.payload.id_sub[index].id
                    it.isImageUploaded = it.image_list.findIndex(im => im.answer && !im.isUploaded) == -1
                    return it
                })
                historyItem.isImageUploaded = historyItem.data.subs.findIndex(it => !it.isImageUploaded) == -1
            }
            return copy(result)
        case SET_SUCCESS_UPLOAD_IMAGE:
            result = {...state}
            historyItem = result.history[action.idRole][action.index]
            if(historyItem.data.subs){
                subsItem = historyItem.data.subs.find(it => it.id == action.id)
                subsItem.image_list.find(it => it.index == action.payload).isUploaded = true
                subsItem.isImageUploaded = !subsItem.image_list.find(it => !it.isUploaded)   
                historyItem.isImageUploaded = historyItem.data.subs.findIndex(it => !it.isImageUploaded) == -1
            }else{
                historyItem.image_list.find(it => it.index == action.payload).isUploaded = true
                historyItem.isImageUploaded = !historyItem.image_list.find(it => !it.isUploaded)   
            }
            result.history[action.idRole] = result.history[action.idRole].map(it => {
                it.isFullyUploaded = it.isImageUploaded && it.isDataUploaded
                return it
            })
            return result
        case UPLOAD_MEMBER:
            result = {...state}
            historyItem = result.history[action.idRole][action.index]
            teamPickerItem = historyItem.display.find(it => it.type == 'teampicker').answer.data
            teamPickerItem.find(it => it.id == action.payload).isUploaded = true
            if(teamPickerItem.findIndex(it => it.image && !it.isUploaded) == -1){
                historyItem.isMemberUploaded = true
            }
            return copy(result)
        case SET_CURRENT_HISTORY:
            return {...state, currentHistory: action.payload}
        case LOGOUT:
            result = {...state}
            for(var key in result.history){
                result.history[key] = result.history[key].filter(it => !it.isDataUploaded || !it.isImageUploaded)
            }
            return result
        default:
            return state
    }
}

export default localReducer