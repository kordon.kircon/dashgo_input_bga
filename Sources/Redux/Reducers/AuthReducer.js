import { LOGIN, LOGOUT } from "../Constant"

const initialState = {
    isLogin:false,
    token:null,
    userId:null,
    loginData:null,
    idRole:null,
}

const authReducer = (state = initialState,action) => {
    switch(action.type){
        case LOGOUT:
            return initialState
        case LOGIN:
            return {...state,isLogin:true,token:action.payload.token,userId:action.payload.user_id,loginData:action.payload,idRole: action.payload.id_role}
        default:
            return state
    }
}

export default authReducer