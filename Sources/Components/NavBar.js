import React,{Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    FlatList,
    TextInput,
    Platform,
    Dimensions,
    Alert,
    SafeAreaView,
    Animated,
    BackHandler
} from 'react-native'
import Color from '../Utilities/Color'
import Image from '../Components/Image'
import ic_back from '../Images/keyboard_backspace_24_px.png'
import {goBack} from '../Utilities/RootNavigation'
import {connect} from 'react-redux'
import ic_logout from '../Images/power_settings_new_24_px.png'
import {reset} from '../Utilities/RootNavigation'
import {logout} from '../Redux/Actions/AuthAction'
import { showAlert } from '../Screens/Alert'
import styles from '../Utilities/Style'
import { useFocusEffect } from '@react-navigation/native'

const mapStateToProps = (state, ownProps) => {
    return {
        bgColor: state.data.identity?.navbar_bg
    }
}

export const NavBarTypes = {
    TITLE:0,
    LOGOUT:1,
    BACK:2,
    BANNER:3,
    SPACE:6,
    RIGHT_BUTTON:7
}

class NavBar extends React.Component {
    constructor(){
        super()
        this.goBackAction = this.goBackAction.bind(this)
    }

    goBackAction() {
        let{items} = this.props
        if(items){
            let menuitem = items.find(it => it.type == NavBarTypes.BACK)
            if(menuitem && menuitem.onBack){
                menuitem.onBack()
            }else{
                goBack()
            }
        }
    }

    componentDidMount() {
        let{items} = this.props
        if(items){
            let menuitem = items.find(it => it.type == NavBarTypes.BACK)
            let titleItem = items.find(it => it.type == NavBarTypes.TITLE)
            if(menuitem){
                // console.log('REGISTER BACK HANDLER '+titleItem.text)
                this.backHandler = BackHandler.addEventListener("hardwareBackPress", ()=>{
                    this.goBackAction()
                    return true
                })
            }
        }
    }

    componentWillUnmount() {
        let{items} = this.props
        var titleItem
        if(items){
            titleItem = items.find(it => it.type == NavBarTypes.TITLE)
        }
        if(this.backHandler){
            // console.log('REMOVE BACK HANDLER '+titleItem?.text)
            this.backHandler.remove()
        }
    }

    render(){
        let{items,style,logout, bgColor} = this.props
        if(!items)return null

        return(
            <SafeAreaView  style={style}>
                <View style={{position:'absolute',left:0,top:0,right:0,backgroundColor:bgColor?bgColor:Color.navBarBg,height:50}}/>
                <View style={{width:'100%',flexDirection:'row',height:50}}>
                    {items != null ? items.map((menuitem,index) => {
                        if(!menuitem)return null
                        switch(menuitem.type){
                            case NavBarTypes.RIGHT_BUTTON:
                                return(
                                    <TouchableOpacity 
                                        key={menuitem.type} style={{height:50,alignItems:'center',justifyContent:'center'}}
                                        onPress={menuitem.onPress}>
                                        <Image
                                            style = {{height:24,width:24,marginLeft:20,marginRight:20,tintColor:'white'}}
                                            resizeMode = {'center'}
                                            source={menuitem.icon}
                                        />
                                    </TouchableOpacity>
                                )
                            case NavBarTypes.LOGOUT:
                                return(
                                    <TouchableOpacity 
                                        key={menuitem.type} style={{height:50,alignItems:'center',justifyContent:'center'}}
                                        onPress={()=>{
                                            showAlert('Peringatan','Anda yakin ingin keluar dari aplikasi?','Keluar',()=>{
                                                reset(0,[{name:'Login'}])
                                                logout()
                                            })
                                        }}>
                                        <Image
                                            style = {{height:24,width:24,marginLeft:20,marginRight:20}}
                                            resizeMode = {'center'}
                                            source={ic_logout}
                                        />
                                    </TouchableOpacity>
                                )
                            case NavBarTypes.BANNER:
                                return(
                                    <Text key={menuitem.type} style={{position:'absolute',left:0,top:0,width:'100%',height:'100%',lineHeight:50, textAlign:'center',textAlignVertical:'center',fontFamily:'Montserrat Bold',fontSize:16,color:'white'}}>MENTORSIP</Text>
                                )
                            case NavBarTypes.BACK:
                                return(
                                    <TouchableOpacity 
                                        key={menuitem.type} style={{paddingLeft:19.8,paddingRight:15.6,height:50,alignItems:'center',justifyContent:'center'}}
                                        onPress={()=>{
                                            if(menuitem.onBack){
                                                menuitem.onBack()
                                            }else{
                                                goBack()
                                            }
                                        }}>
                                        <Image
                                            style = {{height:11.2,width:17.8}}
                                            source={ic_back}
                                        />
                                    </TouchableOpacity>
                                )
                            case NavBarTypes.TITLE:
                                return(
                                    <View key = {menuitem.type} style={{height:50,justifyContent:'center',paddingLeft:index==0?20:0,flex:1}}>
                                        <Text style={{...styles.productsans_regular_14,color:'white'}}>{menuitem.text}</Text>
                                        {menuitem.subtext?
                                        <Text style={{fontFamily:'Roboto',fontSize:10,color:'white'}}>{menuitem.subtext}</Text>
                                        :null}
                                    </View>
                                )
                            case NavBarTypes.SPACE:
                                return(
                                    <View key = {menuitem.type} style={{flex:1}}/>
                                )
                            default:return null
                        }
                    }) : null}
                </View>
            </SafeAreaView>
        )        
    }
}

// const NavBar  = props => {
//     let{items,style,logout, bgColor} = props
//     if(!items)return null

//     // useFocusEffect(
//     //     React.useCallback(() => {
//     //       const onBackPress = () => {
//     //         // if (isSelectionModeEnabled()) {
//     //         //   disableSelectionMode();
//     //         //   return true;
//     //         // } else {
//     //         //   return false;
//     //         // }
//     //         let menuitem = items.find(it => it.type == NavBarTypes.BACK)
//     //         if(menuitem && menuitem.onBack){
//     //             menuitem.onBack()
//     //             return true
//     //         }else{
//     //             return false
//     //             // goBack()
//     //         }
//     //         // return false
//     //       };
    
//     //       BackHandler.addEventListener('hardwareBackPress', onBackPress);
    
//     //       return () =>
//     //         BackHandler.removeEventListener('hardwareBackPress', onBackPress);
//     //     }, [])
//     // );

//     return(
//         <SafeAreaView  style={style}>
//             <View style={{position:'absolute',left:0,top:0,right:0,backgroundColor:bgColor?bgColor:Color.navBarBg,height:50}}/>
//             <View style={{width:'100%',flexDirection:'row',height:50}}>
//                 {items != null ? items.map((menuitem,index) => {
//                     if(!menuitem)return null
//                     switch(menuitem.type){
//                         case NavBarTypes.RIGHT_BUTTON:
//                             return(
//                                 <TouchableOpacity 
//                                     key={menuitem.type} style={{height:50,alignItems:'center',justifyContent:'center'}}
//                                     onPress={menuitem.onPress}>
//                                     <Image
//                                         style = {{height:24,width:24,marginLeft:20,marginRight:20,tintColor:'white'}}
//                                         resizeMode = {'center'}
//                                         source={menuitem.icon}
//                                     />
//                                 </TouchableOpacity>
//                             )
//                         case NavBarTypes.LOGOUT:
//                             return(
//                                 <TouchableOpacity 
//                                     key={menuitem.type} style={{height:50,alignItems:'center',justifyContent:'center'}}
//                                     onPress={()=>{
//                                         showAlert('Peringatan','Anda yakin ingin keluar dari aplikasi?','Keluar',()=>{
//                                             reset(0,[{name:'Login'}])
//                                             logout()
//                                         })
//                                     }}>
//                                     <Image
//                                         style = {{height:24,width:24,marginLeft:20,marginRight:20}}
//                                         resizeMode = {'center'}
//                                         source={ic_logout}
//                                     />
//                                 </TouchableOpacity>
//                             )
//                         case NavBarTypes.BANNER:
//                             return(
//                                 <Text key={menuitem.type} style={{position:'absolute',left:0,top:0,width:'100%',height:'100%',lineHeight:50, textAlign:'center',textAlignVertical:'center',fontFamily:'Montserrat Bold',fontSize:16,color:'white'}}>MENTORSIP</Text>
//                             )
//                         case NavBarTypes.BACK:
//                             return(
//                                 <TouchableOpacity 
//                                     key={menuitem.type} style={{paddingLeft:19.8,paddingRight:15.6,height:50,alignItems:'center',justifyContent:'center'}}
//                                     onPress={()=>{
//                                         if(menuitem.onBack){
//                                             menuitem.onBack()
//                                         }else{
//                                             goBack()
//                                         }
//                                     }}>
//                                     <Image
//                                         style = {{height:11.2,width:17.8}}
//                                         source={ic_back}
//                                     />
//                                 </TouchableOpacity>
//                             )
//                         case NavBarTypes.TITLE:
//                             return(
//                                 <View key = {menuitem.type} style={{height:50,justifyContent:'center',paddingLeft:index==0?20:0,flex:1}}>
//                                     <Text style={{...styles.productsans_regular_14,color:'white'}}>{menuitem.text}</Text>
//                                     {menuitem.subtext?
//                                     <Text style={{fontFamily:'Roboto',fontSize:10,color:'white'}}>{menuitem.subtext}</Text>
//                                     :null}
//                                 </View>
//                             )
//                         case NavBarTypes.SPACE:
//                             return(
//                                 <View key = {menuitem.type} style={{flex:1}}/>
//                             )
//                         default:return null
//                     }
//                 }) : null}
//             </View>
//         </SafeAreaView>
//     )

    
// }

export default connect(mapStateToProps,{logout})(NavBar)