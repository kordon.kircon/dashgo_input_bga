import React from 'react'
import {View,Text,TouchableOpacity} from 'react-native'
import ArrowRight from './ArrowRight'
import Line from './Line'
import TextNormal from './TextNormal'

export default function(props){
    let{onPress,leftText,rightText,renderItem} = props
    return(
        <TouchableOpacity onPress = {onPress}>
            <View style={{flexDirection:'row',paddingVertical:10,paddingHorizontal:20,alignItems:'center'}}>
                <View style={{flex:1,paddingRight:20}}>
                    {renderItem ? renderItem():
                    <View style={{flexDirection:'row'}}>
                        <TextNormal text = {leftText} style={{flex:1}}/>
                        <TextNormal text = {rightText}/>
                    </View>
                    }
                </View>
                <ArrowRight/>
            </View>
            <Line/>
        </TouchableOpacity>
    )
}