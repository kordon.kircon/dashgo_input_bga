import React from 'react'
import {View,TextInput,Text,FlatList,TouchableOpacity} from 'react-native'
import Button,{ButtonTypes} from './Button'
import Modal from 'react-native-modal'
import Color from '../Utilities/Color'
import styles from '../Utilities/Style'
import {sizeFigma} from '../Utilities/Conversion'
import { borderColor } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes'

export const RadioButtonTypes = {
    SQUARE: 0,
    CIRCLE: 1
}

export default class RadioButton extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            isOn:props.isOn
        }
        this.toggle = this.toggle.bind(this)
    }

    toggle(){
        let{onToggle} = this.props
        let{isOn} = this.state
        this.setState({
            isOn:!isOn
        },()=>{
            if(onToggle){
                onToggle(this.state.isOn)
            }
        })
    }

    setIsOn(isOn){
        this.setState({
            isOn:isOn
        })
    }

    render(){
        let{text,style, type, isDisabled} = this.props
        let{isOn} = this.state
        var additionalStyle = {}
        var additionalInnerStyle = {}
        if(type == RadioButtonTypes.SQUARE) {
            additionalStyle = {
                borderRadius: sizeFigma(2)
            }
        }
        if(isDisabled){
            additionalStyle.borderColor = Color.gray_light
            additionalInnerStyle = {
                backgroundColor: Color.gray_light
            }
        }
        return(
            <View style={{flexDirection:'row',alignItems:'center',...style}}>
                <TouchableOpacity 
                    style={{padding:sizeFigma(4),...styles.center}}
                    onPress = {()=>{
                        if(!isDisabled){
                            this.toggle()
                        }
                    }}
                >
                    <View style={{...styles.size_round_18,borderWidth:2,borderColor:Color.gray_dark,...styles.center, ...additionalStyle}}>
                        {isOn && <View style={{...styles.size_round_8,backgroundColor:Color.gray_dark, ...additionalInnerStyle}}/>}
                    </View>
                </TouchableOpacity>
                <Text style={{marginLeft:sizeFigma(6),...styles.productsans_regular_14,color:Color.gray_dark}}>{text}</Text>
            </View>
        )
    }
}