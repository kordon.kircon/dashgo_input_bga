import React from 'react'
import {View, FlatList, Text, PermissionsAndroid, Linking, AppState,} from 'react-native'
import Snackbar from 'react-native-snackbar'
import {post} from '../Utilities/Network'
import Geolocation from 'react-native-geolocation-service'
import { showAlert } from '../Screens/Alert'
import Color from '../Utilities/Color'
import {connect} from 'react-redux'
export const gpsRef = React.createRef()
export function setEventListener(onSuccess, onError) {
    gpsRef.current?.setEventListener(onSuccess, onError)
}

const mapStateToProps = (state, ownProps) => {
    return {
        gps_required: state.data.identity?.gps_required
    }
}

class GPS extends React.Component {
    constructor() {
        super()
        this.state = {
            location: null,
            appState: AppState.currentState,
            goToSetting: false,
            isGetCurrentLocation: false,
            onSuccess: null,
            onError: null,
            // watchPosition: null,
        }
        this.initGPS = this.initGPS.bind(this)
        this.handleAppStateChange = this.handleAppStateChange.bind(this)
        this.getCurrentLocation = this.getCurrentLocation.bind(this)
        this.clearAll = this.clearAll.bind(this)
        this.setEventListener = this.setEventListener.bind(this)
    }

    componentDidMount() {
        let{isDisabled, gps_required} = this.props
        if(isDisabled || !gps_required){ return }
        this.initGPS()
        this.eventListener = AppState.addEventListener('change', this.handleAppStateChange)
    }

    componentWillUnmount() {
        this.clearAll()
    }

    setEventListener(onSuccess, onError) {
        this.setState({
            onSuccess : onSuccess,
            onError: onError
        })
    }

    clearAll(){
        if(this.watchPosition){
            Geolocation.clearWatch(this.watchPosition)   
        } 
        if(this.eventListener){
            this.eventListener.remove()
        }
    }

    async initGPS() {
        let{onError, onSuccess} = this.props
        try {
            let granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.clearAll()
                this.watchPosition = Geolocation.watchPosition( position => {
                    try {
                        if(onSuccess) onSuccess(position)
                    } catch(err){
                        console.log('GPS',err)
                    }
                }, error => {
                    try{
                        if(onError) onError(error)
                        post('loger',{code: '', url: 'gps', error_message: JSON.stringify(error), body: ''})
                        Snackbar.show({
                            text: error.message,
                            duration: Snackbar.LENGTH_SHORT
                        })
                        this.getCurrentLocation()
                    }catch(err){
                        console.log('GPS',err)
                    }
                },{
                    enableHighAccuracy: true,
                    distanceFilter: 5,
                    forceRequestLocation: true,
                    forceLocationManager: true,
                })
                // this.setState({
                //     watchPosition: watchPosition
                // })
                this.getCurrentLocation()
            } else {
                showAlert('Peringatan', 
                'Aplikasi tidak dapat digunakan bila izin lokasi ditolak, silahkan nyalakan izin lokasi di pengaturan untuk melanjutkan menggunakan aplikasi', 
                'Ke Pengaturan', 
                async ()=>{
                    this.setState({
                        goToSetting: true  
                    })
                    await Linking.openSettings()
                },()=>{
                    this.initGPS()
                },false)
            }
        } catch (err) {
            // console.log('Dashboard','initPermission','catch error',err)
        }
    }

    handleAppStateChange(nextAppState) {
        let{appState, goToSetting} = this.state
        if(goToSetting){
            if(appState == 'background' && nextAppState == 'active'){
                this.setState({
                    goToSetting: false
                }, this.initGPS)
            }
        }else{
            this.getCurrentLocation()
        }
        this.setState({
            appState: nextAppState
        })
    }

    async getCurrentLocation() {
        let{onSuccess, onError} = this.props
        let{isGetCurrentLocation} = this.state
        if(isGetCurrentLocation) return
        this.setState({
            isGetCurrentLocation: true
        }, () => {
            Geolocation.getCurrentPosition(position => {
                if(onSuccess){
                    onSuccess(position)

                }
                this.setState({
                    isGetCurrentLocation: false
                })
            },
            error => {
                if(onError) onError(error)
                post('loger',{code: '', url: 'gps', error_message: JSON.stringify(error), body: ''})
                if(error.code == 3){
                    Snackbar.show({
                        text: error.message,
                        duration: Snackbar.LENGTH_INDEFINITE,
                        action: {
                            text: 'RETRY',
                            textColor: Color.green,
                            onPress: () => {
                                Snackbar.dismiss()
                                this.getCurrentLocation()
                            }
                        }
                    })
                } else {
                    Snackbar.show({
                        text: error.message,
                        duration: Snackbar.LENGTH_SHORT
                    })
                    if(error.code == 5){
                        this.setState({
                            isGetCurrentLocation: false
                        }, this.getCurrentLocation)
                    }
                }
                // this.setState({
                //     isGetCurrentLocation: false
                // })
            },{
                enableHighAccuracy: true,
                timeout: 15000,
                distanceFilter: 5,
            })
        })
    }

    render(){
        return null
    }
}

export default connect(mapStateToProps, null, null, {forwardRef: true})(GPS)