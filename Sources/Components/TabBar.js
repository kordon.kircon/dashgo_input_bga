import React from 'react'
import {View,Dimensions,TouchableOpacity,Text,Animated} from 'react-native'
import Color from '../Utilities/Color'
import { sizeFigma } from '../Utilities/Conversion'
import styles from '../Utilities/Style'

let {width} = Dimensions.get('window')

export default class TabBar extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            index:new Animated.Value(0),
            currentIndex:0,
        }
        let{items} = this.props
        this.inputRange = []
        this.outputRange = []
        this.delta = (width - sizeFigma(40))/items.length
        for(var i=0; i<items.length; i++){
            this.inputRange.push(i)
            this.outputRange.push(i*this.delta)
        }
        this.changeTab = this.changeTab.bind(this)
    }

    changeTab(idx){
        let{index} = this.state
        let{onTabChange} = this.props
        Animated.timing(index,{
            toValue:idx,
            duration:200,
            useNativeDriver: false
        }).start((callback) => {
            // console.warn(callback)
            if(onTabChange){
                onTabChange(idx)
            }
        })
        this.setState({
            currentIndex:idx
        })
    }

    render(){
        let {items, style} = this.props
        let {index,currentIndex} = this.state
        return(
            <View style = {style}>
            <View style = {{height:sizeFigma(41),}}>
                <View style={{flexDirection:'row',flex:1}}>
                    {items.map((item,idx) => {
                        return(
                            <TouchableOpacity
                                key = {item.text}
                                style={{flex:1,alignItems:'center',justifyContent:'center'}}
                                onPress = {()=>{
                                    this.changeTab(idx)
                                }}
                                >
                                <Text style={{...styles.productsans_bold_14,color:idx == currentIndex?Color.textBlack:Color.gray_light}}>{item.text}</Text>
                            </TouchableOpacity>  
                        )            
                    })}
                </View>
                <View style = {{height: sizeFigma(3), backgroundColor: Color.gray_light}}>
                    <Animated.View style={{height:sizeFigma(3),width:this.delta,backgroundColor:Color.green,marginLeft:index.interpolate({
                        inputRange:this.inputRange,
                        outputRange:this.outputRange
                    })}}/>
                </View>
            </View>
            </View>
        )
    }
}