import React from 'react'
import {TouchableOpacity,Text} from 'react-native'
import Color from '../Utilities/Color'
import { sizeFigma } from '../Utilities/Conversion'
import styles from '../Utilities/Style'
import {connect} from 'react-redux'

const mapStateToProps = (state, ownProps) => {
    return {
        bgColor: state.data.identity?.btn_active_color
    }
}

export const ButtonTypes = {
    VERY_DARK:0,
    WHITE_BORDER:1,
    GREEN:2,
    GRAY:3,
    DISABLE:4,
    ORANGE:5,
    RED:6,
}

class Button extends React.Component{

    constructor(props){
        super(props)
        this.backgroundColor = this.backgroundColor.bind(this)
        this.fontColor = this.fontColor.bind(this)
        this.borderColor = this.borderColor.bind(this)
        this.borderWidth = this.borderWidth.bind(this)
    }

    borderWidth(){
        let{type} = this.props
        switch(type){
            case ButtonTypes.WHITE_BORDER:
                return 2
            default :
                return 0
        }
    }

    borderColor(){
        let{type} = this.props
        switch(type){
            case ButtonTypes.WHITE_BORDER:
                return 'white'
            default :
                return 'transparent'
        }
    }

    backgroundColor(){
        let{type, bgColor} = this.props
        switch(type){
            case ButtonTypes.GRAY:
                return '#505050'
            case ButtonTypes.VERY_DARK:
                return '#202020'
            case ButtonTypes.GREEN:
                if(bgColor) return bgColor
                return  '#2acc40'
            case ButtonTypes.DISABLE:
                return Color.gray
            case ButtonTypes.ORANGE:
                return Color.orange
            case ButtonTypes.RED:
                return Color.red
            default :
                return 'transparent'
        }
    }

    fontColor(){
        let{type} = this.props
        switch(type){
            case ButtonTypes.DISABLE:
            case ButtonTypes.GRAY:
            case ButtonTypes.WHITE_BORDER:
            case ButtonTypes.GREEN:
            case ButtonTypes.ORANGE:
            case ButtonTypes.RED:
                return 'white'
            default :
                return Color.textButtonVeryDark
        }
    }

    render(){
        let{text,style,onPress,textStyle,bgColor} = this.props
        return(
            <TouchableOpacity 
                style={{height:sizeFigma(40),borderRadius:sizeFigma(5),backgroundColor:this.backgroundColor(),borderWidth:this.borderWidth(),borderColor:this.borderColor(), alignItems:'center',justifyContent:'center',...style}}
                onPress = {onPress}
                >
                <Text style={{...styles.productsans_bold_14,color:this.fontColor(),...textStyle}}>{text}</Text>
            </TouchableOpacity>
        )
    }
}

export default connect(mapStateToProps)(Button)