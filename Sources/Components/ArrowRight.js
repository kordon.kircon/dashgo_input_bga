import React from 'react'
import Image from './Image'
import Color from '../Utilities/Color'
import icon from '../Images/chevron_right_24_px.png'

export default function(props){
    let{style} = props
    return(
        <Image
            style = {{width:24,height:24,tintColor:Color.gray,...style}}
            source = {icon}
        />
    )
}