import React from 'react'
import {View,TextInput,Text,FlatList} from 'react-native'
import Button,{ButtonTypes} from './Button'
import Modal from 'react-native-modal'
import Color from '../Utilities/Color'
import TextBold from './TextBold'
import { sizeFigma } from '../Utilities/Conversion'
import styles from '../Utilities/Style'

export const pickerRef = React.createRef()
export function showPicker(title = '', data = [], onSelect = null, onAction = null,textKey = 'text'){
    pickerRef.current?.show(title,data,onSelect,onAction,textKey)
}

let itemHeight = sizeFigma(30)

export default class Picker extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            value:null,
            isVisible:false,
            title:null,
            data:[],
            onSelect:null,
            onAction:null,
            textKey:null,
            searchData:null,
        }
        this.close = this.close.bind(this)
        this.show = this.show.bind(this)
    }

    show(title = '', data = [], onSelect = null, onAction = null,textKey = 'text'){
        this.setState({
            isVisible:true,
            title:title,
            data:data,
            onSelect:onSelect,
            onAction:onAction,
            textKey:textKey,
            searchData: null,
        })
    }

    close(){
        this.setState({
            isVisible:false
        })
    }

    render(){
        let{value,isVisible,title,data,onSelect,onAction,textKey,searchData} = this.state
        var _data = data
        if(searchData) {
            _data = searchData
        }
        return(
            <Modal 
                style = {{margin:0}}
                isVisible = {isVisible}
                onBackButtonPress = {this.close}
                onBackdropPress = {this.close}
            >
                <View style={{flex:1,justifyContent:'flex-end'}}>
                    <View style={{backgroundColor:'white',borderTopLeftRadius:sizeFigma(5),borderTopRightRadius:sizeFigma(5),paddingLeft:sizeFigma(20),paddingRight:sizeFigma(20),paddingBottom:sizeFigma(20),paddingTop:sizeFigma(10)}}>
                        <View style={{backgroundColor:Color.gray_dark,height:sizeFigma(3),width:sizeFigma(50),borderRadius:sizeFigma(1.5),alignSelf:'center'}}/>
                        {title && <TextBold text = {title}/>}
                        <TextInput
                            style = {{borderWidth: 1, paddingVertical: sizeFigma(5), paddingHorizontal: sizeFigma(10), borderRadius: sizeFigma(5)}}
                            placeholder = 'Cari'
                            onChangeText = {text => {
                                if(text){
                                    this.setState({
                                        searchData: data.filter(it => it[textKey].toLowerCase().includes(text.toLowerCase()))
                                    })
                                }else{
                                    this.setState({
                                        searchData: null
                                    })
                                }
                            }}
                        />
                        <FlatList
                            style={{height:itemHeight*data.length>sizeFigma(185)?sizeFigma(185):itemHeight*data.length,width:'100%'}}
                            data = {_data}
                            keyExtractor = {(item, index) => index+''}
                            renderItem = {({item,index}) => 
                                <Text 
                                    style={{...styles.productsans_regular_14,textAlignVertical:'center',height:itemHeight,lineHeight:itemHeight,width:'100%'}}
                                    onPress = {()=>{
                                        this.close()
                                        if(onSelect){
                                            onSelect(item)
                                        }
                                    }}
                                >{item[textKey]}</Text>
                            }
                        />
                    </View>
                </View>
            </Modal>
        )
    }
}