import React from 'react'
import {launchImageLibrary,launchCamera} from 'react-native-image-picker'
import {showCustomAlert,hideAlert} from '../Screens/Alert'
import Button,{ButtonTypes} from './Button'
import { View } from 'react-native'
import {store} from '../Redux/index'

// const imageSize = 1200

export function launchLib(onSuccess,imageSize = 300,mediaType = 'photo'){
    setTimeout(()=>{
        launchImageLibrary(
            {
                maxHeight:imageSize,
                maxWidth:imageSize,
                mediaType:mediaType,
                // title: mediaType == 'video'?'Pilih video':'Pilih gambar',
                // takePhotoButtonTitle: mediaType == 'video'?'Ambil video':'Ambil gambar',
                // chooseFromLibraryButtonTitle : 'Pilih dari library',
            }
            ,async (response)=>{
            if (response.didCancel) {
                // console.log('ImagePicker','cancel')
            } else if (response.error) {
                // console.log('ImagePicker','error '+response.error)
            } else if (response.customButton) {
                // console.log('ImagePicker','custom button: '+response.customButton)
            } else {
                // console.log('ImagePicker','else '+JSON.stringify(response))
                if(onSuccess){
                    // console.log('ON SUCCESS PICKER',response)
                    // let uri = Platform.OS === "android" ? response.uri : response.uri.replace("file://", "")
                    response = response.assets[0]
                    let uri = response.uri
                    var name = ''
                    var type = ''
                    if(mediaType == 'video'){
                        let tempArray = uri.split('/')
                        name = tempArray[tempArray.length - 1]
                        types = name.split('.')
                        type = 'video/'+types[types.length-1]
                    }else{
                        name = response.fileName.split('.')[0]
                        type = response.type
                    }
                    onSuccess({
                        uri:uri,
                        name:name,
                        type:type
                    })
                }
            }
        })                             
    },1500)
}

export function launchCam(onSuccess,imageSize = 300,mediaType = 'photo'){
    setTimeout(() => {
        launchCamera(
            {
                maxHeight:imageSize,
                maxWidth:imageSize,
                mediaType:mediaType,
                // title: mediaType == 'video'?'Pilih video':'Pilih gambar',
                // takePhotoButtonTitle: mediaType == 'video'?'Ambil video':'Ambil gambar',
                // chooseFromLibraryButtonTitle : 'Pilih dari library',
            }
            ,async (response)=>{
            if (response.didCancel) {
                // console.log('User cancelled image picker')
            } else if (response.error) {
                // console.log('ImagePicker Error: '+response.error)
            } else if (response.customButton) {
                // console.log('User tapped custom button: '+response.customButton)
            } else {
                if(onSuccess){
                    // console.log('ON SUCCESS PICKER',response)
                    response = response.assets[0]
                    // let uri = Platform.OS === "android" ? response.uri : response.uri.replace("file://", "")
                    let uri = response.uri
                    var name = ''
                    var type = ''
                    if(mediaType == 'video'){
                        let tempArray = uri.split('/')
                        name = tempArray[tempArray.length - 1]
                        types = name.split('.')
                        type = 'video/'+types[types.length-1]
                    }else{
                        if(response && response.fileName){
                            // name = response.fileName.split('.')[0]
                            name = response.fileName
                            type = response.type
                        }
                    }
                    onSuccess({
                        uri:uri,
                        name:name,
                        type:type
                    })
                }
            }
        })
    }, 1500);
}

export default function imagePicker(onSuccess,imageSize = 300,mediaType = 'photo',isUseGallery = true){
    let typeString = mediaType == 'photo' ? 'Foto' : 'Video'
    showCustomAlert('Pilih '+typeString,()=>{
        return(
            <View>
                <Button
                    type = {ButtonTypes.RED}
                    text = {'Ambil '+typeString}
                    onPress = {()=>{
                        hideAlert(()=>{
                            launchCam(onSuccess,imageSize,mediaType)
                        })
                    }}
                />
                {isUseGallery && 
                <Button
                    style = {{marginTop:15}}
                    type = {ButtonTypes.RED}
                    text = {'Ambil dari galeri'}
                    onPress = {()=>{
                        hideAlert(()=>{
                            launchLib(onSuccess,imageSize,mediaType)
                        })
                    }}
                />
                }
            </View>
        )
    },'Batal',null,false
    )
}