import React, { Component } from 'react';
import {Modal,View,ActivityIndicator} from 'react-native';
import Slider from '@react-native-community/slider'
import Color from '../Utilities/Color'
// import {connect} from 'react-redux'

// const mapStateToProps = (state, ownProps) => ({showLoading: state.app.showLoading})

// var self = null

export const loadingRef = React.createRef()

export function showLoading(){
  loadingRef.current?.show()
}

export function hideLoading(){
  loadingRef.current?.hide()
}

export function setLoadingProgress(progress){
  loadingRef.current?.setProgress(progress)
}

export default class Loading extends Component{

  constructor(props){
    super(props)
    this.state = {
      visible:false,
      isLocal:false,
      progress:null,
    }
    this.hide = this.hide.bind(this)
    this.show = this.show.bind(this)
  }

  async hide(){
    this.setState({
      visible:false,
      progress:null,
    })
  }

  setProgress(progress){
    this.setState({
      progress:progress
    })
  }

  show(){
    this.setState({
      visible:true,
    })
  }

  render(){
    let{visible,isLocal,progress} = this.state
    // let{isLocal,showLoading} = this.props
    return(
      showLoading ?
      isLocal ?
        <View style={{flex:1,alignItems:'center',justifyContent:'center',position:'absolute',left:0,top:1,right:0,bottom:0}}>
          <ActivityIndicator color="white" size="small"/>
        </View>
        :
        <Modal
          visible={visible}
          onRequestClose={()=>{

          }}
          transparent={true}>
          <View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'#0000004D'}}>
            <ActivityIndicator color="white" size="small"/>
            {progress?
            <Slider
              style={{width: '100%', height: 13}}
              minimumValue={0}
              maximumValue={progress.maximumValue}
              value = {progress.value}
              minimumTrackTintColor={Color.red}
              maximumTrackTintColor="#656565"
              thumbTintColor = {'white'}
            />
            :null}
          </View>
        </Modal>
        :
        null

    )
  }

}

// export default connect(mapStateToProps)(Loading)