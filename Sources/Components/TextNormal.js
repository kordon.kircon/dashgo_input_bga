import React from 'react'
import {Text} from 'react-native'
import Color from '../Utilities/Color'
import styles from '../Utilities/Style'

export default function(props){
    let{text,style} = props
    return(
        <Text style={{...styles.productsans_regular_14,color:Color.gray_dark,...style}}>{text}</Text>
    )
}