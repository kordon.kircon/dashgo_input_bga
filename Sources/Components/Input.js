import React from 'react'
import {View,TextInput,Text,TouchableOpacity} from 'react-native'
import Color from '../Utilities/Color'
import styles from '../Utilities/Style'
import {sizeFigma} from '../Utilities/Conversion'
import Image from './Image'
import ic_dropdown from '../Images/dropdownicon.png'
import { showPicker } from './Picker'

export const InputTypes = {
    TEXT: 0,
    PICKER: 1
}

export default class Input extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            value:props.value
        }
        this.getValue = this.getValue.bind(this)
        this.blur = this.blur.bind(this)
        this.renderPicker = this.renderPicker.bind(this)
    }

    setValue(value) {
        this.setState({
            value:''+value
        })
    }

    getValue(){
        return this.state.value
    }

    blur(){
        this.input.blur()
    }

    isEmpty(){
        let{value} = this.state
        return !(value && value.length>0)
    }

    renderPicker() {
        let{placeholder, datas, getDatas, onSelect, isDisabled, style, isResetValue, textKey} = this.props
        let{value} = this.state
        var text = placeholder
        var textColor = Color.gray
        var extraStyle = {}
        if(value){
            text = textKey && value[textKey]?value[textKey]: value.nama ? value.nama : value
            textColor = Color.textBlack
        }
        if(isDisabled){
            extraStyle = {
                borderColor: Color.gray_light
            }
        }
        
        return(
            <TouchableOpacity 
                style = {{...styles.input_border, ...extraStyle, ...style}}
                onPress = {()=>{
                    // var _datas = datas
                    // if(((_datas && _datas.length == 0) || ! _datas) && getDatas) {
                    //     console.log('Input datas get datas 1',_datas)
                    //     console.log('Mamam',getDatas())
                    //     _datas = getDatas()
                    //     console.log('Input datas get datas 2',_datas)
                    // }
                    // console.log('Input datas',_datas)
                    // if(!_datas || (_datas && _datas.length == 0)){ return }
                    if(datas){
                        // console.log('Input', 'renderPicker', datas, textKey?textKey:'nama', textKey)
                        showPicker(null, datas, item => {
                            if(isResetValue){
                                if(onSelect){
                                    onSelect(item)
                                }
                            }else{
                                this.setState({
                                    value: item
                                },()=>{
                                    if(onSelect){
                                        onSelect(item)
                                    }
                                })
                            }
                        }, null, textKey?textKey:'nama')
                        return
                    }
                    if(!getDatas || isDisabled) return
                    getDatas()
                    .then(response => {
                        if(response && response.length > 0){
                            showPicker(null, response, item => {
                                if(isResetValue){
                                    if(onSelect){
                                        onSelect(item)
                                    }
                                }else{
                                    this.setState({
                                        value: item
                                    },()=>{
                                        if(onSelect){
                                            onSelect(item)
                                        }
                                    })
                                }
                            }, null , textKey?textKey:'nama')
                        }
                    })
                }}
            >
                <Text style = {{flex: 1, ...styles.productsans_regular_14, color: textColor}}>{text}</Text>
                {!isDisabled &&
                    <Image
                        style = {styles.size_24}
                        source = {ic_dropdown}
                    />
                }
            </TouchableOpacity>
        )
    }

    render(){
        let{style,placeholder,keyboardType,rightView,onChangeText,placeholderTextColor,title,rootStyle,secureTextEntry, type, isDisabled} = this.props
        let{value} = this.state
        var additionalBoxStyle = {}
        if(type == InputTypes.PICKER) {
            return this.renderPicker()
        }
        if(isDisabled){
            additionalBoxStyle = {
                borderColor: Color.gray_light
            }
        }
        return(
            <View style={[{},rootStyle]}>
                {title && 
                <Text style={{...styles.productsans_regular_14,color:Color.gray,marginBottom:sizeFigma(10)}}>{title}</Text>
                }
                <View style={{borderWidth:sizeFigma(1),borderRadius:sizeFigma(5),borderColor:Color.gray,height:sizeFigma(40),paddingLeft:sizeFigma(10),alignItems:'center',flexDirection:'row', ...style, ...additionalBoxStyle}}>
                    <TextInput 
                        ref = {ref => this.input = ref}
                        style={{...styles.productsans_regular_14,color:Color.gray_dark,flex:1}}
                        placeholderTextColor = {Color.gray}
                        placeholder = {placeholder}
                        placeholderTextColor = {placeholderTextColor}
                        keyboardType = {keyboardType}
                        secureTextEntry = {secureTextEntry}
                        onChangeText = {text => {
                            this.setState({
                                value:text
                            })
                            if (onChangeText){
                                onChangeText(text)
                            }
                        }}
                        value = {value}
                        editable = {!isDisabled}
                        />
                    {rightView?
                    rightView
                    :null}
                </View>
            </View>
        )
    }
}