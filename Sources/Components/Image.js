import React from 'react'
import FastImage from 'react-native-fast-image'
import {Image,TouchableOpacity,View,Animated, Text} from 'react-native'
import Modal from 'react-native-modal'
import { PinchGestureHandler,State } from 'react-native-gesture-handler'
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
import Button, { ButtonTypes } from './Button'
import Color from '../Utilities/Color'
import styles from '../Utilities/Style'
import ic_zoom from '../Images/zoom.png'
import ic_empty_image from '../Images/image_black_24dp-1.png'
import {isValidImage, sizeFigma} from '../Utilities/Conversion'
import { IMAGE_BASE_URL } from '../Utilities/Network'

export default class TheImage extends React.PureComponent{

  constructor(props){
    super(props)
    this.state = {
      isZooming : false,
      scale : new Animated.Value(1)
    }
    this.renderContent = this.renderContent.bind(this)
    this.setZoom = this.setZoom.bind(this)
    this.hide = this.hide.bind(this)
    this.onZoomEvent = this.onZoomEvent.bind(this)
    this.onZoomStateChange = this.onZoomStateChange.bind(this)
  }



  renderContent(){
    let{style,resizeMode,uri,source,isZoom,title,isDebug} = this.props
    if(isDebug){
      // console.log('Bagong ngepet',uri,source)
      return null
    }
    var imageUrl = null
    if(uri){
      if(uri.uri){
        imageUrl = uri.uri
      }else{
        imageUrl = uri
      }
    }      
    if(title){
      let imagePadding = sizeFigma(10)
      let color = Color.primary_blue
      

      return(
        <View pointerEvents = {'none'}>
          <View style={{marginTop: sizeFigma(8),borderWidth: sizeFigma(1),borderColor:color,height:sizeFigma(200), ...styles.center, borderRadius: sizeFigma(10), paddingRight: sizeFigma(8)}}>
              {imageUrl ? 
              <View style = {{position: 'absolute', left: imagePadding, top: imagePadding, right: imagePadding, bottom: imagePadding, borderRadius: sizeFigma(5), overflow: 'hidden'}}>
                  <FastImage
                      style = {styles.absolute}
                      source = {{uri:imageUrl}}
                      resizeMode = {resizeMode?resizeMode : 'cover'}
                  />
              </View>                    
              :
              <View style = {{position: 'absolute', left: imagePadding, top: imagePadding, right: imagePadding, bottom: imagePadding, borderRadius: sizeFigma(5), ...styles.center, borderWidth: sizeFigma(1), borderColor: Color.gray_3}}>
                  <Image
                      style = {styles.image_24}
                      source = {ic_empty_image}
                  />
                  {/* <Text style = {{...styles.productsans_regular_12, color: Color.gray_2, marginTop: sizeFigma(5)}}>Belum ada foto</Text> */}
              </View>                    
              }
              {uri?
              <Image
                  style = {styles.image_48}
                  source = {ic_zoom}
              />
              :null
              }
          </View>
          <Text style={{...styles.productsans_bold_12, ...styles.topLeft, left: sizeFigma(15), color:color, paddingHorizontal: 5, backgroundColor: Color.white}}>{title}</Text>
      </View>
      )
    }
    return(
      style.tintColor || (source && !imageUrl) || (imageUrl && !isValidImage(imageUrl))?
      <View pointerEvents = {'none'}>
        <Image
          style = {{...style}}
          resizeMode={resizeMode ? resizeMode : 'contain'}
          source = {source}
        />
      </View>
      :
      <FastImage
        style={style}
        resizeMode={source ? resizeMode : imageUrl ? resizeMode ? resizeMode : 'cover' : 'contain'}
        source={imageUrl ? {uri:imageUrl} : source ? source : null }
      />
    )
  }

  setZoom(){
    let{uri,source} = this.props
    if(!uri && !source) return
    this.setState({
      isZooming:true
    })
  }
  
  hide(){
    this.setState({
      isZooming:false
    })
  }

  onZoomEvent(event){
    let{scale} = this.state
    return Animated.event(
      [
        {
          nativeEvent: { scale: scale }
        }
      ],
      {
        useNativeDriver: true
      }
    )
  }
  
  onZoomStateChange(event){
    if (event.nativeEvent.oldState === State.ACTIVE) {
      Animated.spring(scale, {
        toValue: 1,
        // useNativeDriver: true
      }).start()
    }
  }

  render(){
    let{style,resizeMode,uri,source,isZoom,action,title} = this.props
    var imageUrl = uri
    if(uri && uri.uri){
      imageUrl = uri.uri
    }
    if(isZoom||title){
      let{isZooming,scale} = this.state
      return(

        <View style={style}>
        {/* <View> */}
          <Modal
                style = {{margin:0}}
                isVisible = {isZooming}
                onBackButtonPress = {this.hide}
                onBackdropPress = {this.hide}
          >
            <ReactNativeZoomableView
              maxZoom={1.5}
              minZoom={0.5}
              zoomStep={0.5}
              initialZoom={1}
              bindToBorders={true}
              onZoomAfter={this.logOutZoomState}
              captureEvent = {true}
              style={{
                  padding: 10,
                  // backgroundColor: 'red',
              }}
            >
              <TouchableOpacity
                style={{
                  width: '100%',
                  height: '100%',
                  // transform: [{ scale: scale }]
                }}
                onPress = {this.hide}
              >
                <FastImage
                    source={{
                      uri: imageUrl
                    }}
                    style={{
                      width: '100%',
                      height: '100%',
                      // transform: [{ scale: scale }]
                    }}
                    resizeMode='contain'
                  />
              </TouchableOpacity>
            </ReactNativeZoomableView>
            {action && 
            <Button
              style = {action.style}
              type = {ButtonTypes.BLUE}
              text = {action.text}
              onPress = {action.onPress}
            />
            }
          </Modal>
          <TouchableOpacity
                  style = {style}
                  onPress = {this.setZoom}
          >
            {this.renderContent()}
          </TouchableOpacity>
        </View>
      )
    }
    return(
      this.renderContent()
    )
  }
}


// import React from 'react'
// import FastImage from 'react-native-fast-image'
// import {Image} from 'react-native'

// export default class TheImage extends React.PureComponent{
//   render(){
//     let{style,resizeMode,uri,source} = this.props
//     return(
//       style.tintColor || source?
//       <Image
//         style = {style}
//         resizeMode={source ? resizeMode : uri ? resizeMode : 'contain'}
//         source = {source}
//       />
//       :
//       <FastImage
//         style={style}
//         resizeMode={source ? resizeMode : uri ? resizeMode : 'contain'}
//         source={uri ? {uri:uri} : source ? source : null }
//       />
//     )
//   }
// }
