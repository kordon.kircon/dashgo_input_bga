/**
 * @format
 */

import 'react-native-gesture-handler'
import React from 'react'
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {Provider} from 'react-redux'
import {store,persistor} from './Sources/Redux/index'
import Loading,{loadingRef} from './Sources/Components/Loading'
import { PersistGate } from 'redux-persist/integration/react'
import Alert,{alertRef} from './Sources/Screens/Alert'
import Picker,{pickerRef} from './Sources/Components/Picker'
// import GPS, {gpsRef} from './Sources/Components/GPS'

const RNRedux = () => (
    <Provider store = {store}>
        <PersistGate loading={null} persistor = {persistor}>
            <Loading ref = {loadingRef}/>
            <Alert ref = {alertRef}/>
            <Picker ref = {pickerRef}/>
            {/* <GPS ref = {gpsRef}/> */}
            <App/>
        </PersistGate>
    </Provider>
)

AppRegistry.registerComponent(appName, () => RNRedux);
